$(function() {
	// 数据表格
	$('#grid').datagrid({    
	    url:'/baseDict/findByCriteria.action',    
	    columns:[[    
	        {field:'dict_id',title:'数据字典id',width:100},    
	        {field:'dict_type_code',title:'类型编码',width:100,sortable: true, sorter: function (a, b) { return (a > b ? 1 : -1) }},    
	        {field:'dict_type_name',title:'类型名称',width:100},    
	        {field:'dict_item_name',title:'项目名称',width:100},    
	        {field:'dict_item_code',title:'项目编码',width:100},    
	        {field:'dict_sort',title:'排序字段',width:100},    
	        {field:'dict_enble',title:'状态',width:100, formatter: function(value,rowData,index) {
	        	if (rowData.dict_enble == 1) {
	        		return '使用中';
	        	} else {
	        		return '已停用';
	        	}
	        }},    
	        {field:'dict_memo',title:'备注',width:100}
	    ]],
	    toolbar: [{
			iconCls: 'icon-add',
			text: '添加',
			handler: function(){
				// 打开添加窗口
				$("#insertDlg").dialog('open');
			}
		},'-',{
			iconCls: 'icon-cut',
			text: '删除',
			handler: function(){
				// 删除
				var rowData = $('#grid').datagrid('getSelected');
				if (null != rowData) {
					deleteData();
				} else {
					$.messager.alert("提示", "请选中要删除的行", 'error');
				}
			}
		},'-',{
			iconCls: 'icon-save',
			text: '修改',
			handler: function(){
				var rowData = $('#grid').datagrid('getSelected');
				if (null != rowData) {
					// 打开修改窗口
					$('#updateDlg').dialog('open');
					// 填充后台数据
					$('#updateForm').form('load', '/baseDict/findById.action?dict_id=' + rowData.dict_id);
				} else {
					$.messager.alert("提示", "请选中要修改的行", 'error');
				}
			}
		}],
		onDblClickRow:function(rowIndex, rowData) {
				// 打开修改窗口
				$('#updateDlg').dialog('open');
				// 填充后台数据
				$('#updateForm').form('load', '/baseDict/findById.action?dict_id=' + rowData.dict_id);
		},
		remoteSort:false,
	    striped: true,
	    rownumbers: true,
	    singleSelect: true,
	    pagination: true,
	}); 
	// 添加数据对话框
	$('#insertDlg').dialog({    
	    title: '添加数据',    
	    width: 400,    
	    height: 260,    
	    closed: true,    
	    modal: true,
	    buttons:[{
			text:'保存',
			handler:function(){
				// 访问后台数据
				insertData();
			}
		},{
			text:'关闭',
			handler:function(){
				// 关闭对话框
				$("#insertDlg").dialog('close');
			}
		}]
	});
	// 修改数据对话框
	$('#updateDlg').dialog({    
		title: '修改数据',    
		width: 400,    
		height: 260,    
		closed: true,    
		modal: true,
		buttons:[{
			text:'保存',
			handler:function(){
				// 访问后台数据
				updateData();
			}
		},{
			text:'关闭',
			handler:function(){
				// 关闭对话框
				$("#updateDlg").dialog('close');
			}
		}]
	});
	// 搜索窗口
	$('#searchPanel').panel({
		  title: '查询窗口',
		  iconCls:'icon-search',
          collapsible:true
	});
	// 查询按钮的点击事件
	$('#searchBtn').bind('click', function() {
		var formdata= $('#searchForm').serializeJSON();
		$('#grid').datagrid('load',formdata);	
	});	
	// 重置按钮的点击事件
	$('#resetBtn').bind('click', function() {
		$('#searchForm').form('clear');
	});	
});
/**
 * 添加数据
 */
function insertData() {
	// 提交添加数据的表单
	$('#insertForm').form('submit', {
		url: '/baseDict/insert.action',
		success:function(data){ 
			data = eval('(' + data + ')');
			// 如果结果码是200， 提示添加成功，否则，添加失败
			if (data.status == 200) {
				$.messager.alert("提示", data.msg, 'info', function() {
					// 刷新表格数据
					$('#grid').datagrid('reload');
					// 关闭对话框
					$('#insertDlg').dialog('close');
					// 清除表单数据
					$('#insertForm').form('clear');
				});
			} else if (data.status == 400) {
				$.messager.alert("提示", data.msg, 'info');
			}
		}
	});
}

/**
 * 删除数据
 */
function deleteData() {
	$.ajax({
		type: 'POST',
		url: '/baseDict/deleteById.action',
		data: {'dict_id': $('#grid').datagrid('getSelected').dict_id},
		dataType: 'json',
		success: function(data) {
			// 如果结果码是200， 提示添加成功，否则，添加失败
			if (data.status == 200) {
				$.messager.alert("提示", data.msg, 'info', function() {
					// 刷新表格数据
					$('#grid').datagrid('reload');
				});
			} else if (data.status == 400) {
				$.messager.alert("提示", data.msg, 'info');
			}
		}
	});
}

/**
 * 修改数据
 */
function updateData() {
	// 提交修改后的数据到后台
	$('#updateForm').form('submit', {
		url: '/baseDict/updateById.action',
		success:function(data){ 
			data = eval('(' + data + ')');
			// 如果结果码是200， 提示添加成功，否则，添加失败
			if (data.status == 200) {
				$.messager.alert("提示", data.msg, 'info', function() {
					// 刷新表格数据
					$('#grid').datagrid('reload');
					// 关闭对话框
					$('#updateDlg').dialog('close');
					// 清除表单数据
					$('#updateForm').form('clear');
				});
			} else if (data.status == 400) {
				$.messager.alert("提示", data.msg, 'info');
			}
		}
	});
}