$(function() {
	// 设置弹出窗口的属性
	$('#add').window({
		title : '添加人员',
		closed : false,
		cache : false,
		collapsible : true,
		maximizable : false, // 不可最大化
		resizable : false,
		shadow : true,
		left : 500,
		top : 80,
		modal : true, // 模态窗
	});
	$('#update').window({
		title : '修改人员',
		closed : false,
		cache : false,
		collapsible : true,
		maximizable : false, // 不可最大化
		resizable : false,
		shadow : true,
		left : 500,
		top : 80,
		modal : true, // 模态窗
	});
	$('#update').window('close');
	$('#add').window('close');
	$('#centerStaffCheck').datagrid({
		title : '用户管理',
		iconCls : 'icon-tip',
		loadMsg : '数据加载中...',
		nowrap : false,
		striped : true,
		collapsible : true,
		url : '/json/centerStaffCheck.json',
		sortName : 'id',
		sortOrder : 'desc',
		remoteSort : false,
		idField : 'id',
		frozenColumns : [ [ {
			field : 'ck',
			checkbox : true
		}, {
			title : '编号',
			field : 'id',
			sortable : true
		} ] ],
		columns : [ [ {
			field : 'staff_code',
			title : '姓名'
		}, {
			field : 'staff_college',
			title : '院系'
		}, {
			field : 'staff_major',
			title : '专业'
		}, {
			field : 'staff_class',
			title : '班级'
		}, {
			field : 'staff_sno',
			title : '学号'
		}, {
			field : 'staff_tel',
			title : '联系电话'
		}, {
			field : 'staff_qq',
			title : 'QQ'
		}, {
			field : 'staff_job',
			title : '职务'
		}] ],
		onDblClickRow : function(selectRow) {
			edit(selectRow);
		},
		pagination : true,
		rownumbers : true,
		toolbar : [ {
			id : 'btnadd',
			text : '添加',
			iconCls : 'icon-add',
			handler : function() {
				$('#addForm').form('clear');
				// 打开表单窗口
				$('#add').window('open');
			}
		}, '-', {
			id : 'btnsave',
			text : '修改',
			iconCls : 'icon-save',
			handler : function(selectRow) {
				btnEdit(selectRow);
			}
		}, '-', {
			id : 'btncut',
			text : '删除',
			iconCls : 'icon-cut',
			handler : function() {
				del();
			}
		} ]
	});
	var p = $('#centerStaffCheck').datagrid('getPager');
	$(p).pagination({
		onBeforeRefresh : function() {
		}
	});
	// 添加用户
	$("#addstaff").click(function() {
		$('#addForm').form('submit', {
			url : '',
			success : function(data) {
				var jsonData = JSON.parse(data);
				if (jsonData.status == 200) {
					$.messager.alert("提示", jsonData.msg, 'info', function() {
						// 刷新表格数据
						$('#centerStaffCheck').datagrid('reload');
						$('#add').window('close');
					});

				} else if (jsonData.status == 400) {
					$.messager.alert("提示", jsonData.msg, 'info', function() {
					});
				}
			}
		});
	});	
	// 修改用户
	$("#updatestaff").click(function() {
		$('#updateForm').form('submit', {
			url : '',
			success : function(data) {// {"status":200,"msg":"用户信息更新成功","data":null}
				var jsonData = JSON.parse(data);
				console.log(jsonData);
				if (jsonData.status == 200) {
					$.messager.alert("提示", jsonData.msg, 'info', function() {
						// 刷新表格数据
						$('#centerStaffCheck').datagrid('reload');
						$('#update').window('close');
					});
				} else if (jsonData.status == 400) {
					$.messager.alert("提示", jsonData.msg, 'info', function() {
					});
				}
			}
		});
	})
})

	function btnEdit(selectRow) {
		let rows = $("#centerStaffCheck").datagrid('getSelections');
		let row = $("#centerStaffCheck").datagrid('getSelected');
		if (rows.length <= 0) {
			$.messager.alert("提示", "请先选中要修改的行!");
			return false;
		}
		if (rows.length > 1) {
			$.messager.alert("提示", "不能同时修改多行!!");
			return false;
		}
		$('#updateForm').form('load', row);
		$('#update').dialog('open');
	}


// 双击修改用户
function edit(selectRow) {
	var r = $("#centerStaffCheck").datagrid("getRows");// 获取本页有多少行
	row = r[selectRow];// 获得当前行
	$('#updateForm').form('load', row);
	$('#update').dialog('open');
}

function getSelectionsIds() {
	var itemList = $("#centerStaffCheck");
	var sels = itemList.datagrid('getSelections');
	var user_ids = [];
	for ( var i in sels) {
		user_ids.push(sels[i].id);
	}
	user_ids = user_ids.join(",");
	return user_ids;
}
//删除客户
function del() {
	var rows = getSelectionsIds();
	if (rows <= 0) {
		$.messager.alert('提示', '请先选中要删除的行!', 'info');
		return;
	}
	// 向后台返回user_id字符串，需要进行字符串拆分，返回sucess表示删除完成，返回error表示删除失败
	$.messager.confirm("提示", "确定要删除吗？", function(value) {
		if (value) {
			$.post('', {
				"id" : rows
			}, function(data) {// {status: 200, msg: "用户删除成功", data: null}
				if (data.status == 200) {
					$.messager.alert("提示", data.msg, 'info', function() {
						// 刷新表格数据
						$('#centerStaffCheck').datagrid('reload');
						$("#centerStaffCheck").datagrid('clearSelections');

					});
				} else if (data.status == 400) {
					$.messager.alert("提示", data.msg, 'info', function() {
						// 刷新表格数据
						$('#centerStaffCheck').datagrid('reload');
					});
				}
			}, "json");
		}
	});
}
//查询
function query() {
	var formData = getFormData('searchForm');
	$('#centerStaffCheck').datagrid('load', formData);
}
function getFormData(form) {
	var staff_code = $("#staff_codeSearch").val();
	var staff_type = $("#staff_typeSearch").combobox('getValue');
	var staff_job = $("#staff_jobSearch").combobox('getValue');
	if (staff_code == "") {
		user_code = null;
	}
	if (staff_type == "---请选择---") {
		user_type = null;
	}
	if (staff_job == "") {
		staff_job = null;
	}
	var formValues = "staff_code=" + staff_code + "&" + "staff_type=" + staff_type
			+ "&" + "staff_job=" + staff_job;
	// 关于jquery的serialize方法转换空格为+号的解决方法
	formValues = formValues.replace(/\+/g, " "); // g表示对整个字符串中符合条件的都进行替换
	var temp = decodeURIComponent(JSON
			.stringify(conveterParamsToJson(formValues)));
	var queryParam = JSON.parse(temp);
	return queryParam;
}