//应用于autoComplete.js自动补全
var _url='/user/UserName.action';
var _value='user_name';
$(function(){
	//加载表格数据
	$('#grid').datagrid({
		url:'/user/simplelist.action',
		columns:[[
		  		    {field:'user_id',title:'编号',width:100},
		  		    {field:'user_code',title:'账号',width:100},
		  		    {field:'user_name',title:'姓名',width:100},
		  		    {field:'user_sno',title:'学号',width:100},
		  		    {field:'user_major',title:'专业',width:100},
					{field:'-',title:'操作',width:200,formatter: function(value,row,index){
						var oper = "<a href=\"javascript:void(0)\" onclick=\"updatePwd_reset(" + row.user_id + ')">重置密码</a>';
						return oper;
					}}
					]],
		singleSelect: true,
		pagination: true
	});
	//点击查询按钮
	$('#btnSearch').bind('click',function(){
		//把表单数据转换成json对象
		var formData = $('#searchForm').serializeJSON();
		$('#grid').datagrid('load',formData);
	});
	
	//点击重置按钮
	$('#btnReset').bind('click',function(){
		$('#searchForm').form('clear');
	});
	
	//重置密码的窗口
	$('#editDlg').dialog({
		title: '重置密码',//窗口标题
		width: 260,//窗口宽度
		height: 120,//窗口高度
		closed: true,//窗口是是否为关闭状态, true：表示关闭
		modal: true,//模式窗口
		iconCls: 'icon-save',
		buttons: [
		   {
			   text: '保存',
			   iconCls: 'icon-save',
			   handler:function(){
				   var formdata = $('#editForm').serializeJSON();
				   $.ajax({
					   url: '/user/resetPwd.action',
					   data : formdata,
					   dataType: 'json',
					   type: 'post',
					   success:function(rtn){
						   $.messager.alert('提示',rtn.msg,'info',function(){
							   if(rtn.status == 200){
								   $('#editDlg').dialog('close');
							   }
						   });
					   }
				   });
			   }
		   }       
		          
		]
	});
});

//打开重置密码窗口
function updatePwd_reset(user_id){
	
	$('#editDlg').dialog('open');
	//清空表单
	 $('#editForm').form('clear');
	 //加载数据
	 $('#editForm').form('load',{user_id: user_id, user_pwd:""});
}