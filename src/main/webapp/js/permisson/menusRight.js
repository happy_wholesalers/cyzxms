$(function() {
	loadTree();
	// 添加数据对话框
	$('#insertDlg').dialog({    
	    title: '添加数据',    
	    width: 360,    
		height: 200,    
	    closed: true,    
	    modal: true,
	    buttons:[{
			text:'保存',
			handler:function(){
				// 访问后台数据
				insertData();
			}
		},{
			text:'关闭',
			handler:function(){
				// 关闭对话框
				$("#insertDlg").dialog('close');
			}
		}]
	});
	// 修改数据对话框
	$('#updateDlg').dialog({    
		title: '修改数据',    
		width: 400,    
		height: 260,    
		closed: true,    
		modal: true,
		buttons:[{
			text:'保存',
			handler:function(){
				// 访问后台数据
				updateData();
			}
		},{
			text:'关闭',
			handler:function(){
				// 关闭对话框
				$("#updateDlg").dialog('close');
			}
		}]
	});
	// 右键菜单
	$('#mm').menu({    
	    onClick:function(item){    
	    	var rowData = $('#grid').datagrid('getData').rows[0];
	    	switch(item.text) {
	    		case '添加':
	    			$("#insertDlg").dialog('open');
	    			break;
	    		case '修改':
	    			$("#updateDlg").dialog('open');
					// 填充后台数据
					if (rowData.is_parent == 1) {
						rowData.is_parent =  '是';
		        	} else {
		        		rowData.is_parent =  '否';
		        	}
	    			$('#updateForm').form('load', rowData);
	    			break;
	    		case '重命名':
	    			$("#renameDlg").dialog('open');
					// 填充后台数据
	    			$('#renameForm').form('load', rowData);
	    			break;
	    		case '删除':
	    			deleteData(item.id);
	    			break;
	    	}
	    }    
	});
	// 菜单重命名
	$('#renameDlg').dialog({    
		title: '菜单重命名',    
		width: 250,    
		height: 100,    
		closed: true,    
		modal: true,
		buttons:[{
			text:'保存',
			handler:function(){
				// 访问后台数据
				renameMenu();
			}
		},{
			text:'关闭',
			handler:function(){
				// 关闭对话框
				$("#renameDlg").dialog('close');
			}
		}]
	});
	// 图标的下拉框
	$('#icon').combobox({    
//	    url:'combobox_data.json',    
	    valueField:'icon',    
	    textField:'icon'   
	}); 
});

/**
 * 加载两侧菜单
 */ 
function loadTree() {
	$.ajax({
		type: 'POST',
		url: '/menus/findAll.action',
		dataType: 'json',
		success: function(data) {
			var oriDataLeft = JSON.parse(JSON.stringify(data));
			var oriDataRight = JSON.parse(JSON.stringify(data));
			disposeTree('left', oriDataLeft, oriDataRight);
			disposeTree('right', oriDataLeft, oriDataRight);
		}
	});
}
/**
 * 处理两侧菜单
 */
function disposeTree(orientation, data, dataRight) {
	var flag = false;
	// 如果是左侧菜单
	if (orientation == 'left') {
		// 循环一级子菜单
		$.each(data[0].children,function(idx,obj) {
			// 循环二级子菜单
			for (var i = 0; i < obj.children.length; i++) {
				var status = obj.children[i].status;
				// 如果是删除状态，就从数据中移除
				if(status == 0) { 
					obj.children.splice(i, 1);
					i = -1;
				} 
			}
		});
	} else if(orientation == 'right') {
		// 循环一级子菜单
		$.each(dataRight[0].children,function(idx,obj) {
			// 循环二级子菜单
			for (var i = 0; i < obj.children.length; i++) {
				var status = obj.children[i].status;
				// 如果不是删除状态，就从一级菜单中移除
				if(status != 0) { 
					obj.children.splice(i, 1);
					i = -1;
				} else {// 有删除的子菜单
					flag = true;
				}
			}
		});
	}
	// 加载左侧菜单
	$('#tt').tree({
		data: data,
		onContextMenu: function(e, node){
			e.preventDefault();
			var rowData = $('#grid').datagrid('getRows')[0];
			if (rowData != null) {
				// 找到菜单项
				var item = $('#mm').menu('findItem', '删除');
				if (1 == rowData.is_parent) {
					if (item) {
						// 移除菜单项
						$('#mm').menu('removeItem', item.target);
					}
				} else {
					if (item == null) {
						// 追加一个顶部菜单
						$('#mm').menu('appendItem', {
							text: '删除',
							iconCls: 'icon-cut',
							onClick: function(item){
								deleteData(rowData.id);
							}
						});
					}
				}
				// 显示快捷菜单
				$('#mm').menu('show', {
					left: e.pageX,
					top: e.pageY
				});
			} else {
				$.messager.alert("提示", "请选中后操作", 'warning');
			}
		},
		onClick: function(node){
			// 显示子菜单到datagrid
			loadDataGrid('left', node.id);
		}
	});
	// 加载右侧菜单
	if (flag) {
		$('#ttRight').tree({
			data: dataRight,
			onClick: function(node){
				// 显示子菜单到datagrid
				var menuid = -1;
				if (node.id % 100 != 0) {
					menuid = node.id;
				}
				loadDataGrid('right', menuid);
			}
		});
	} else {
		$('#ttRight').tree({
			data: []
		});
	}
}

/**
 * 加载表格数据
 * orientation: 右侧菜单还是左侧菜单
 * menuid：菜单id
 */
function loadDataGrid(orientation, menuid) {
	if (undefined != orientation && undefined != menuid) {
		$('#grid').datagrid({
			url: '/menus/findById.action?menuid=' + menuid,
			columns: [[
				{field:'menuid',title:'编号',width:100},    
				{field:'menuname',title:'名称',width:100},    
				{field:'url',title:'对应URL',width:100},    
				{field:'icon',title:'图标样式',width:100},    
				{field:'pid',title:'上级菜单编号',width:100,},         
				{field:'status',title:'状态',width:100, formatter: function(value,rowData,index) {
					if (rowData.status == 1) {
						return '正常';
					} else {
						return '已删除';
					}
				}},     
				{field:'is_parent',title:'是否为父菜单',width:100, formatter: function(value,rowData,index) {
					if (rowData.is_parent == 1) {
						return '是';
					} else {
						return '否';
					}
				}}]],
				loading:true,
				striped: true,
				rownumbers: true,
				singleSelect: true
		});
		if (orientation == 'left') {
			$('#grid').datagrid({
				toolbar: [{
					iconCls: 'icon-add',
					text: '添加',
					handler: function(){
						// 打开添加窗口
						$("#insertDlg").dialog('open');
					}
				},'-',{
					iconCls: 'icon-cut',
					text: '删除',
					handler: function(){
						// 删除
						var rowData = $('#tt').tree('getSelected');
						if (null != rowData) {
							if (rowData.id % 100 == 0) {
								$.messager.alert("提示", "父级菜单不可删除", 'error');
							} else {
								deleteData(rowData.id);
							}
						} else {
							$.messager.alert("提示", "请选中要删除数据", 'error');
						}
					}
				},'-',{
					iconCls: 'icon-save',
					text: '修改',
					handler: function(){
						var rowData = $('#grid').datagrid('getSelected');
						if (null != rowData) {
							// 打开修改窗口
							$('#updateDlg').dialog('open');
							// 填充后台数据
							if (rowData.is_parent == 1) {
								rowData.is_parent =  '是';
							} else {
								rowData.is_parent =  '否';
							}
							$('#updateForm').form('load', rowData);
						} else {
							$.messager.alert("提示", "请选中要修改的行", 'error');
						}
					}
				}],
				onDblClickRow:function(rowIndex, rowData) {
					// 打开修改窗口
					$('#updateDlg').dialog('open');
					// 填充后台数据
					if (rowData.is_parent == 1) {
						rowData.is_parent =  '是';
					} else {
						rowData.is_parent =  '否';
					}
					$('#updateForm').form('load', rowData);
				}
			});
		} else if (orientation == 'right') {
			$('#grid').datagrid({
				toolbar: [{
					iconCls: 'icon-undo',
					text: '还原',
					handler: function(){
						var rowData = $('#ttRight').tree('getSelected');
						if (null != rowData) {
							$.ajax({
								type: 'POST',
								url: '/menus/updateById.action',
								data: {'menuid': rowData.id, 'status': 1},
								dataType: 'json',
								success: function(data) {
									// 如果结果码是200， 提示添加成功，否则，添加失败
									if (data.status == 200) {
										$.messager.alert("提示", data.msg, 'info', function() {
											// 刷新表格数据
											$('#grid').datagrid('deleteRow', 0);
											loadDataGrid();
											// 刷新树形菜单
											loadTree();
										});
									} else if (data.status == 400) {
										$.messager.alert("提示", data.msg, 'info');
									}
								}
							});
						} else {
							$.messager.alert("提示", "请选择表格中的数据进行操作", 'error');
						}
					}
				}]
			});
		}
	}
}

/**
 * 添加数据
 */
function insertData() {
	var rowData = $('#grid').datagrid('getData').rows[0];
	// 提交添加数据的表单
	$('#insertForm').form('submit', {
		url: '/menus/insert.action',
		onSubmit: function(param){    
			param.pid = rowData.menuid;
			param.is_parent = rowData.is_parent;
		},
		success:function(data){ 
			data = eval('(' + data + ')');
			// 如果结果码是200， 提示添加成功，否则，添加失败
			if (data.status == 200) {
				$.messager.alert("提示", data.msg, 'info', function() {
					// 刷新表格数据
					$('#grid').datagrid('reload');
					// 刷新树形菜单
					loadTree();
					// 关闭对话框
					$('#insertDlg').dialog('close');
					// 清除表单数据
					$('#insertForm').form('clear');
				});
			} else if (data.status == 400) {
				$.messager.alert("提示", data.msg, 'info');
			}
		}
	});
}

/**
 * 删除数据
 */
function deleteData(menuid) {
	var gridData = $('#grid').datagrid('getData').rows[0];
	if (gridData) {
		if (0==gridData.status) {
			$.messager.alert("警告", '不能重复删除', 'warning');
			return;
		}
		$.messager.confirm('警告', '确认要删除'+gridData.menuname+'菜单吗?', function(r){
			if (r){
				$.ajax({
					type: 'POST',
					url: '/menus/deleteById.action',
					data: {'menuid': menuid},
					dataType: 'json',
					success: function(data) {
						// 如果结果码是200， 提示添加成功，否则，添加失败
						if (data.status == 200) {
							$.messager.alert("提示", data.msg, 'info', function() {
								// 刷新表格数据
								$('#grid').datagrid('deleteRow', 0);
								loadDataGrid();
								// 刷新树形菜单
								loadTree();
							});
						} else if (data.status == 400) {
							$.messager.alert("提示", data.msg, 'info');
						}
					}
				});
			}
		});
	}
}

/**
 * 修改数据
 */
function updateData() {
	// 提交修改后的数据到后台
	$('#updateForm').form('submit', {
		url: '/menus/updateById.action',
		success:function(data){ 
			data = eval('(' + data + ')');
			// 如果结果码是200， 提示添加成功，否则，添加失败
			if (data.status == 200) {
				$.messager.alert("提示", data.msg, 'info', function() {
					// 刷新表格数据
					$('#grid').datagrid('reload');
					// 刷新树形菜单
					loadTree();
					// 关闭对话框
					$('#updateDlg').dialog('close');
					// 清除表单数据
					$('#updateForm').form('clear');
				});
			} else if (data.status == 400) {
				$.messager.alert("提示", data.msg, 'info');
			}
		}
	});
}
/**
 * 重命名菜单
 */
function renameMenu() {
	// 提交修改后的数据到后台
	$('#renameForm').form('submit', {
		url: '/menus/updateById.action',
		onSubmit: function(param){    
			param.menuid = $('#tt').tree('getSelected').id;
		},
		success:function(data){ 
			data = eval('(' + data + ')');
			// 如果结果码是200， 提示添加成功，否则，添加失败
			if (data.status == 200) {
				$.messager.alert("提示", data.msg, 'info', function() {
					// 刷新表格数据
					$('#grid').datagrid('reload');
					// 刷新树形菜单
					loadTree();
					// 关闭对话框
					$('#renameDlg').dialog('close');
					// 清除表单数据
					$('#renameForm').form('clear');
				});
			} else if (data.status == 400) {
				$.messager.alert("提示", data.msg, 'info');
			}
		}
	});
}