//应用于autoComplete.js自动补全
var _url='/user/UserName.action';
var _value='user_name';
$(function() {
	$('#user_grid').datagrid({
		url : '/user/simplelist.action',
		columns : [ [ {
			field : 'user_id',
			title : '编号',
			width : 50
		}, {
			field : 'user_code',
			title : '账号',
			width : 100
		}, {
			field : 'user_name',
			title : '姓名',
			width : 100
		}, {
			field : 'user_sno',
			title : '学号',
			width : 100
		}, {
			field : 'user_major',
			title : '专业',
			width : 120
		} ] ],
		pagination : true,
		pageSize : 50,
		singleSelect : true,
		loading : true,
		onClickRow : function(rowIndex, rowData) {
			$('#tree').tree({
				url : '/user/findUserRole.action?id=' + rowData.user_id,
				animate : true,
				checkbox : true
			});
		}
	});
	$('#btnSave').bind("click", function() {
		// 角色id
		var user_id = $('#user_grid').datagrid("getSelected").user_id;
		// 选择的节点
		var nodes = $('#tree').tree('getChecked');
		var checkedIds = new Array();
		$.each(nodes, function(i, node) {
			checkedIds.push(node.id);
		})
		// 权限菜单对应id
		checkedIds = checkedIds.join(",");
		var formData = {
			'id' : user_id,
			'checkedIds' : checkedIds
		};
		$.ajax({
			url : '/user/updateUserRole.action',
			data : formData,
			dataType : 'json',
			type : 'post',
			success : function(data) {
				$.messager.alert("提示", data.msg, 'info', function() {
				});
			}
		});
	});
	//点击查询按钮
	$('#btnSearch').bind('click',function(){
		//把表单数据转换成json对象
		var formData = $('#searchForm').serializeJSON();
		$('#user_grid').datagrid('load',formData);
	});
	
	//点击重置按钮
	$('#btnReset').bind('click',function(){
		$('#searchForm').form('clear');
	});
})
