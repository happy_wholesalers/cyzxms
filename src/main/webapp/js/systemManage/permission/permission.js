$(function() {
	$("#addPermissionitms").combobox({
		url : '/permission/listName.action',
		valueField : 'menuid',
		textField : 'menuname',
		panelHeight : '150',
	});
	$("#updatePermissionitms").combobox({
		url : '/permission/listName.action',
		valueField : 'menuid',
		textField : 'menuname',
		panelHeight : '150',
	});
	$("#addRuTyId").combobox({
		url : '/permission/listRuTyId.action',
		valueField : 'ru_ty_id',
		textField : 'ru_ty_id',
		panelHeight : '150',
	});
	$('#add').window({
		title : '添加权限',
		closed : false,
		cache : false,
		collapsible : true,
		maximizable : false, // 不可最大化
		resizable : false,
		shadow : true,
		left : 500,
		top : 80,
		modal : true, // 模态窗
	});
	$('#update').window({
		title : '修改权限',
		closed : false,
		cache : false,
		collapsible : true,
		maximizable : false, // 不可最大化
		resizable : false,
		shadow : true,
		left : 500,
		top : 80,
		modal : true, // 模态窗
	});
	$('#add').window('close');
	$('#update').window('close');
	$('#permission').datagrid({
		title : '权限管理',
		iconCls : 'icon-tip',
		loadMsg : '数据加载中...',
		nowrap : false,
		striped : true,
		collapsible : true,
		url : '/permission/list.action',
		sortName : 'role_id',
		sortOrder : 'desc',
		remoteSort : false,
		idField : 'role_id',
		frozenColumns : [ [ {
			field : 'ck',
			checkbox : true
		}, {
			title : '编号',
			field : 'role_id',
			sortable : true
		} ] ],
		columns : [ [ {
			field : 'ru_ty_id',
			title : '角色',
		}, {
			field : 'menuname',
			title : '权限',
		} ] ],
		onDblClickRow : function(selectRow) {
			edit(selectRow);
		},
		pagination : true,
		rownumbers : true,
		toolbar : [ {
			id : 'btnadd',
			text : '添加',
			iconCls : 'icon-add',
			handler : function() {
				$('#btnsave').linkbutton('enable');
				$('#addForm').form('clear');
				$('#add').window('open');
			}
		}, '-', {
			id : 'btnsave',
			text : '修改',
			iconCls : 'icon-save',
			handler : function(selectRow) {
				$('#btnsave').linkbutton('enable');
				btnEdit(selectRow);
			}
		}, '-', {
			id : 'btncut',
			text : '删除',
			iconCls : 'icon-cut',
			handler : function() {
				$('#btnsave').linkbutton('enable');
				del();
			}
		} ]
	});
	// 添加用户
	$("#addPermission").click(function() {
		$('#addForm').form('submit', {
			url : '/permission/add.action',
			onSubmit : function() {
				
			},
			success : function(data) {
				var jsonData = JSON.parse(data);
				if (jsonData.status == 200) {
					$.messager.alert("提示", jsonData.msg, 'info', function() {
						// 刷新表格数据
						$('#add').window('close');
						$('#permission').datagrid('reload');
					});
				}
				if (jsonData.status == 400) {
					$.messager.alert("提示", jsonData.msg, 'info', function() {
					});
				}
			}
		});
	});
	$("#updatePermission").click(function() {
		$('#updateForm').form('submit', {
			url : '/permission/update.action',
			onSubmit : function() {
			},
			success : function(data) {// {"status":200,"msg":"用户信息更新成功","data":null}
				var jsonData = JSON.parse(data);
				if (jsonData.status == 200) {
					$.messager.alert("提示", jsonData.msg, 'info', function() {
						// 刷新表格数据
						$('#update').window('close');
						$('#permission').datagrid('reload');
					});
				}
				if (jsonData.status == 400) {
					$.messager.alert("提示", jsonData.msg, 'info', function() {
					});
				}
			}
		});
	})

	function btnEdit(selectRow) {
		let rows = $("#permission").datagrid('getSelections');
		let row = $("#permission").datagrid('getSelected');
		if (rows.length <= 0) {
			$.messager.alert("提示", "请先选中要修改的行!");
			return false;
		}
		if (rows.length > 1) {
			$.messager.alert("提示", "不能同时修改多行!!");
			return false;
		}
		$('#updateForm').form('load', row);
		$('#update').dialog('open');
	}

	function edit(selectRow) {
		var r = $("#permission").datagrid("getRows");// 获取本页有多少行
		row = r[selectRow];// 获得当前行
		$('#updateForm').form('load', row);
		$('#update').dialog('open');
	}
	function getSelectionsIds() {
		var itemList = $("#permission");
		var sels = itemList.datagrid('getSelections');
		var role_id = [];
		for ( var i in sels) {
			role_id.push(sels[i].role_id);
		}
		role_id = role_id.join(",");
		return role_id;
	}
	function del() {
		var rows = getSelectionsIds();
		if (rows <= 0) {
			$.messager.alert('提示', '请先选中要删除的行!', 'info');
			return;
		}
		// 向后台返回user_id字符串，需要进行字符串拆分，返回sucess表示删除完成，返回error表示删除失败
		$.messager.confirm("提示", "确定要删除吗？", function(value) {
			if (value) {
				$.post('/permission/delete.action', {
					"role_id" : rows
				}, function(data) {// {status: 200, msg: "删除成功", data: null}
					if (data.status == 200) {
						$.messager.alert("提示", data.msg, 'info', function() {
							// 刷新表格数据
							$('#permission').datagrid('reload');
							$("#permission").datagrid('clearSelections');
						});
					}
					if (data.status == 400) {
						$.messager.alert("提示", data.msg, 'info', function() {
							// 刷新表格数据
							$('#permission').datagrid('reload');
						});
					}
				}, "json");
			}
		});
	}
})