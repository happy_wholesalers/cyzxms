$(function() {
	var body;
	// 初始化表格
	$('#grid').datagrid({
		url : '/protocol/selectAll.action',
	    columns:[[    
	        {field:'a_b_companyname',title:'公司名称',width:100}  
	    ]],
	    onDblClickRow: function (rowIndex, rowData) {
	    	// 显示协议[获取iframe中的元素]
	    	$("#printFrame").contents().find("#protocolForm").form('load', rowData);
	    	// 打开协议显示窗口
	    	$('#protocolWin').window('open');
	    },
	    striped: true,
	    pagination: true,
	    rownumbers: true,
	    singleSelect: true
	});
	// 初始化协议显示窗口
	$('#protocolWin').window({
		title: '入驻协议',    
	    width: 1000,    
	    height: 500,    
	    closed: true,    
	    cache: false,    
	    modal: true 
	});
});