//应用于autoComplete.js自动补全
var _url='/user/UserName.action';
var _value='user_name';

$(function() {
	$("#queryCompany").combobox({
		url : '/company/listCname.action',
		valueField : 'company_id',
		textField : 'company_name',
		panelHeight : '150',
	});
	$("#updateCompany").combobox({
		url : '/company/listCname.action',
		valueField : 'company_id',
		textField : 'company_name',
		panelHeight : '150',
	});
	$("#addCompany").combobox({
		url : '/company/listCname.action',
		valueField : 'company_id',
		textField : 'company_name',
		panelHeight : '150',
	});
	
	// 设置弹出窗口的属性
	$('#w').window({
		title : '修改用户',
		closed : false,
		cache : false,
		collapsible : true,
		maximizable : false, // 不可最大化
		resizable : false,
		shadow : true,
		left : 500,
		top : 80,
		modal : true, // 模态窗
	});
	$('#e').window({
		title : '添加用户',
		closed : false,
		cache : false,
		collapsible : true,
		maximizable : false, // 不可最大化
		resizable : false,
		shadow : true,
		left : 500,
		top : 80,
		modal : true, // 模态窗
	});
	$('#w').window('close');
	$('#e').window('close');
	$('#grid').datagrid({
		title : '用户管理',
		iconCls : 'icon-tip',
		loadMsg : '数据加载中...',
		nowrap : false,
		striped : true,
		collapsible : true,
		url : '/user/list.action',
		sortName : 'user_id',
		sortOrder : 'desc',
		remoteSort : false,
		idField : 'user_id',
		frozenColumns : [ [ {
			field : 'ck',
			checkbox : true
		}, {
			title : '编号',
			field : 'user_id',
			sortable : true
		} ] ],
		columns : [ [ {
			field : 'user_name',
			title : '用户名',
		}, {
			field : 'user_pwd',
			title : '密  码',
		}, {
			field : 'user_image',
			title : '相 片',
		}, {
			field : 'user_type',
			title : '用户类型',
		}, {
			field : 'user_tel',
			title : '联系电话',
		}, {
			field : 'user_wx',
			title : '微信或QQ',
		}, {
			field : 'user_sno',
			title : '学号',
		}, {
			field : 'user_card',
			title : '身份证',
		}, {
			field : 'user_politics',
			title : '政治面貌',
		}, {
			field : 'user_sex',
			title : '性别',
		}, {
			field : 'user_enterSchool',
			title : '入校时间',
		}, {
			field : 'user_major',
			title : '专业',
		}, {
			field : 'user_homeaddr',
			title : '家庭住址',
		}, {
			field : 'user_facilityid',
			title : '所属院系',
		}, {
			field : 'company',
			title : '所属公司',
			formatter : function(value, row, index) {// value：当前列对应字段值。row：当前的行记录数据。index：当前的行下标。
				if (row.company) {
					return row.company.company_name;
				} else {
					return value;
				}
			}
		} ] ],
		onDblClickRow : function(selectRow) {
			edit(selectRow);
		},
		pagination : true,
		rownumbers : true,
		toolbar : [ {
			id : 'btnadd',
			text : '添加',
			iconCls : 'icon-add',
			handler : function() {
				// 打开前清空表单
				$('#adduserForm').form('clear');
				// 打开表单窗口
				$('#e').window('open');
			}
		}, '-', {
			id : 'btnsave',
			text : '修改',
			iconCls : 'icon-save',
			handler : function(selectRow) {
				btnEdit(selectRow);
			}
		}, '-', {
			id : 'btncut',
			text : '删除',
			iconCls : 'icon-cut',
			handler : function() {
				del();
			}
		} ]
	});
	$("#grid").datagrid('hideColumn', 'user_pwd');
	$("#grid").datagrid('hideColumn', 'user_image');
	$("#grid").datagrid('hideColumn', 'user_card');
	$("#grid").datagrid('hideColumn', 'user_politics');
	$("#grid").datagrid('hideColumn', 'user_major');
	$("#grid").datagrid('hideColumn', 'user_homeaddr');
	$("#grid").datagrid('hideColumn', 'user_facilityid');
	var p = $('#grid').datagrid('getPager');
	$(p).pagination({
		onBeforeRefresh : function() {
		}
	});

	function btnEdit(selectRow) {
		let rows = $("#grid").datagrid('getSelections');
		let row = $("#grid").datagrid('getSelected');
		if (rows.length <= 0) {
			$.messager.alert("提示", "请先选中要修改的行!");
			return false;
		}
		if (rows.length > 1) {
			$.messager.alert("提示", "不能同时修改多行!!");
			return false;
		}
		img("imgHeadPhoto1", row.user_image);
		$('#updateUserForm').form('load', row);
		$('#w').dialog('open');
	}

	// 添加用户
	$("#addUser").click(function() {
		$('#adduserForm').form('submit', {
			url : '/user/add.action',
			success : function(data) {
				var jsonData = JSON.parse(data);
				if (jsonData.status == 200) {
					$.messager.alert("提示", jsonData.msg, 'info', function() {
						// 刷新表格数据
						$('#grid').datagrid('reload');
						$('#e').window('close');
						cleanImg("imgHeadPhoto2");
					});

				} else if (jsonData.status == 400) {
					$.messager.alert("提示", jsonData.msg, 'info', function() {
					});
				}
			}
		});
	});
	// 修改用户
	$("#updateUser").click(function() {
		$('#updateUserForm').form('submit', {
			url : '/user/update.action',
			onSubmit : function() {
			},
			success : function(data) {// {"status":200,"msg":"用户信息更新成功","data":null}
				var jsonData = JSON.parse(data);
				console.log(jsonData);
				if (jsonData.status == 200) {
					$.messager.alert("提示", jsonData.msg, 'info', function() {
						// 刷新表格数据
						$('#grid').datagrid('reload');
						$('#w').window('close');
						cleanImg("imgHeadPhoto1");
					});
				} else if (jsonData.status == 400) {
					$.messager.alert("提示", jsonData.msg, 'info', function() {
					});
				}
			}
		});
	})
});

// 双击修改用户
function edit(selectRow) {
	var r = $("#grid").datagrid("getRows");// 获取本页有多少行
	row = r[selectRow];// 获得当前行
	img("imgHeadPhoto1", row.user_image);
	$('#updateUserForm').form('load', row);
	$('#w').dialog('open');
}

// 删除客户

function del() {
	var rows = getSelectionsIds();
	if (rows <= 0) {
		$.messager.alert('提示', '请先选中要删除的行!', 'info');
		return;
	}
	// 向后台返回user_id字符串，需要进行字符串拆分，返回sucess表示删除完成，返回error表示删除失败
	$.messager.confirm("提示", "确定要删除吗？", function(value) {
		if (value) {
			$.post('/user/delete.action', {
				"user_id" : rows
			}, function(data) {// {status: 200, msg: "用户删除成功", data: null}
				if (data.status == 200) {
					$.messager.alert("提示", data.msg, 'info', function() {
						// 刷新表格数据
						$('#grid').datagrid('reload');
						$("#grid").datagrid('clearSelections');

					});
				} else if (data.status == 400) {
					$.messager.alert("提示", data.msg, 'info', function() {
						// 刷新表格数据
						$('#grid').datagrid('reload');
					});
				}
			}, "json");
		}
	});
}

function getSelectionsIds() {
	var itemList = $("#grid");
	var sels = itemList.datagrid('getSelections');
	var user_ids = [];
	for ( var i in sels) {
		user_ids.push(sels[i].user_id);
	}
	user_ids = user_ids.join(",");
	return user_ids;
}

// 查询
function query() {
	var formData = getFormData('searchForm');
	$('#grid').datagrid('load', formData);
}
function getFormData(form) {
	var user_name = $("#user_name").val();
	var user_type = $("#user_type").combobox('getValue');
	var queryCompany = $("#queryCompany").combobox('getValue');
	var user_politics = $("#user_politics").combobox('getValue');
	if (user_name == "") {
		user_name = null;
	}
	if (user_type == "---请选择---") {
		user_type = null;
	}
	if (queryCompany == "") {
		queryCompany = null;
	}
	if (user_politics == "---请选择---") {
		user_politics = null;
	}
	var formValues = "user_name=" + user_name + "&" + "user_type=" + user_type
			+ "&" + "user_companyid=" + queryCompany + "&" + "user_politics="
			+ user_politics;
	// 关于jquery的serialize方法转换空格为+号的解决方法
	formValues = formValues.replace(/\+/g, " "); // g表示对整个字符串中符合条件的都进行替换
	var temp = decodeURIComponent(JSON
			.stringify(conveterParamsToJson(formValues)));
	var queryParam = JSON.parse(temp);
	return queryParam;
}
function conveterParamsToJson(paramsAndValues) {
	var jsonObj = {};
	var param = paramsAndValues.split("&");
	for (var i = 0; param != null && i < param.length; i++) {
		var para = param[i].split("=");
		jsonObj[para[0]] = para[1];
	}
	return jsonObj;
}
// 清除图片路径
function cleanImg(fileId) {
	var fId = "#" + fileId;
	$(fId).attr("src", "");
}

function img(id, image) {
	if (image == "" || image == null) {
		image = "/images/default.jpg";
	}
	var fId = "#" + id;
	$(fId).attr("src", image);
}
