//添加图片	
	$(function() {
		//1.读取该公司营业执照信息存入imgPath
		$.post("/company/getBusinessLicence.action",
				function(data){
			if(data.status==200){
				$("#licence").attr("src",data.data);
			}else if(data.status==400){
				$.messager.alert("提示",data.msg,'info',function(){
					$("#licence").attr("src",'');
				});
			}
	    },"json");
		//2.上传图片功能
		$("#submitImg").click(function() {	
			$('#licenceImgForm').form('submit', {
				url : '/company/businessLicence.action',
				success : function(data) {
					var jsonData = JSON.parse(data);
					if (jsonData.status == 200) {
						$.messager.alert("提示",jsonData.msg,'info',function(){
							$("#licence").attr("src",jsonData.data);							
							alert(jsonData.data);
						});
					}
					if (jsonData.status == 400) {
						$.messager.alert("提示",jsonData.msg,'info',function(){
							$("#licence").attr("src",'');
						});
					}
				}
			});
		});
		
	})