$(function () {
	
	
	
	//默认关闭
	$('#w').window('close');
	$('#e').window('close');
    $('#test').datagrid({
        title: '人员统计',
        iconCls: 'icon-tip',
        nowrap: false,
        striped: true,
        collapsible: true,
        url: '../json/personCount.json',
        sortName: 'e_id',
        sortOrder: 'desc',
        remoteSort: false,
        idField: 'e_id',
        frozenColumns: [[{
            field: 'ck',
            checkbox: true
        }, {
            title: '编号',
            field: 'e_id',
            sortable: true
        }]],
        columns: [[{
            field: 'e_name',
            title: '员工名'
        }, {
            field: 'e_sex',
            title: '性别'
        }, {
            field: 'e_fid',
            title: '所属院系'
        }, {
            field: 'e_tel',
            title: '联系方式'
        }, {
            field: 'e_cid',
            title: '所属公司'
        }, {
            field: 'e_uid',
            title: '所属负责人'
        }, {
            field: 'e_position',
            title: '职位'
        }
        ]],
        pagination: true,
        rownumbers: true,
        toolbar: [{
            id: 'btnadd',
            text: '添加',
            iconCls: 'icon-add',
            handler: function () {
                $('#btnsave').linkbutton('enable');
                // 打开前清空表单
                $('#addPerson').form('clear');
                // 打开表单窗口
                $('#e').window('open');
            }
        }, '-', {
            id: 'btnsave',
            text: '修改',
            iconCls: 'icon-save',
            handler: function (selectRow) {
                $('#btnsave').linkbutton('enable');
                btnEdit(selectRow);
            }
        }, '-', {
            id: 'btncut',
            text: '删除',
            iconCls: 'icon-cut',
            handler: function () {
                $('#btnsave').linkbutton('enable');
                del();
            }
        }]
    });
    var p = $('#test').datagrid('getPager');
    $(p).pagination({
        onBeforeRefresh: function () {
        }
    });
    
 
    function btnEdit(selectRow) {
        let rows = $("#test").datagrid('getSelections');
        let row = $("#test").datagrid('getSelected');
        if (rows.length <= 0) {
            $.messager.alert("提示", "请先选中要修改的行!");
            return false;
        }
        if (rows.length > 1) {
            $.messager.alert("提示", "不能同时修改多行!!");
            return false;
        }
        $('#updateUserForm').form('load', row);
        $('#w').dialog('open');
    }

    //添加用户
    $("#addUser").click(function () {
        $('#adduserForm').form('submit', {
            url: '/user/add.action',
            success: function (data) {
                if (data.status == 200) {
                    $.messager.alert(data.msg);
                }
                if (data.status == 400) {
                    $.messager.alert(data.msg);
                }
            }
        });
    });

    // 修改用户
    $("#updateUser").click(function () {
        $('#updateUserForm').form('submit', {
            url: '/user/update.action',
            onSubmit: function () {
            },
            success: function (data) {//{"status":200,"msg":"用户信息更新成功","data":null}
                var jsonData = JSON.parse(data);
                console.log(jsonData);
                if (jsonData.status == 200) {
                    $.messager.alert("提示", jsonData.msg, 'info', function () {
                        //成功的话，我们要关闭窗口
                        //$('#test').datagrid('close');
                        //刷新表格数据
                        $('#test').datagrid('reload');
                    });
                }
                if (jsonData.status == 400) {
                    $.messager.alert("提示", jsonData.msg, 'info', function () {
                    });
                }
            }
        });
    })
    
    
});

function resize() {
    $('#test').datagrid('resize', {
        width: 700,
        height: 400
    });
}
function getSelected() {
    var selected = $('#test').datagrid('getSelected');
    if (selected) {
        alert(selected.code + ":" + selected.name + ":" + selected.addr + ":"
            + selected.col4);
    }
}
function getSelections() {
    var ids = [];
    var rows = $('#test').datagrid('getSelections');
    for (var i = 0; i < rows.length; i++) {
        ids.push(rows[i].code);
    }
    alert(ids.join(':'));
}
function clearSelections() {
    $('#test').datagrid('clearSelections');
}
function selectRow() {
    $('#test').datagrid('selectRow', 2);
}
function selectRecord() {
    $('#test').datagrid('selectRecord', '002');
}
function unselectRow() {
    $('#test').datagrid('unselectRow', 2);
}
function mergeCells() {
    $('#test').datagrid('mergeCells', {
        index: 2,
        field: 'addr',
        rowspan: 2,
        colspan: 2
    });
}

// 双击修改用户
function edit(selectRow) {
    var r = $("#test").datagrid("getRows");// 获取本页有多少行
    row = r[selectRow];// 获得当前行
    $('#updateUserForm').form('load', r[selectRow]);
    $('#w').dialog('open');
}

// 删除客户

function del() {
    var rows = getSelectionsIds();
    if (rows <= 0) {
        $.messager.alert('提示', '请先选中要删除的行!', 'info');
        return;
    }
    // 向后台返回user_id字符串，需要进行字符串拆分，返回sucess表示删除完成，返回error表示删除失败
    $.messager.confirm("提示", "确定要删除吗？", function (value) {
        if (value) {
            $.post('/user/delete.action', { "user_id": rows },
                function (data) {//{status: 200, msg: "用户删除成功", data: null}
                    if (data.status == 200) {
                        $.messager.alert("提示", data.msg, 'info', function () {
                            //成功的话，我们要关闭窗口
                            $('#editDlg').dialog('close');
                            //刷新表格数据
                            $('#test').datagrid('reload');
                        });
                    }
                    if (data.status == 400) {
                        $.messager.alert("提示", data.msg, 'info', function () {
                            //成功的话，我们要关闭窗口
                            $('#editDlg').dialog('close');
                            //刷新表格数据
                            $('#test').datagrid('reload');
                        });
                    }
                }, "json");
        }
    });
}

function getSelectionsIds() {
    var itemList = $("#test");
    var sels = itemList.datagrid("getSelections");
    var user_ids = [];
    for (var i in sels) {
        user_ids.push(sels[i].user_id);
    }
    user_ids = user_ids.join(",");
    return user_ids;
}

// 查询
function query() {
    var formData = getFormData('searchForm');
    $.post("/user/list.action", formData, function (data) {
        $('#test').datagrid('load', data);
    });
}
function getFormData(form) {
    var user_code = $("#user_code").val();
    var user_type = $("#user_type").combobox('getValue');
    var queryCompany = $("#queryCompany").combobox('getValue');
    var user_politics = $("#user_politics").combobox('getValue');
    var formValues = "user_code=" + user_code + "&" + "user_type=" + user_type
        + "&" + "user_companyid=" + queryCompany + "&" + "user_politics="
        + user_politics;
    // 关于jquery的serialize方法转换空格为+号的解决方法
    formValues = formValues.replace(/\+/g, " "); // g表示对整个字符串中符合条件的都进行替换
    var temp = decodeURIComponent(JSON
        .stringify(conveterParamsToJson(formValues)));
    var queryParam = JSON.parse(temp);
    return queryParam;
}
function conveterParamsToJson(paramsAndValues) {
    var jsonObj = {};
    var param = paramsAndValues.split("&");
    for (var i = 0; param != null && i < param.length; i++) {
        var para = param[i].split("=");
        jsonObj[para[0]] = para[1];
    }
    return jsonObj;
}
