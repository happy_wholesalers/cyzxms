/**
 * 公司入驻申请统计
 * 
 * @returns
 */
$(function() {
	$('#dg').datagrid({
		title : '当前公司',
		url : '/company/listCurrentCompanyByPage.action',
		frozenColumns : [ [ {
			field : 'ck',
			checkbox : true
		}, {
			title : '编号',
			field : 'company_id',
			width : 80,
			sortable : true
		} ] ],
		columns : [ [ {
			field : 'company_name',
			title : '企业名称'
		}, {
			field : 'company_enterdate',
			title : '入驻时间'
		},  {
			field : 'company_address',
			title : '注册地址'
		}, {
			field : 'company_joindate',
			title : '注册时间'
		},  {
			field : 'company_area',
			title : '场地面积'
		}, {
			field : 'company_field',
			title : '技术领域'
		}, {
			field : 'company_product',
			title : '主要产品'
		}, {
			field : 'company_istechnology',
			title : '是否科技型中小企业',
			formatter : function(value, row, index) {
				if (row.company_istechnology == 1) {
					return '是';
				} else {
					return '否';
				}
			}
		}, {
			field : 'company_islicense',
			title : '是否有营业执照',
			formatter : function(value, row, index) {
				if (row.company_islicense == 1) {
					return '是';
				} else {
					return '否';
				}
			}
		}, {
			field : 'company_leader',
			title : '负责人'
		}, {
			field : 'company_tel',
			title : '电话'
		},{
			field : 'company_departmentid',
			title : '院系id'
		}, {
			field : 'company_tel2',
			title : '备用电话'
		} ] ],
		toolbar : [ {
			id : 'btnAdd',
			text : '添加',
			iconCls : 'icon-add',
			handler : function() {
				$('#addCompanyWin').window('open');
			}
		}, {
			id : 'btnUpdate',
			text : '修改',
			iconCls : 'icon-edit',
			handler : function(selectRow) {
				btnShowEditWindow(selectRow);
			}
		}, {
			id : 'btnCut',
			text : '删除',
			iconCls : 'icon-cut',
			handler : function() {
				deleteCompanyInfo();
			}
		} ],
		pagination : true,
		rownumbers : true,
		striped : true,
		loadMsg : '数据加载中...',
		collapsible : true,
		onDblClickRow : function(selectRow) {
			dblShowEditWindow(selectRow);
		}
	});

	// 入驻信息修改窗口
	$('#updateCompanyWin').window({
		title : '修改入驻信息',
		closed : false,
		cache : false,
		collapsible : true,
		maximizable : false, // 不可最大化
		resizable : false,
		shadow : true,
		modal : true, // 模态窗
	});
	// 默认关闭修改入驻信息窗口
	$('#updateCompanyWin').window('close');
	// 通过双击当前行，弹出当前行入驻信息修改窗口
	function dblShowEditWindow(selectRow) {
		var r = $("#dg").datagrid("getRows"); // 获取本页有多少行
		var row = r[selectRow]; // 获得当前行
		$('#updateEnterApplyForm').form('load', row);
		$('#updateCompanyWin').window('open');
	}

	// 通过修改按钮，弹出当前行入住信息修改窗口
	function btnShowEditWindow(selectRow) {
		let rows = $("#dg").datagrid('getSelections');
		let row = $("#dg").datagrid('getSelected');
		if (rows.length <= 0) {
			$.messager.alert("提示", "请先选中要修改的行!");
			return false;
		}
		if (rows.length > 1) {
			$.messager.alert("提示", "不能同时修改多行!!");
			return false;
		}
		$('#updateEnterApplyForm').form('load', row);
		$('#updateCompanyWin').window('open');
	}
	// 通过修改按钮，修改选中行中的数据
	$('#updateEnterApply').click(function() {
		$('#updateEnterApplyForm').form('submit', {
			url : '/company/updateCompanyInfo.action',
			onSubmit : function() {
			},
			success : function(data) { // {"status":200,"msg":"用户信息更新成功","data":null}
				var jsonData = JSON.parse(data);
				console.log(jsonData);
				if (jsonData.status == 200) {
					$.messager.alert("提示", jsonData.msg, 'info', function() {
						// 刷新表格数据
						$('#dg').datagrid('reload');
						$('#updateCompanyWin').window('close');
					});
				}
				if (jsonData.status == 400) {
					$.messager.alert("提示", jsonData.msg, 'info');
				}
			}
		});
	});

	// 获取公司id
	function getSelectionsIds() {
		var itemList = $("#dg");
		var sels = itemList.datagrid('getSelections');
		var company_ids = [];
		for ( var i in sels) {
			company_ids.push(sels[i].company_id);
		}
		company_ids = company_ids.join(",");
		return company_ids;
	}

	// 根据公司id删除公司信息
	function deleteCompanyInfo() {
		var rows = getSelectionsIds();
		if (rows <= 0) {
			$.messager.alert('提示', '请先选中要删除的行!', 'info');
			return;
		}
		// 向后台返回company_id字符串，需要进行字符串拆分，返回sucess表示删除完成，返回error表示删除失败
		$.messager.confirm("提示", "确定要删除吗？", function(value) {
			if (value) {
				$.post('/company/delete/' + rows + '.action', function(data) { // {status:
					// 200,
					// msg:
					// "用户删除成功",
					// data:
					// null}
					if (data.status == 200) {
						$.messager.alert("提示", data.msg, 'info', function() {
							// 刷新表格数据
							$('#dg').datagrid('reload');
							$("#dg").datagrid('clearSelections');
						});
					}
					if (data.status == 400) {
						$.messager.alert("提示", data.msg, 'info', function() {
							// 刷新表格数据
							$('#dg').datagrid('reload');
						});
					}
				}, "json");
			}
		});
	}
	// 入驻信息添加窗口
	$('#addCompanyWin').window({
		title : '添加入驻信息',
		closed : false,
		cache : false,
		collapsible : true,
		maximizable : false, // 不可最大化
		resizable : false,
		shadow : true,
		modal : true, // 模态窗
	});
	// 默认关闭入驻信息添加窗口
	$('#addCompanyWin').window('close');
	// 通过修改按钮，修改选中行中的数据
	$('#addEnterApply').click(function() {
		$('#addEnterApplyForm').form('submit', {
			url : '/company/addCompanyInfo.action',
			onSubmit : function() {
			},
			success : function(data) { // {"status":200,"msg":"用户信息更新成功","data":null}
				var jsonData = JSON.parse(data);
				console.log(jsonData);
				if (jsonData.status == 200) {
					$.messager.alert("提示", jsonData.msg, 'info', function() {
						// 刷新表格数据
						$('#dg').datagrid('reload');
						$('#addCompanyWin').window('close');
					});
				}
				if (jsonData.status == 400) {
					$.messager.alert("提示", jsonData.msg, 'info');
				}
			}
		});
	});
});