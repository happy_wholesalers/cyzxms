$(function(){
	$('input').validatebox({    
	    required: true,      
	}); 
	//院系信息
	$("#e_fid").combobox({
		url : '/college/get.action',
		valueField : 'college_id',
		textField : 'college_name',
		panelHeight:'150',
	});
	// 设置弹出窗口的属性
	$('#add').window({
		title : '添加主要负责成员',
		closed : false,
		cache : false,
		collapsible : true,
		maximizable : false, // 不可最大化
		resizable : false,
		shadow : true,
		left : 500,
		top : 80,
		modal : true, // 模态窗
	});
	// 设置弹出窗口的属性
	$('#update').window({
		title : '修改主要负责成员',
		closed : false,
		cache : false,
		collapsible : true,
		maximizable : false, // 不可最大化
		resizable : false,
		shadow : true,
		left : 500,
		top : 80,
		modal : true, // 模态窗
	});
	$('#add').window('close');
	$('#update').window('close');
	$('#apply2').datagrid({
	title:'入驻企业团队概况（第二步）',
	iconCls : 'icon-tip',
	nowrap : false,
	striped : true,
	collapsible : true,
	sortName : 'e_id',
	sortOrder : 'desc',
	remoteSort : false,
	url:'/company/employee/get.action',
	remoteSort: false,
	idField:'e_id',
	frozenColumns:[[
	       {field:'ck',checkbox:true},
	        {
				title : '编号',
				field : 'e_id',
				width : 80,
				sortable : true
			} 
		]],
		columns:[[
			{field:'e_name',title:'姓名',width:100},
			{field:'e_sex',title:'性别',width:100,},
			{field:'e_fid',title:'院系',width:100,
/*				formatter : function(value, row, index) {// value：当前列对应字段值。row：当前的行记录数据。index：当前的行下标。
					if (row.e_fid) {
						return row.e_fid.college_name;
					} else {
						return value;
					}
				}*/
			},
			{field:'e_major',title:'专业',width:100},
			{field:'e_tel',title:'联系电话',width:100}
		]],
		onDblClickRow : function(selectRow) {
			edit(selectRow);
		},
		pagination:true,
		rownumbers:true,
		toolbar:[{
			id:'btnadd',
			text:'添加主要成员',
			iconCls:'icon-add',
			handler:function(){
				// 打开前清空表单
				$('#addForm').form('clear');	
				// 打开表单窗口
				$('#add').window('open');	
				addEmployee();
			}
			},'-',{
				id:'btncut',
				text:'删除主要成员',
				iconCls:'icon-cut',
				handler:function(){
					del();
			}
			},'-',{
				id:'btnsave',
				text:'修改主要成员信息',
				disabled:false,
				iconCls:'icon-save',
				handler : function(selectRow) {
					btnEdit(selectRow);
				}
				}]
			});
	// 添加用户
	/*$("#addbut").click(function() {
		$('#addForm').form('submit', {
			url : '/company/employee/add.action',
			success : function(data) {
				var jsonData = JSON.parse(data);
				if (jsonData.status == 200) {
					$.messager.alert("提示", jsonData.msg, 'info', function() {
						$('#apply2').datagrid('reload');
						$('#add').window('close');
					});

				} else if (jsonData.status == 400) {
					$.messager.alert("提示", jsonData.msg, 'info', function() {
					});
				}
			}
		});
	});*/
	//添加雇员
	function addEmployee(){
		$('#addbut').click(function(){
			$('#addForm').form('submit',{
				url : '/company/employee/add.action',
				success : function(data) {
					var jsonData = JSON.parse(data);
					if (jsonData.status == 200) {
						$.messager.alert("提示",jsonData.msg,'info',function(){
							//刷新表格数据
							$('#add').window('close');
							$('#apply2').datagrid('reload');
						});
					}
					if (jsonData.status == 400) {
						$.messager.alert("提示",jsonData.msg,'info',function(){
						});
					}
					$("#addbut").prop("onclick",null).off("click");
				}
				
			});
		});
	}
	// 修改用户
	$("#updatebut").click(function() {
		$('#updateForm').form('submit', {
			url : '/company/employee/update.action',
			success : function(data) {// {"status":200,"msg":"用户信息更新成功","data":null}
				var jsonData = JSON.parse(data);
				console.log(jsonData);
				if (jsonData.status == 200) {
					$.messager.alert("提示", jsonData.msg, 'info', function() {
						$('#apply2').datagrid('reload');
						$('#update').window('close');
					});
				} else if (jsonData.status == 400) {
					$.messager.alert("提示", jsonData.msg, 'info', function() {
					});
				}
			}
		});
	})
});		
function btnEdit(selectRow) {
	let rows = $("#apply2").datagrid('getSelections');
	let row = $("#apply2").datagrid('getSelected');
	if (rows.length <= 0) {
		$.messager.alert("提示", "请先选中要修改的行!");
		return false;
	}
	if (rows.length > 1) {
		$.messager.alert("提示", "不能同时修改多行!!");
		return false;
	}
	$('#updateForm').form('load', row);
	$('#update').dialog('open');
}
//双击修改用户
function edit(selectRow) {
	var r = $("#apply2").datagrid("getRows");// 获取本页有多少行
	row = r[selectRow];// 获得当前行
	$('#updateForm').form('load', row);
	$('#update').dialog('open');
}

//删除客户

function del() {
	var rows = getSelectionsIds();
	if (rows <= 0) {
		$.messager.alert('提示', '请先选中要删除的行!', 'info');
		return;
	}
	// 向后台返回user_id字符串，需要进行字符串拆分，返回sucess表示删除完成，返回error表示删除失败
	$.messager.confirm("提示", "确定要删除吗？", function(value) {
		if (value) {
			$.post('/company/employee/delete/'+rows+'.action', {
				"e_id" : rows
			}, function(data) {// {status: 200, msg: "用户删除成功", data: null}
				if (data.status == 200) {
					$.messager.alert("提示", data.msg, 'info', function() {
						$('#apply2').datagrid('reload');
						$("#apply2").datagrid('clearSelections');

					});
				} else if (data.status == 400) {
					$.messager.alert("提示", data.msg, 'info', function() {
						$('#apply2').datagrid('reload');
					});
				}
			}, "json");
		}
	});
}
function getSelectionsIds() {
	var itemList = $("#apply2");
	var sels = itemList.datagrid('getSelections');
	var user_ids = [];
	for ( var i in sels) {
		user_ids.push(sels[i].e_id);
	}
	user_ids = user_ids.join(",");
	return user_ids;
}








