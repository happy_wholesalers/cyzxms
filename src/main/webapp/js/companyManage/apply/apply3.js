$(function(){
	//加载表格数据
	$('#grid').datagrid({
		iconCls : 'icon-tip',
		nowrap : false,
		striped : true,
		collapsible : true,
		title : '导师信息（第三步）',
		url:'/tutor/findbyid.action',
		frozenColumns : [ [ {
			field : 'ck',
			checkbox : true
		}, {
			title : '编号',
			field : 't_id',
			sortable : true
		} ] ],
		columns : [ [ {
			field : 't_name',
			title : '姓名',
		}, {
			field : 't_tel',
			title : '联系电话',
		}, {
			field : 't_department',
			title : '导师所属部门',
		},{
			field : 't_professional',
			title : '导师职称',
		}, {
			field : 't_sex',
			title : '性别',
		}, {
			field : 't_num',
			title : '目前办公人数',
		},{
			field : 't_require',
			title : '办公场地需求',
		},{
			field : 't_worknum',
			title : '工位数量',
		}
			] ],
			onDblClickRow : function(selectRow) {
				edit(selectRow);
			},
			toolbar : [ {
				id : 'btnEdit',
				text : '修改',
				iconCls : 'icon-edit',
				handler : function(selectRow) {
					btnEdit(selectRow);
				}
			}]
		});
	// 设置弹出窗口的属性
	$('#editDlg').dialog({
		width:580,
		title : '编辑',
		closed : true,
		cache : false,
		collapsible : true,
		maximizable : true, // 不可最大化
		resizable : true,
		shadow : true,
		left : 500,
		top : 80,
		modal : true, // 模态窗
	});
	
	function btnEdit(selectRow) {
		let rows = $("#grid").datagrid('getSelections');
		let row = $("#grid").datagrid('getSelected');
		if (rows.length <= 0) {
			$.messager.alert("提示", "请先选中要修改的行!");
			return false;
		}
		if (rows.length > 1) {
			$.messager.alert("提示", "不能同时修改多行!!");
			return false;
		}
		$('#editForm').form('load', row);
		$('#editDlg').dialog('open');
	}
	// 双击修改用户
	function edit(selectRow) {
		var r = $("#grid").datagrid("getRows");// 获取本页有多少行
		row = r[selectRow];// 获得当前行
		$('#editForm').form('load', row);
		$('#editDlg').dialog('open');
	}
	// 提交修改
	$("#btnSave").click(function() {
		$('#editForm').form('submit', {
			url : '/tutor/update.action',
			onSubmit : function() {
			},
			success : function(data) {// {"status":200,"msg":"用户信息更新成功","data":null}
				var jsonData = JSON.parse(data);
				
				if (jsonData.status == 200) {
					$.messager.alert("提示", jsonData.msg, 'info', function() {
						// 刷新表格数据
						$('#grid').datagrid('reload');
						$('#editDlg').dialog('close');
						
					});
				} else if (jsonData.status == 400) {
					$.messager.alert("提示", jsonData.msg, 'info', function() {
					});
				}
			}
		});
	})
		
})
