$(function() {
	$("#updateCompany").combobox({
		url : '/company/listCname.action',
		valueField : 'company_id',
		textField : 'company_name',
		panelHeight : '150',
	});
	$("#updateCollege").combobox({
		url : '/college/get.action',
		valueField : 'college_id',
		textField : 'college_name',
		panelHeight : '150',
	});
	$('.needValidate').validatebox({    
	    required: true,      
	});  
/*	$('textarea').validatebox({    
	    required: true,      
	}); */


	$('#grid').datagrid({
		novalidate:true,
		title : '团队负责人（第一步）',
		iconCls : 'icon-tip',
		loadMsg : '数据加载中...',
		url : '/company/charger/get.action',
		sortName : 'user_id',
		sortOrder : 'desc',
		remoteSort : false,
		idField : 'user_id',
		frozenColumns : [[{
			field : 'ck',
			checkbox : true
		},{
			title : '编号',
			field : 'user_id',
		} ] ],
		columns : [ [ {
			field : 'user_name',
			title : '姓名',
		},{
			field : 'user_sex',
			title : '性别',
		},{
			field : 'user_nation',
			title : '民族',
		},{
			field : 'user_age',
			title : '年龄',
		},{
			field : 'user_tel',
			title : '联系电话',
		},{
			field : 'user_wx',
			title : '微信或QQ',
		},{
			field : 'user_email',
			title : '邮箱',
		},{
			field : 'user_card',
			title : '身份证',
		},{
			field : 'user_politics',
			title : '政治面貌',
		},{
			field : 'user_major',
			title : '学历',
		},{
			field : 'user_graduation',
			title : '毕业时间',
		},{
			field : 'user_major',
			title : '专业',
		},{
			field : 'user_homeaddr',
			title : '家庭住址',
		}] ],
		onDblClickRow : function(selectRow) {
			edit(selectRow);
		},
		toolbar : [ {
			id : 'btnEdit',
			text : '修改',
			iconCls : 'icon-edit',
			handler : function(selectRow) {
				btnEdit(selectRow);
			}
		}]
	});
	
	
	// 设置弹出窗口的属性
	$('#editDlg').dialog({
		width:580,
		title : '编辑',
		closed : true,
		cache : false,
		collapsible : true,
		maximizable : true, // 不可最大化
		resizable : true,
		shadow : true,
		left : 500,
		top : 80,
		modal : true, // 模态窗
	});
	
	
	function btnEdit(selectRow) {
		let rows = $("#grid").datagrid('getSelections');
		let row = $("#grid").datagrid('getSelected');
		if (rows.length <= 0) {
			$.messager.alert("提示", "请先选中要修改的行!");
			return false;
		}
		if (rows.length > 1) {
			$.messager.alert("提示", "不能同时修改多行!!");
			return false;
		}
		img("imgHeadPhoto1", row.user_image);
		$('#editForm').form('load', row);
		$('#editDlg').dialog('open');
	}
	// 双击修改用户
	function edit(selectRow) {
		var r = $("#grid").datagrid("getRows");// 获取本页有多少行
		row = r[selectRow];// 获得当前行
		img("imgHeadPhoto1", row.user_image);
		$('#editForm').form('load', row);
		$('#editDlg').dialog('open');
	}
	
	// 提交修改
	$("#btnSave").click(function() {
		//验证
		var isValid = $('#editForm').form('validate');
		if(isValid == false){
			return;
		}
		$('#editForm').form('submit', {
			url : '/company/charger/update.action',
			onSubmit : function() {
				
			},
			success : function(data) {// {"status":200,"msg":"用户信息更新成功","data":null}
				var jsonData = JSON.parse(data);
				
				if (jsonData.status == 200) {
					$.messager.alert("提示", jsonData.msg, 'info', function() {
						// 刷新表格数据
						$('#grid').datagrid('reload');
						$('#editDlg').dialog('close');
						cleanImg("imgHeadPhoto1");
					});
				} else if (jsonData.status == 400) {
					$.messager.alert("提示", jsonData.msg, 'info', function() {
					});
				}
			}
		});
	})
});
	
function img(id, image) {
	if (image == "" || image == null) {
		image = "/images/default.jpg";
	}
	var fId = "#" + id;
	$(fId).attr("src", image);
}


/*
	// 修改
	$("#updateCharge").click(function() {
		$('#headForm').form('submit', {
			url : '/company/charger/update.action',
			success : function(data) {
				var jsonData = JSON.parse(data);
				if (jsonData.status == 200) {
					window.setTimeout("window.location='apply2.html'",200); 
				} else if (jsonData.status == 400) {
					$.messager.alert("提示", jsonData.data,'info');
				}
			}
		});
	});
*/