$(function() {
	// 查询
	$.ajax({
		url : '/protocol/select.action',
		dataType : 'json',
		contentType : 'utf-8',
		success : function(value) {
			$('#protocolForm').form('load', value);
		}
	});
	// 通过保存按钮，修改数据
	$('#protocolSave').click(function() {
		$('#protocolForm').form('submit', {
			url : '/protocol/update.action',
			onSubmit : function() {
			},
			success : function(data) { // {"status":200,"msg":"用户信息更新成功","data":null}
				var jsonData = JSON.parse(data);
				console.log(jsonData);
				if (jsonData.status == 200) {
					$.messager.alert("提示", jsonData.msg, 'info', function() {
						// 刷新表格数据
						$('#protocolForm').datagrid('reload', data[0]);
						// $('#updateCompanyWin').window('close');
					});
				}
				if (jsonData.status == 400) {
					$.messager.alert("提示", jsonData.msg, 'info');
				}
			}
		});
	});
});