$(function() {
		$('#assetsLendNavigation').datagrid({
			url:'/public/assetlend/get.action',//异步请求数据的url
			loadMsg:'数据加载中...',
			nowrap : false,
			striped : true,
			collapsible : true,
			frozenColumns : [ [ {
				field : 'ck',
				checkbox : true
			} ] ],
			fitColumns:true,//自动适应列大小
			autoRowHeight:true,//自动调整行的高度
			pagination:true,//设置分页
			pageSize:10,//设置显示页面数据行数
			pageList:[10,50,100],//设置显示页面的行数的选择
			rownumbers:true,//是否在行前面添加序号 */
			columns:[[ 
			   {'field':'itemName','title':'物品名称'},
			   {'field':'itemNumber','title':'总数量'},
			   {'field':'itemMeasurement','title':'计量单位'},
			   {'field':'itemObjectName','title':'仪器名称'},
			   ]],
			fitColumns : true,
			striped : true,
			pagination : true,
			singleSelect : true,
			rownumbers : true,
	});
});
	
	