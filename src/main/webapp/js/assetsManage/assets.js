$(function() {
		//公司名字
		$("#companyId").combobox({
			url : '/company/listCname.action',
			valueField : 'company_id',
			textField : 'company_name',
			panelHeight:'150',
		});
		$('#assetsTable').window('close');
		var lastIndex;
		$('#assetsNavigation').datagrid({
			url:'/public/asset/get.action',//异步请求数据的url
			loadMsg:'数据加载中...',
			nowrap : false,
			striped : true,
			collapsible : true,
			frozenColumns : [ [ {
				field : 'ck',
				checkbox : true
			} ] ],
			fitColumns:true,//自动适应列大小
			autoRowHeight:true,//自动调整行的高度
			pagination:true,//设置分页
			pageSize:10,//设置显示页面数据行数
			pageList:[10,50,100],//设置显示页面的行数的选择
			rownumbers:true,//是否在行前面添加序号 */
			columns:[[ 
			   {'field':'assetsName','title':'物品名称'},
			   {'field':'assetsMeasurement','title':'计量单位'},
			   {'field':'assetsTotalNumber','title':'总数量'},
			   {'field':'assetsRemainingNumber','title':'剩余数量'},
			   {'field':'assetsPrice','title':'单价'},
			   {'field':'assetsObjectName','title':'仪器名称'},
			   {'field':'assetsBuyDate','title':'购买时间'},
			   {'field':'assetsSequence','title':'编号'}
			   ]],	
			toolbar : [ {
				text : '添加',
				iconCls : 'icon-add',
				handler : function() {
					// 打开前清空表单
					$('#assetsForm').form('clear');
					$('#assetsTable').window('open');
					addAssets();
				}
			}, '-', {
				id : 'btnsave',
				text : '修改',
				iconCls : 'icon-save',
				handler : function(selectRow) {
					btnEdit(selectRow);
				}
			}, '-',
			{
				text : '删除',
				iconCls : 'icon-remove',
				handler : function() {
					//调用函数删除公司资产
					delAssets();
				}
			}],
			fitColumns : true,
			striped : true,
			pagination : true,
			singleSelect : true,
			rownumbers : true,
			onDblClickRow : function(selectRow) {
				doubleClickUpdate(selectRow);
			},
			onBeforeLoad : function() {
				$(this).datagrid('rejectChanges');
			}
		});
		//点击导航栏上的修改按钮
		 function btnEdit(selectRow) {
		        let rows = $("#assetsNavigation").datagrid('getSelections');
		        let row = $("#assetsNavigation").datagrid('getSelected');
		        if (rows.length <= 0) {
		            $.messager.alert("提示", "请先选中要修改的行!");
		            return false;
		        }
		        if (rows.length > 1) {
		            $.messager.alert("提示", "不能同时修改多行!!");
		            return false;
		        }
				$('#assetsForm').form('load', row);
				$('#assetsTable').dialog('open');
				updateAssets();
		    }
		//添加公司资产
		function addAssets(){
			$('#save').click(function(){
				$('#assetsForm').form('submit',{
					url : '/public/asset/add.action',
					success : function(data) {
						var jsonData = JSON.parse(data);
						if (jsonData.status == 200) {
							$.messager.alert("提示",jsonData.msg,'info',function(){
								//刷新表格数据
								$('#assetsTable').window('close');
								$('#assetsNavigation').datagrid('reload');
							});
						}
						if (jsonData.status == 400) {
							$.messager.alert("提示",jsonData.msg,'info',function(){
							});
						}
						$("#save").prop("onclick",null).off("click");
					}
					
				});
			});
		}
		
		// 双击修改用户
		function doubleClickUpdate(selectRow) {
			var r = $("#assetsNavigation").datagrid("getRows"); // 获取本页有多少行
			var row = r[selectRow]; // 获得当前行
			$('#assetsForm').form('load', row);
			$('#assetsTable').dialog('open');
			updateAssets();
		}
	});

	//修改公司资产
	function updateAssets(){
			$('#save').click(function(){
				$('#assetsForm').form('submit',{
					url : '/public/asset/update.action',
					success : function(data) {
						var jsonData = JSON.parse(data);
						if (jsonData.status == 200) {
							$.messager.alert("提示",jsonData.msg,'info',function(){
								//刷新表格数据
								$('#assetsTable').window('close');
								$('#assetsNavigation').datagrid('reload');
							});
						}
						if (jsonData.status == 400) {
							$.messager.alert("提示",jsonData.msg,'info',function(){
							});
						}
						$("#save").prop("onclick",null).off("click");
					}
					
				});
			});
		}
	//删除公司资产
	function delAssets() {
		var row = $('#assetsNavigation').datagrid('getSelections');
		if (row.length <= 0) {
			$.messager.alert('提示', '请先选中要删除的行!','info');
			return;
		}
		var id=row[0].assetsId;
		// 向后台返回user_id字符串，需要进行字符串拆分，返回sucess表示删除完成，返回error表示删除失败
		$.messager.confirm("提示", "确定要删除吗？", function(value) {
			if (value) {
				$.get('/public/asset/delete/'+id+'.action',
					function(data) {//{status: 200, msg: "用户删除成功", data: null}
						if (data.status == 200) {
							$.messager.alert("提示",data.msg,'info',function(){
								//成功的话，我们要关闭窗口
								$('#editDlg').dialog('close');
								//刷新表格数据
								$('#assetsNavigation').datagrid('reload');
								$("#assetsNavigation").datagrid('clearSelections');
							});
						}
						if (data.status == 400) {
							$.messager.alert("提示",data.msg,'info',function(){
								//成功的话，我们要关闭窗口
								$('#editDlg').dialog('close');
								//刷新表格数据
								$('#assetsNavigation').datagrid('reload');
							});
						}
				}, "json");
			}
		});
	}
	
	