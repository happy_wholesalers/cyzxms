/**
 * 公司入驻申请统计

 * 
 * @returns
 */
//应用于autoComplete.js自动补全
var _url='/dep/team/findLeaderName.action';
var _value='t_leader';
$(function() {
	$('#t_name').combobox({
		url : '/dep/team/findTeamName.action',
		valueField : 't_name',
		textField : 't_name',
		panelHeight : '150',
	});
	//院系下拉框加载
	//更新
	$("#t_dep_up").combobox({
		url : '/college/get.action',
		valueField : 'college_id',
		textField : 'college_name',
		panelHeight : '150',
	});
	//增加
	$("#t_dep_add").combobox({
		url : '/college/get.action',
		valueField : 'college_id',
		textField : 'college_name',
		panelHeight : '150',
	});
	
	
	$('#grid').datagrid({
		title : '院系团队管理',
		url : '/dep/team/get.action',
		frozenColumns : [ [ {
			field : 'ck',
			checkbox : true
		}, {
			title : '编号',
			field : 't_id',
			width : 80,
			sortable : true
		} ] ],
		columns : [ [ {
			field : 't_name',
			title : '团队名称'
		},{
			field : 't_leader',
			title : '负责人'
		}, {
			field : 't_number',
			title : '人数'
		}, 
		{
			field : 't_enterdate',
			title : '入孵时间'
		},  {
			field : 't_field',
			title : '技术领域'
		}, {
			field : 't_knowledge',
			title : '知识产权情况'
		},  {
			field : 't_collectionPerson',
			title : '联系人'
		}, {
			field : 't_tel',
			title : '联系电话'
		}, {
			field : 't_dep',
			title : '院系',
			formatter : function(value, row, index) {
				if (row.t_dep == 1) {
					return '文学院';
				} else if (row.t_dep == 2){
					return '新闻与传播学院';
				}else if (row.t_dep == 3){
					return '外国语学院';
				}else if (row.t_dep == 4){
					return '电气与机械工程学院';
				}else if (row.t_dep == 5){
					return '化学与环境工程学院';
				}else if (row.t_dep == 6){
					return '计算机学院(软件学院)';
				}else if (row.t_dep == 7){
					return '信息工程学院';
				}else if (row.t_dep == 8){
					return '数学与统计学院';
				}else if (row.t_dep == 9){
					return '经济管理学院';
				}else if (row.t_dep == 10){
					return '旅游与规划学院';
				}else if (row.t_dep == 11){
					return '政法学院';
				}else if (row.t_dep == 12){
					return '医学院';
				}else if (row.t_dep == 13){
					return '教师教育学院';
				}else if (row.t_dep == 14){
					return '艺术设计学院';
				}else if (row.t_dep == 15){
					return '体育学院';
				}else if (row.t_dep == 16){
					return '音乐学院';
				}else if (row.t_dep == 17){
					return '陶瓷学院';
				}else{
					return '无';
				}
			}
				
				
		}
		 ] ],
		toolbar : [ {
			id : 'btnAdd',
			text : '添加',
			iconCls : 'icon-add',
			handler : function() {
				$('#addEnterApplyForm').form('clear');
				$('#addTeam').window('open');
			}
		}, {
			id : 'btnUpdate',
			text : '修改',
			iconCls : 'icon-edit',
			handler : function(selectRow) {
				btnShowEditWindow(selectRow);
			}
		}, {
			id : 'btnCut',
			text : '删除',
			iconCls : 'icon-cut',
			handler : function() {
				deleteTeam();
			}
		} ],
		pagination : true,
		rownumbers : true,
		striped : true,
		loadMsg : '数据加载中...',
		collapsible : true,
		onDblClickRow : function(selectRow) {
			dblShowEditWindow(selectRow);
		}
	});

	// 入驻信息修改窗口
	$('#updateTeam').window({
		title : '修改入驻信息',
		closed : false,
		cache : false,
		collapsible : true,
		maximizable : false, // 不可最大化
		resizable : false,
		shadow : true,
		modal : true, // 模态窗
	});
	// 默认关闭修改入驻信息窗口
	$('#updateTeam').window('close');
	// 通过双击当前行，弹出当前行入驻信息修改窗口
	function dblShowEditWindow(selectRow) {
		var r = $("#grid").datagrid("getRows"); // 获取本页有多少行
		var row = r[selectRow]; // 获得当前行
		$('#updateTeamForm').form('load', row);
		$('#updateTeam').window('open');
	}

	// 通过修改按钮，弹出当前行入住信息修改窗口
	function btnShowEditWindow(selectRow) {
		let rows = $("#grid").datagrid('getSelections');
		let row = $("#grid").datagrid('getSelected');
		if (rows.length <= 0) {
			$.messager.alert("提示", "请先选中要修改的行!");
			return false;
		}
		if (rows.length > 1) {
			$.messager.alert("提示", "不能同时修改多行!!");
			return false;
		}
		$('#updateTeamForm').form('load', row);
		$('#updateTeam').window('open');
	}
	// 通过修改按钮，修改选中行中的数据
	$('#updateEnterApply').click(function() {
		$('#updateTeamForm').form('submit', {
			url : '/dep/team/update.action',
			onSubmit : function() {
			},
			success : function(data) { // {"status":200,"msg":"用户信息更新成功","data":null}
				var jsonData = JSON.parse(data);
				console.log(jsonData);
				if (jsonData.status == 200) {
					$.messager.alert("提示", jsonData.msg, 'info', function() {
						// 刷新表格数据
						$('#grid').datagrid('reload');
						$('#updateTeam').window('close');
					});
				}
				if (jsonData.status == 400) {
					$.messager.alert("提示", jsonData.msg, 'info');
				}
			}
		});
	});

	// 获取公司id
	function getSelectionsIds() {
		var itemList = $("#grid");
		var sels = itemList.datagrid('getSelections');
		var t_ids = [];
		for ( var i in sels) {
			t_ids.push(sels[i].t_id);
		}
		t_ids = t_ids.join(",");
		return t_ids;
	}
	// 根据公司id删除团队信息
	function deleteTeam() {
		var rows = getSelectionsIds();
		if (rows <= 0) {
			$.messager.alert('提示', '请先选中要删除的行!', 'info');
			return;
		}
		// 向后台返回t_id字符串，需要进行字符串拆分，返回sucess表示删除完成，返回error表示删除失败
		$.messager.confirm("提示", "确定要删除吗？", function(value) {
			if (value) {
				$.post('/dep/team/delete/' + rows + '.action', function(data) { // {status:
					// 200,
					// msg:
					// "用户删除成功",
					// data:
					// null}
					if (data.status == 200) {
						$.messager.alert("提示", data.msg, 'info', function() {
							// 刷新表格数据
							$('#grid').datagrid('reload');
							$("#grid").datagrid('clearSelections');
						});
					}
					if (data.status == 400) {
						$.messager.alert("提示", data.msg, 'info', function() {
							// 刷新表格数据
							$('#grid').datagrid('reload');
						});
					}
				}, "json");
			}
		});
	}
	// 入驻信息添加窗口
	$('#addCompanyWin').window({
		title : '添加入驻信息',
		closed : false,
		cache : false,
		collapsible : true,
		maximizable : false, // 不可最大化
		resizable : false,
		shadow : true,
		modal : true, // 模态窗
	});
	// 默认关闭入驻信息添加窗口
	$('#addCompanyWin').window('close');
	// 通过修改按钮，修改选中行中的数据
	$('#addEnterApply').click(function() {
		$('#addEnterApplyForm').form('submit', {
			url : '/dep/team/add.action',
			onSubmit : function() {
			},
			success : function(data) { // {"status":200,"msg":"用户信息更新成功","data":null}
				var jsonData = JSON.parse(data);
				console.log(jsonData);
				if (jsonData.status == 200) {
					$.messager.alert("提示", jsonData.msg, 'info', function() {
						// 刷新表格数据
						$('#grid').datagrid('reload');
						$('#addTeam').window('close');
					});
				}
				if (jsonData.status == 400) {
					$.messager.alert("提示", jsonData.msg, 'info');
				}
			}
		});
	});
});