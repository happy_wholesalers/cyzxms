$(function() {
	if (PDFObject.supportsPDFs) {
		// PDF嵌入到网页
		PDFObject.embed("", "#pdf_viewer");//返回保存路径
	} else {
		location.href = "/canvas";
	}
	
	$('submitPdf').click(function() {
		$.ajax({
	        url: '',
	             type: 'POST',
	             cache: false,
	             data:new FormData($('#certificateForm')[0]),//h5的DataForm对象
	             dataType:"json",
	             processData: false,
	             contentType: false,
	             success:function(data){
	            	 if (data.status == 200) {
	            		 $('#pdf_viewer').css('display','');
	            		 $('#pre').css('display','none');
	            		 $.messager.alert('提示', '上传成功，若继续上传请继续选择文件', 'info');
	            			if (PDFObject.supportsPDFs) {
	            				// PDF嵌入到网页
	            				PDFObject.embed("/upload/pdf/demo.pdf", "#pdf_viewer");//返回保存路径
	            			} else {
	            				location.href = "/canvas";
	            			}
	 				} else if (data.status == 400) {
	 					 $.messager.alert('提示', '上传失败，请联系管理员检查', 'info');
	 				}
	             }
	        })  
	})
})
