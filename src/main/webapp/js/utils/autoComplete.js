//自动补全
$(function() {
	//自动补全
	$('#inputtable').combobox(
			{
				prompt : '输入关键字后自动搜索',
				mode : 'remote',
				url : _url,//_url,_value已经在各自的js文件中定义
				valueField: _value,
	            textField: _value,
	            panelHeight: 'auto',
	            panelMaxHeight: 150,
				editable : true,
				hasDownArrow : false,
				onBeforeLoad : function(param) {
					if (param == null || param.q == null
							|| param.q.replace(/ /g, '') == '') {
						var value = $(this).combobox('getValue');
						if (value) {// 修改的时候才会出现q为空而value不为空
							param.id = value;
							return true;
						}
						return false;
					}
				}
			});
	
})
