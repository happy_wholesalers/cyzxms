/**
 * 公用的查询和重置按钮
 * */

$(function(){

	//点击查询按钮
	$('#btnSearch').bind('click',function(){
		//把表单数据转换成json对象
		var formData = $('#searchForm').serializeJSON();
		if(formData.endDate ==''){
			formData.endDate ='2099-12-31';
		}
		if(formData.beginDate ==''){
			formData.beginDate ='2000-12-31';
		}
		$('#grid').datagrid('load',formData);
	});
	//点击重置按钮
	$('#btnReset').bind('click',function(){
		
		$('#searchForm').form('clear');
	});
})