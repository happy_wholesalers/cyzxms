/**
 * @author 徐培珊
 * @date 2018年12月5日下午7:45:30
 * @version 1.0
 * @company 河南喜乐融商贸有限公司
 * @Copyright: 2018 www.xlr.com Inc. All rights reserved. 
 */
package com.xlr.exception;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import com.xlr.utils.MailUtils;
import com.xlr.utils.StackTrace;

/**
 * 全局异常处理类
 * 
 * @author 徐培珊
 * @date 2018年12月5日下午7:45:30
 */
public class GlobalExceptionResolver implements HandlerExceptionResolver {

	/**
	 * @author 徐培珊
	 * @date 2018年12月5日下午7:45:30
	 * @param request
	 * @param response
	 * @param handler
	 * @param ex
	 * @return
	 */
	@Override
	public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler,
			Exception ex) {
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				try {
					MailUtils.sendEmail(null, "页面异常", StackTrace.getStackTrace(ex));
				} catch (MessagingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		};
		Thread thread = new Thread(runnable);
		thread.start();
		// 展示错误页面
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("message", "系统发生异常，请稍后重试");
//		modelAndView.setViewName("views/error/exception");
		return modelAndView;
	}

}
