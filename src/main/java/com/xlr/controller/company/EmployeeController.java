package com.xlr.controller.company;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.xlr.pojo.Employee;
import com.xlr.pojo.Userinfo;
import com.xlr.service.company.EmployeeService;
import com.xlr.utils.EasyUIDataGridResult;
import com.xlr.utils.ManagerResult;
import com.xlr.utils.UserinfoUtils;

@Controller
@RequestMapping("/company/employee")
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;

	/**
	 * @author 王宏志 获取职员全部的信息
	 * @date 2018年12月4日下午6:25:01
	 * @param page
	 * @param rows
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/get")
	public EasyUIDataGridResult assetList(@RequestParam(value = "page", defaultValue = "1") Integer page,
			@RequestParam(value = "rows", defaultValue = "10") Integer rows) {
		Userinfo userinfo = UserinfoUtils.getUserinfo();
		Integer cid = userinfo.getUser_companyid();
		return employeeService.get(cid, page, rows);
	}

	/**
	 * @author 王宏志 添加职员信息
	 * @date 2018年12月4日下午6:25:17
	 * @param employee
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/add")
	public ManagerResult assetAdd( Employee employee) {
		Userinfo userinfo = UserinfoUtils.getUserinfo();
		Integer companyid = userinfo.getUser_companyid();
		Integer id = userinfo.getUser_id();
		employee.setE_cid(companyid);
		employee.setE_uid(id);
		return employeeService.add(employee);
	}

	/**
	 * @author 王宏志 删除职员信息
	 * @date 2018年12月4日下午6:25:33
	 * @param cid
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/delete/{eid}")
	public ManagerResult assetDelete(@PathVariable("eid") Integer eid) {
		return employeeService.delete(eid);
	}

	/**
	 * @author 王宏志 更新职员信息
	 * @date 2018年12月4日下午6:26:09
	 * @param employee
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/update")
	public ManagerResult assetUpdate(Employee employee) {
		return employeeService.update(employee);
	}

}
