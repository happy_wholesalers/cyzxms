/**
 * @author 高俊
 * @date 2018年11月8日下午8:50:04
 * @version 1.0
 * @company 河南喜乐融商贸有限公司
 * @Copyright: 2018 www.xlr.com Inc. All rights reserved. 
 */
package com.xlr.controller.company;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.xlr.pojo.Company;
import com.xlr.pojo.Overview;
import com.xlr.pojo.Userinfo;
import com.xlr.service.company.CompanyService;
import com.xlr.utils.EasyUIDataGridResult;
import com.xlr.utils.ManagerResult;
import com.xlr.utils.UserinfoUtils;

/**
 * @author 高俊
 * @date 2018年11月8日下午8:50:04
 */
/**
 * @author 李贺鹏
 * @date 2019年2月21日下午3:14:35
 */
/**
 * @author 李贺鹏
 * @date 2019年2月21日下午3:14:41
 */
@Controller
public class CompanyController {

	@Autowired
	private CompanyService companyService;

	/**
	 * 查询公司所有信息_分页
	 * 
	 * @author 徐培珊
	 * @date 2018年11月28日下午3:23:09
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping(value = "/company/listCname")
	@ResponseBody
	public List<Company> listCompany() {
		return companyService.findAllCompany();
	}

	@RequestMapping(value = "/company/listCnameByPage")
	@ResponseBody
	public EasyUIDataGridResult index(@RequestParam(value = "page", required = true, defaultValue = "1") Integer page,
			@RequestParam(value = "rows", required = true, defaultValue = "10") Integer rows) {
		EasyUIDataGridResult result = companyService.findAllCnameByPage(page, rows);
		return result;
	}

	/**
	 * 根据session中的公司id查询公司信息
	 * 
	 * @author 徐培珊
	 * @date 2018年12月5日下午8:41:34
	 * @param httpSession
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping(value = "/company/listCurrentCompanyByPage")
	@ResponseBody
	public EasyUIDataGridResult indexByCompanyId(
			@RequestParam(value = "page", required = true, defaultValue = "1") Integer page,
			@RequestParam(value = "rows", required = true, defaultValue = "10") Integer rows) {
		Userinfo userinfo = UserinfoUtils.getUserinfo();
		String user_code = userinfo.getUser_code();
		EasyUIDataGridResult result = companyService.findAllCurrentCompanyByPage(user_code, page, rows);
		return result;
	}

	@RequestMapping(value = "/company/businessLicence")
	@ResponseBody
	public ManagerResult businessLicence(MultipartFile image, HttpSession session) {
		if (!image.getOriginalFilename().equals("")) {
			Userinfo userinfo = UserinfoUtils.getUserinfo();
			Company company = new Company();
			company.setCompany_id(userinfo.getUser_companyid());
			// 图片新名字
			String newName = UUID.randomUUID().toString();
			// 图片原来的名字
			String oldName = image.getOriginalFilename();
			// 后缀
			String sux = oldName.substring(oldName.lastIndexOf("."));
			// 新建本地文件流
			String realPath = session.getServletContext().getRealPath("/");
			File file = new File(realPath + "/upload/businessLicense/" + newName + sux);
			// 写入本地磁盘
			try {
				image.transferTo(file);
			} catch (IllegalStateException e) {
				e.printStackTrace();
				return ManagerResult.build(400, "营业执照上传错误");
			} catch (IOException e) {
				e.printStackTrace();
			}
			company.setCompany_image("/upload/businessLicense/" + newName + sux);
			ManagerResult result = companyService.updateCompanyImage(company);
			return result;
		}
		return ManagerResult.build(400, "营业执照不存在");
	}

	@RequestMapping(value = "/company/getBusinessLicence")
	@ResponseBody
	public ManagerResult getBusinessLicence() {
		Userinfo userinfo = UserinfoUtils.getUserinfo();
		if (userinfo == null) {
			return ManagerResult.build(400, "用户不存在");
		}
		ManagerResult result = companyService.findCompanyImageId(userinfo.getUser_companyid());
		return result;
	}

	/**
	 * 入驻申请统计_更新公司信息
	 * 
	 * @author 徐培珊
	 * @date 2018年11月27日下午2:08:38
	 * @param company
	 * @return
	 */
	@RequestMapping("/company/updateCompanyInfo")
	@ResponseBody
	public ManagerResult updateCompanyInfo(Company company) {
		return companyService.updateCompanyInfo(company);
	}

	/**
	 * 入驻申请统计_根据公司id删除公司信息
	 * 
	 * @author 徐培珊
	 * @date 2018年11月27日下午4:32:17
	 * @param company_id
	 * @return
	 */
	@RequestMapping("/company/delete/{company_id}")
	@ResponseBody
	public ManagerResult deleteCompanyInfoById(@PathVariable("company_id") Integer company_id) {
		return companyService.deleteCompanyInfoById(company_id);
	}

	/**
	 * 入驻申请统计_添加公司信息
	 * 
	 * @author 徐培珊
	 * @date 2018年11月27日下午5:30:09
	 * @param company
	 * @return
	 */
	@RequestMapping("/company/addCompanyInfo")
	@ResponseBody
	public ManagerResult addCompanyInfo(Company company) {
		System.out.println(company);
		return companyService.addCompanyInfo(company);
	}

	/**
	 * 根据session中的用户名添加公司信息
	 * 
	 * @author 徐培珊
	 * @date 2018年12月5日下午9:04:52
	 * @param company
	 * @return
	 */
	@RequestMapping("/company/addCompanyInfoByUserCode")
	@ResponseBody
	public ManagerResult addCompanyInfoByUserCode(Company company) {
		Userinfo userinfo = UserinfoUtils.getUserinfo();
		company.setCompany_leader(userinfo.getUser_code());
		return companyService.addCompanyInfo(company);
	}

	/**
	 * 根据孵化类型查询公司
	 * 
	 * @author 徐培珊
	 * @date 2018年12月6日上午11:46:03
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping(value = "/company/listChatchByPage", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public EasyUIDataGridResult listChatchByPage(Company company,
			@RequestParam(value = "page", required = true, defaultValue = "1") Integer page,
			@RequestParam(value = "rows", required = true, defaultValue = "10") Integer rows) {
		// 1：表示孵化成功
		company.setCompany_hatchtype(1);
		EasyUIDataGridResult result = companyService.findAllCompanyByPage(company, page, rows);
		return result;
	}
	
	/**
	 * 根据当前登陆用户id获取其团队项目概况
	 * @author 李贺鹏
	 * @date 2019年2月21日下午3:14:44
	 * @param company
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping(value = "/company/overviewlistByPage.action", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public EasyUIDataGridResult listOverview(Overview overview ,
			@RequestParam(value = "page", required = true, defaultValue = "1") Integer page,
			@RequestParam(value = "rows", required = true, defaultValue = "10") Integer rows) {
		Userinfo userinfo = UserinfoUtils.getUserinfo();
		overview.setO_companyid(userinfo.getUser_companyid());
		return null;
	}
}