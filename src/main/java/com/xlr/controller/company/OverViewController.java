package com.xlr.controller.company;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xlr.pojo.OverViewAll;
import com.xlr.pojo.Overview;
import com.xlr.service.company.OverViewService;
import com.xlr.utils.EasyUIDataGridResult;
import com.xlr.utils.ManagerResult;
import com.xlr.utils.UserinfoUtils;

/**
 * 项目概况
 * @author Zoe
 * @date 2019年2月28日下午8:08:29
 */
@Controller
public class OverViewController {
	
	@Autowired
	private OverViewService overViewService;

	/**
	 * 查找显示所有表格数据
	 * @author Zoe
	 * @date 2019年2月28日下午8:10:31
	 * @param companyId 公司id
	 * @return
	 */
	@RequestMapping("/overview/findAllTable")
	@ResponseBody
	public OverViewAll findAllTable(Integer companyId) {
		if(null==companyId) {
			companyId=UserinfoUtils.getUserinfo().getUser_companyid();
		}
		return overViewService.findAllTable(companyId);
	}
	
	/**
	 * 添加项目概况
	 * @author Zoe
	 * @date 2019年3月1日上午10:37:32
	 * @param overview 项目概况对象
	 * @return
	 */
	@RequestMapping("/overview/add")
	@ResponseBody
	public ManagerResult insert(Overview overview, @RequestParam(value = "o_type[]", required = false) String[] o_type) {
		return overViewService.insert(overview, o_type);
	}
	
	/**
	 * 分页查找所有项目概况
	 * @author Zoe
	 * @date 2019年3月1日下午2:38:49
	 * @param page 页数
	 * @param rows 每页记录数
	 * @return
	 */
	@RequestMapping("/overview/listByPage")
	@ResponseBody
	public EasyUIDataGridResult listByPage(@RequestParam(value = "page", required = true, defaultValue = "1") Integer page,
			@RequestParam(value = "rows", required = true, defaultValue = "10") Integer rows) {
		return overViewService.listByPage(page, rows);
	}
	
	/**
	 * 更新项目概况
	 * @author Zoe
	 * @date 2019年3月1日下午2:49:05
	 * @param overview 项目概况对象
	 * @param o_type 项目概况所属类型
	 * @return
	 */
	@RequestMapping("/overview/update")
	@ResponseBody
	public ManagerResult update(Overview overview, @RequestParam(value = "o_type[]", required = false) String[] o_type) {
		return overViewService.update(overview, o_type);
	}
}
