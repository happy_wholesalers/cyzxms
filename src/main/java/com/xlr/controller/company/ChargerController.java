package com.xlr.controller.company;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.xlr.pojo.Charger;
import com.xlr.pojo.Userinfo;
import com.xlr.service.company.ChargerService;
import com.xlr.utils.EasyUIDataGridResult;
import com.xlr.utils.ManagerResult;
import com.xlr.utils.UserinfoUtils;

/**
 * @author 王宏志
 * 
 *         入驻项目负责人Controller层
 * @date 2018年12月2日上午11:04:22
 * @TOOD
 */
@Controller
@RequestMapping(value = "/company/charger")
public class ChargerController {

	@Autowired
	private ChargerService chargerService;

	/**
	 * @author 王宏志 展示入驻项目负责人的全部信息
	 * @date 2018年12月2日上午11:16:22
	 * @return
	 */
	@RequestMapping(value = "/get")
	@ResponseBody
	public EasyUIDataGridResult listChargerInfo() {
		Userinfo userinfo = UserinfoUtils.getUserinfo();
		return chargerService.showChargerByUserId(userinfo.getUser_id(),userinfo.getUser_code());
	}

	/**
	 * @author 王宏志 修改入驻项目负责人信息
	 * @date 2018年12月2日上午11:19:34
	 * @param charger
	 * @return
	 * @throws IOException
	 * @throws IllegalStateException
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	public ManagerResult updateChargerInfo(HttpSession session,
			Charger charger, MultipartFile image) throws IllegalStateException,
			IOException {
		Userinfo userinfo = UserinfoUtils.getUserinfo();
		System.out.println(userinfo);
		Integer id = userinfo.getUser_id();
		if (!image.getOriginalFilename().equals("")) {
			// 图片新名字
			String newName = UUID.randomUUID().toString();
			// 图片原来的名字
			String oldName = image.getOriginalFilename();
			// 后缀
			String sux = oldName.substring(oldName.lastIndexOf("."));
			// 新建本地文件流
			String realPath = session.getServletContext().getRealPath("/");
			File file = new File(realPath + "/upload/head/" + newName + sux);
			// 写入本地磁盘
			image.transferTo(file);
			charger.setUser_image("/upload/head/" + newName + sux);
		}
		charger.setUser_id(id);
		ManagerResult result = chargerService.updateCharger(charger);
		System.out.println(result.getStatus());
		return result;
	}
}
