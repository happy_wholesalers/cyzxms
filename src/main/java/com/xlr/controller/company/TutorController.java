package com.xlr.controller.company;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xlr.pojo.Tutor;
import com.xlr.pojo.Userinfo;
import com.xlr.service.company.TutorService;
import com.xlr.utils.EasyUIDataGridResult;
import com.xlr.utils.ManagerResult;
import com.xlr.utils.UserinfoUtils;

/**
 * @author 范英豪
 * @date 2019年2月18日下午15:59:04
 */
@Controller
public class TutorController {
	@Autowired
	private TutorService tutorService;

	/**
	 * 根据公司id查导师
	 * 
	 * @author 范英豪
	 * @date 2019年2月18日下午15:59:09
	 * @return
	 */
	@RequestMapping(value = "/tutor/findbyid")
	@ResponseBody
	public EasyUIDataGridResult findbyid() {
		Userinfo userinfo = UserinfoUtils.getUserinfo();
		return tutorService.findByID(userinfo.getUser_companyid());
	}

	/**
	 * 根据公司id修改导师
	 * 
	 * @author 范英豪
	 * @date 2019年2月18日下午15:59:09
	 * @return
	 */
	@RequestMapping(value = "/tutor/update")
	@ResponseBody
	public ManagerResult update(Tutor tutor) {
		Userinfo userinfo = UserinfoUtils.getUserinfo();
		tutor.setT_companyid(userinfo.getUser_companyid());
		return tutorService.update(tutor);
	}
}
