package com.xlr.controller.company;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xlr.service.company.CollegeService;
import com.xlr.pojo.College;

@Controller
public class CollegeController {

	@Autowired
	private CollegeService collegeService;

	@RequestMapping("/college/get")
	@ResponseBody
	public List<College> getAll() {
		return collegeService.get();
	}
}
