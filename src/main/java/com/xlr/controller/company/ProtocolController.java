/**
 * @author 徐培珊
 * @date 2018年12月1日下午8:49:42
 * @version 1.0
 * @company 河南喜乐融商贸有限公司
 * @Copyright: 2018 www.xlr.com Inc. All rights reserved. 
 */
package com.xlr.controller.company;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xlr.pojo.Protocol;
import com.xlr.pojo.Userinfo;
import com.xlr.service.company.ProtocolService;
import com.xlr.utils.EasyUIDataGridResult;
import com.xlr.utils.ManagerResult;
import com.xlr.utils.UserinfoUtils;

/**
 * @author 徐培珊
 * @date 2018年12月1日下午8:49:42
 */
@Controller
public class ProtocolController {

	@Autowired
	private ProtocolService protocolService;

	/**
	 * 入驻申请_根据session中的公司id获取协议信息
	 * 
	 * @author 徐培珊
	 * @date 2018年12月1日下午8:53:56
	 * @param companyId
	 * @return
	 */
	@RequestMapping("/protocol/select")
	@ResponseBody
	public Protocol selectProtocolByCompanyId() {
		Userinfo userinfo = UserinfoUtils.getUserinfo();
		ManagerResult result = protocolService.selectProtocolByCompanyId(userinfo.getUser_companyid());
		return (Protocol) result.getData();
	}
	
	@RequestMapping("/protocol/select/{a_companyid}")
	@ResponseBody
	public Protocol selectOneProtocolByCompanyId(@PathVariable("a_companyid")Integer a_companyid) {
		ManagerResult result = protocolService.selectProtocolByCompanyId(a_companyid);
		return (Protocol) result.getData();
	}
	@RequestMapping("/protocol/selectAll")
	@ResponseBody
	public EasyUIDataGridResult selectAllProtocol(@RequestParam(value="page",required=true,defaultValue="1")Integer page, 
			@RequestParam(value="rows",required=true,defaultValue="10")Integer rows) {
		EasyUIDataGridResult result = protocolService.selectAllProtocol(page, rows);
		System.out.println(result.getRows().size());
		return result;
	}

	/**
	 * 	入驻申请_根据表单提交的session中的id更新协议信息
	 * @author 徐培珊
	 * @date 2018年12月1日下午9:01:32
	 * @param protocol
	 * @return
	 */
	@RequestMapping("/protocol/update")
	@ResponseBody
	public ManagerResult updateProtocolByCompanyId(Protocol protocol) {
		Userinfo userinfo = UserinfoUtils.getUserinfo();
		protocol.setA_companyid(userinfo.getUser_companyid());
		return protocolService.updateProtocolByCompanyId(protocol);
	}
}
