package com.xlr.controller.company;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xlr.pojo.AssetsNameAndSeq;
import com.xlr.pojo.CompanyUsedAssets;
import com.xlr.pojo.Userinfo;
import com.xlr.service.company.AssetsService;
import com.xlr.service.company.CompanyUsedAssetsService;
import com.xlr.utils.EasyUIDataGridResult;
import com.xlr.utils.ManagerResult;
import com.xlr.utils.UserinfoUtils;


/**
 * @author 王宏志
 * 公司资产管理的Controller
 * @date 2018年12月4日下午6:19:20
 * @TOOD 
 */
@Controller
@RequestMapping("/company/asset")
public class AssetsController {
	@Autowired
	CompanyUsedAssetsService companyUsedAssetsService;
	
	@Autowired
	private AssetsService assetsService;
	/**   
	 * @Title: assetList   
	 * @Description: 根据公司ID将该公司的资产全部列出
	 * @param: @return      
	 * @return: List<CompanyUsedAssets>      
	 * @throws   
	 */
	@ResponseBody
	@RequestMapping("/get")
	public EasyUIDataGridResult assetList(Integer page, Integer rows){
		Userinfo userinfo = UserinfoUtils.getUserinfo();
		EasyUIDataGridResult easyUIDataGridResult = companyUsedAssetsService.selectAllCompanyAssertsByCompanyId(userinfo.getUser_companyid(),page,rows);
		return easyUIDataGridResult;
	}
	
	/**   
	 * @Title: assetAdd   
	 * @Description: 公司资产添加
	 * @param: @return      
	 * @return: ManagerResult      
	 * @throws   
	 */
	@ResponseBody
	@RequestMapping("/add")
	public ManagerResult assetAdd(CompanyUsedAssets companyUsedAssets) {
		return companyUsedAssetsService.addCompanyAssets(companyUsedAssets);
	}
	
	/**   
	 * @Title: assetdelete   
	 * @Description: 公司资产删除
	 * @param: @param itemId
	 * @param: @return      
	 * @return: ManagerResult      
	 * @throws   
	 */
	@ResponseBody
	@RequestMapping("/delete/{itemId}")
	public ManagerResult assetDelete(@PathVariable("itemId") Integer itemId) {
		return companyUsedAssetsService.deleteCompanyAssetsByID(itemId);
	}
	/**   
	 * @Title: assetUpdate   
	 * @Description: 公司资产信息修改
	 * @param: @param companyUsedAssets
	 * @param: @return      
	 * @return: ManagerResult      
	 * @throws   
	 */
	@ResponseBody
	@RequestMapping("/update")
	public ManagerResult assetUpdate(CompanyUsedAssets companyUsedAssets) {
		return companyUsedAssetsService.updateCompanyAssets(companyUsedAssets);
	}
	/**
	 * @author 王宏志
	 * 	获取全部资产的编号与名称
	 * @date 2018年12月6日下午3:03:14
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/allAssets")
	public List<AssetsNameAndSeq> getAllAssetsNameAndSeq(){
		return assetsService.getAssetsNameAndSeq();
	}
	
}
