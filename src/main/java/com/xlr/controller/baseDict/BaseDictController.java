package com.xlr.controller.baseDict;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xlr.pojo.BaseDict;
import com.xlr.service.baseDict.BaseDictervice;
import com.xlr.utils.EasyUIDataGridResult;
import com.xlr.utils.ManagerResult;

/**
 * 数据字典控制层
 * @author Zoe
 * @date 2019年2月13日下午8:27:15
 */
@Controller
@RequestMapping("/baseDict")
public class BaseDictController {
	
	@Autowired
	private BaseDictervice baseDictService;

	/**
	 * 查询所有数据_分页
	 * @author Zoe
	 * @date 2019年2月13日下午8:37:32
	 * @return List集合
	 */
	@RequestMapping(value="findAll")
	@ResponseBody
	public EasyUIDataGridResult findAll(@RequestParam(value = "page", required = true, defaultValue = "1") Integer page,
			@RequestParam(value = "rows", required = true, defaultValue = "10") Integer rows) {
		return baseDictService.findAll(page, rows);
	}
	
	/**
	 * 根据id查找数据
	 * @author Zoe
	 * @date 2019年2月14日下午1:00:03
	 * @param dict_id
	 * @return
	 */
	@RequestMapping(value="findById")
	@ResponseBody
	public BaseDict findById(Integer dict_id) {
		return baseDictService.findById(dict_id);
	}
	
	/**
	 * 添加数据
	 * @author Zoe
	 * @date 2019年2月13日下午10:26:17
	 * @return
	 */
	@RequestMapping(value="insert")
	@ResponseBody
	public ManagerResult insert(BaseDict baseDict) {
		return baseDictService.insert(baseDict);
	}
	
	/**
	 * 根据id删除数据[修改状态]
	 * @author Zoe
	 * @date 2019年2月14日上午10:25:29
	 * @param dict_id 主键id
	 * @return
	 */
	@RequestMapping(value="deleteById")
	@ResponseBody
	public ManagerResult deleteById(Integer dict_id) {
		return baseDictService.deleteById(dict_id);
	}
	
	/**
	 * 根据id修改数据
	 * @author Zoe
	 * @date 2019年2月14日下午12:01:49
	 * @param baseDict 数据字典对象
	 * @return
	 */
	@RequestMapping(value="updateById")
	@ResponseBody
	public ManagerResult updateById(BaseDict baseDict) {
		return baseDictService.updateById(baseDict);
	}
	
	/**
	 * 查找类型名称
	 * @author Zoe
	 * @date 2019年2月14日下午3:14:47
	 * @return
	 */
	@RequestMapping(value="findTypeName")
	@ResponseBody
	public List<BaseDict> findTypeName() {
		return baseDictService.findTypeName();
	}
	/**
	 * 多条件查询
	 * @author Zoe
	 * @date 2019年2月14日下午3:30:46
	 * @return
	 */
	@RequestMapping(value="findByCriteria")
	@ResponseBody
	public EasyUIDataGridResult findByCriteria(BaseDict baseDict,@RequestParam(value = "page", required = true, defaultValue = "1") Integer page,
			@RequestParam(value = "rows", required = true, defaultValue = "10") Integer rows) {
		return baseDictService.findByCriteria(baseDict, page, rows);
	}
}
