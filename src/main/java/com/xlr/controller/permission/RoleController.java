package com.xlr.controller.permission;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xlr.pojo.Role;
import com.xlr.pojo.Userinfo;
import com.xlr.service.permission.RoleService;
import com.xlr.utils.EasyUIDataGridResult;
import com.xlr.utils.EasyUIOptionalTreeNode;
import com.xlr.utils.ManagerResult;
import com.xlr.utils.UserinfoUtils;
/**
 * 
 * @ClassName: RoleController 
 * @Description: 接收角色相关请求
 * @author: gj
 * @date: 2019年2月14日 下午6:48:15
 */
@Controller
public class RoleController {

	@Autowired
	private RoleService roleService;
	/**
	 * 
	* @Title: findRoleByEnble 
	* @Description: 查询角色列表 
	* @param enble
	* @param page
	* @param rows
	* @return EasyUIDataGridResult
	* @author gj
	* @date 2019年2月14日下午6:48:36
	 */
	@RequestMapping("/permission/rolelistByPage")
	@ResponseBody
	public EasyUIDataGridResult rolelistByPage(Role role,
			@RequestParam(value = "page", required = true, defaultValue = "1") Integer page,
			@RequestParam(value = "rows", required = true, defaultValue = "10") Integer rows) {
		EasyUIDataGridResult result = roleService.findRoleByPage(page, rows, role);
		return result;
	}
	/**
	 * 
	* @Title: updateRole 
	* @Description: 接收更新角色的数据 
	* @param role
	* @return ManagerResult
	* @author gj
	* @date 2019年2月14日下午8:19:07
	 */
	@RequestMapping(value = "/permission/roleupdate" , method = RequestMethod.POST)
	@ResponseBody
	public ManagerResult updateRole(Role role) {
		ManagerResult result = roleService.updateRole(role);
		return result;
	}
	/**
	 * 
	* @Title: addRole 
	* @Description: 添加角色 
	* @param role
	* @return ManagerResult
	* @author gj
	* @date 2019年2月15日上午11:52:43
	 */
	@RequestMapping(value = "/permission/roleadd" , method = RequestMethod.POST)
	@ResponseBody
	public ManagerResult addRole(Role role) {
		Userinfo userinfo = UserinfoUtils.getUserinfo();
		role.setCreate_name(userinfo.getUser_code());
		ManagerResult result = roleService.addRole(role);
		return result;
	}
	/**
	 * 
	* @Title: rolelistByEnble 
	* @Description: 返回datagrid格式json 
	* @return EasyUIDataGridResult
	* @author gj
	* @date 2019年2月16日下午12:36:00
	 */
	@RequestMapping(value = "/permission/rolelistByEnble")
	@ResponseBody
	public EasyUIDataGridResult rolelistByEnble() {
		EasyUIDataGridResult result = roleService.findRoleByEnble();
		return result;
	}
	/**
	 * 
	* @Title: findRoleMenuByRoleid 
	* @Description: 根据角色id加载对应权限菜单 
	* @param roleuuid
	* @return List<EasyUIOptionalTreeNode>
	* @author gj
	* @date 2019年2月16日下午7:19:56
	 */
	@RequestMapping(value = "/permission/findRoleMenuByRoleid" , method = RequestMethod.POST)
	@ResponseBody
	public List<EasyUIOptionalTreeNode> findRoleMenuByRoleid(
			@RequestParam(value = "id", required = true)Integer roleuuid) {
		return roleService.findRoleMenuByRoleid(roleuuid);
	}
	/**
	 * 
	* @Title: updateRoleMenus 
	* @Description: 更新角色权限菜单 
	* @param roleuuid
	* @param checkedIds
	* @return ManagerResult
	* @author gj
	* @date 2019年2月16日下午8:16:05
	 */
	@RequestMapping(value = "/permission/updateRoleMenus" , method = RequestMethod.POST)
	@ResponseBody
	public ManagerResult updateRoleMenus(
			@RequestParam(value = "id", required = true) Integer roleuuid,
			@RequestParam(value = "checkedIds", required = true) String checkedIds) {
		ManagerResult result = roleService.updateRoleMenus(roleuuid, checkedIds);
		return result;
	}
	
}
