package com.xlr.controller.permission;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xlr.pojo.Menus;
import com.xlr.pojo.Userinfo;
import com.xlr.service.permission.MenusService;
import com.xlr.utils.ManagerResult;
import com.xlr.utils.Tree;
import com.xlr.utils.UserinfoUtils;

/**
 * 菜单管理控制
 * @author Zoe
 * @date 2019年2月15日下午3:44:00
 */
@Controller
@RequestMapping("/menus")
public class MenusController {

	@Autowired
	private MenusService menusService;
	
	/**
	 * 查找所有
	 * @author Zoe
	 * @date 2019年2月15日下午3:46:48
	 * @return
	 */
	@RequestMapping(value="findAll")
	@ResponseBody
	public List<Tree> findAll() {
		return menusService.findAll(); 
	}
	
	/**
	 * 根据菜单id查找菜单，显示菜单详情
	 * @author Zoe
	 * @date 2019年2月15日下午8:15:02
	 * @param menuid 主键
	 * @return
	 */
	@RequestMapping("findById")
	@ResponseBody
	public List<Menus> findById(String menuid) {
		return menusService.findById(menuid);
	}
	/**
	 * 添加数据
	 * @author Zoe
	 * @date 2019年2月15日下午9:47:56
	 * @param menus 菜单对象
	 * @return
	 */
	@RequestMapping(value="insert")
	@ResponseBody
	public ManagerResult insert(Menus menus) {
		return menusService.insert(menus);
	}
	
	/**
	 * 根据id删除数据[修改状态]
	 * @author Zoe
	 * @date 2019年2月15日下午9:47:41
	 * @param menuid 主键
	 * @return
	 */
	@RequestMapping(value="deleteById")
	@ResponseBody
	public ManagerResult deleteById(String menuid) {
		return menusService.deleteById(menuid);
	}
	
	/**
	 * 根据id修改数据
	 * @author Zoe
	 * @date 2019年2月15日下午9:48:22
	 * @param menus 菜单对象
	 * @return
	 */
	@RequestMapping(value="updateById")
	@ResponseBody
	public ManagerResult updateById(Menus menus) {
		return menusService.updateById(menus);
	}
	
	/**
	 * 
	* @Title: loadMenus 
	* @Description: 根据session中的user_id加载菜单
	* @return Menus
	* @author gj
	* @date 2019年2月16日下午9:18:20
	 */
	@RequestMapping(value="loadMenus")
	@ResponseBody
	public Menus loadMenus() {
		Userinfo userinfo = UserinfoUtils.getUserinfo();
		Menus menus = menusService.findMenusByUserid(userinfo.getUser_id());
		return menus;
	}
}
