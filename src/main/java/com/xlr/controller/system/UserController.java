/**
 * @author 高俊
 * @date 2018年11月1日下午8:08:41
 * @version 1.0
 * @company 河南喜乐融商贸有限公司
 * @Copyright: 2018 www.xlr.com Inc. All rights reserved. 
 */
package com.xlr.controller.system;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpSession;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.xlr.dto.SimpleUser;
import com.xlr.pojo.Userinfo;
import com.xlr.service.system.UserService;
import com.xlr.utils.EasyUIDataGridResult;
import com.xlr.utils.EasyUIOptionalTreeNode;
import com.xlr.utils.ManagerResult;
import com.xlr.utils.UserinfoUtils;

/**
 * @author 高俊
 * @date 2018年11月1日下午8:08:41
 */
@Controller
public class UserController {
	@Autowired
	private UserService userService;

//	@RequestMapping(value = "/menu/index")
//	@ResponseBody
//	public String index() {
//		Userinfo userinfo = UserinfoUtils.getUserinfo();
//		List<Menus> list = new LinkedList<Menus>();
//		if (userinfo == null) {
//			return "";
//		}
//		list = userService.findMenuByUserType(userinfo.getUser_type());
//		String jsonString = JSON.toJSONString(list);
//		String menusjson = "{\"menus\":" + jsonString + "}";
//		return menusjson;
//	}

	@RequestMapping(value = "/user/login", method = RequestMethod.POST)
	@ResponseBody
	public ManagerResult login(Userinfo userinfo) {
		try {
			// 1.创建令牌
			UsernamePasswordToken token = new UsernamePasswordToken(userinfo.getUser_code(), userinfo.getUser_pwd());
			// 2.获取主题subject
			Subject subject = SecurityUtils.getSubject();
			// 3.执行login方法
			subject.login(token);
			return ManagerResult.build(200, "");
		} catch (AuthenticationException e) {
			e.printStackTrace();
			return ManagerResult.build(400, "账号或密码错误");
		}
	}

	@RequestMapping(value = "/user/list", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public EasyUIDataGridResult list(Userinfo userinfo,
			@RequestParam(value = "page", required = true, defaultValue = "1") Integer page,
			@RequestParam(value = "rows", required = true, defaultValue = "30") Integer rows) {
		EasyUIDataGridResult result = userService.findUserinfoList(userinfo, page, rows);
		return result;
	}

	/*
	 * 用户名自动补全
	 */
	@RequestMapping(value = "/user/UserName", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public List<Userinfo> getUserNameList(String q) {
		List<Userinfo> userName = userService.findUserName(q);
		return userName;
	}

	@RequestMapping(value = "/user/add", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public ManagerResult add(Userinfo userinfo, MultipartFile image, HttpSession session)
			throws IllegalStateException, IOException {
		if (!image.getOriginalFilename().equals("")) {
			// 图片新名字
			String newName = UUID.randomUUID().toString();
			// 图片原来的名字
			String oldName = image.getOriginalFilename();
			// 后缀
			String sux = oldName.substring(oldName.lastIndexOf("."));
			// 新建本地文件流
			String realPath = session.getServletContext().getRealPath("/");
			File file = new File(realPath + "/upload/head/" + newName + sux);
			// 写入本地磁盘
			image.transferTo(file);
			userinfo.setUser_image("/upload/head/" + newName + sux);
		}
		ManagerResult result = userService.insertUserInfo(userinfo);
		return result;
	}

	@RequestMapping(value = "/user/delete", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public ManagerResult detele(Integer user_id) {
		ManagerResult result = userService.deleteUserInfo(user_id);
		return result;
	}

	@RequestMapping(value = "/user/update", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public ManagerResult update(Userinfo userinfo, MultipartFile image, HttpSession session)
			throws IllegalStateException, IOException {
		if (!image.getOriginalFilename().equals("")) {
			// 图片新名字
			String newName = UUID.randomUUID().toString();
			// 图片原来的名字
			String oldName = image.getOriginalFilename();
			// 后缀
			String sux = oldName.substring(oldName.lastIndexOf("."));
			// 新建本地文件流
			String realPath = session.getServletContext().getRealPath("/");
			File file = new File(realPath + "/upload/head/" + newName + sux);
			// 写入本地磁盘
			image.transferTo(file);
			userinfo.setUser_image("/upload/head/" + newName + sux);
		}
		ManagerResult result = userService.updateUserInfo(userinfo);
		return result;
	}

	/**
	 * 修改密码
	 * 
	 * @param userinfo
	 * @return
	 */
	@RequestMapping(value = "/user/updateUserPwdByIdAndCode", method = RequestMethod.POST)
	@ResponseBody
	public ManagerResult updateUserPwdByIdAndCode(String user_pwd) {
		Integer user_id = UserinfoUtils.getUserinfo().getUser_id();
		ManagerResult result = userService.resetPwd(user_id,user_pwd);
		return result;
	}

	/**
	 * 
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping(value = "/user/simplelist", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public EasyUIDataGridResult simplelist(
			SimpleUser user,
			@RequestParam(value = "page", required = true, defaultValue = "1") Integer page,
			@RequestParam(value = "rows", required = true, defaultValue = "30") Integer rows) {
		EasyUIDataGridResult result = userService.findSimpleUser(user,page, rows);
		return result;
	}

	/**
	 * 
	 * @Title: resetPwd
	 * @Description: 重置密码
	 * @param userId
	 * @param userPwd
	 * @return ManagerResult
	 * @author gj
	 * @date 2019年2月17日下午9:14:09
	 */
	@RequestMapping(value = "/user/resetPwd", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public ManagerResult resetPwd(@RequestParam(value = "user_id", required = true) Integer userId,
			@RequestParam(value = "user_pwd", required = true) String userPwd) {
		ManagerResult result = userService.resetPwd(userId, userPwd);
		return result;
	}

	/**
	 * 
	 * @Title: findUserRole
	 * @Description: esayui tree默认POST方法
	 * @param user_id
	 * @return List<EasyUIOptionalTreeNode>
	 * @author gj
	 * @date 2019年2月16日下午3:23:42
	 */
	@RequestMapping(value = "/user/findUserRole", method = { RequestMethod.POST })
	@ResponseBody
	public List<EasyUIOptionalTreeNode> findUserRole(@RequestParam(value = "id", required = true) Integer user_id) {
		List<EasyUIOptionalTreeNode> treeList = userService.findUserRole(user_id);
		return treeList;
	}

	/**
	 * 
	 * @Title: updateUserRole
	 * @Description: 更新用户对应的权限
	 * @param user_id
	 * @param checkedIds
	 * @return ManagerResult
	 * @author gj
	 * @date 2019年2月16日下午4:05:33
	 */
	@RequestMapping(value = "/user/updateUserRole", method = { RequestMethod.POST })
	@ResponseBody
	public ManagerResult updateUserRole(@RequestParam(value = "id", required = true) Integer user_id,
			@RequestParam(value = "checkedIds", required = true) String checkedIds) {
		ManagerResult result = userService.updateUserRole(user_id, checkedIds);
		return result;
	}
	/**
	 * 
	* @Title: showName 
	* @Description: 显示用户名 
	* @return Map
	* @author gj
	* @date 2019年2月21日下午12:02:45
	 */
	@RequestMapping(value = "/user/showName")
	@ResponseBody
	public Map showName() {
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("user_name",UserinfoUtils.getUserinfo().getUser_name());
		return result;
	}

	@RequestMapping(value = "/logout")
	public String logout() {
		SecurityUtils.getSubject().logout();
		return "login";
	}

}
