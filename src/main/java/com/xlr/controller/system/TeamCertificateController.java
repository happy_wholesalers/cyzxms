/**
 * @author 徐培珊
 * @date 2018年12月11日下午3:37:59
 * @version 1.0
 * @company 河南喜乐融商贸有限公司
 * @Copyright: 2018 www.xlr.com Inc. All rights reserved. 
 */
package com.xlr.controller.system;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.xlr.pojo.TeamCertificate;
import com.xlr.service.system.TeamCertificateService;

/**
 * 	团队证书
 * @author 徐培珊
 * @date 2018年12月11日下午3:37:59
 */
@Controller
public class TeamCertificateController {
	@Autowired
	private TeamCertificateService teamCertificateService;
	
	/**
	 * 查找所有证书
	 * @author 徐培珊
	 * @date 2018年12月11日下午3:48:12
	 * @return
	 */
	@RequestMapping("/systemManage/teamCertificate")
	@ResponseBody
	public String findAllCertificate() {
		List<TeamCertificate> certificatelist = teamCertificateService.findAllCertificate();
		String jsonString = JSON.toJSONString(certificatelist);
		String totallicencejson = "{\"all\":" + jsonString + "}";
		return totallicencejson;
	}
}
