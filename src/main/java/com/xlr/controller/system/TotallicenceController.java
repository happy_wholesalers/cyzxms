/**
 * @date 2018年11月27日下午3:02:59
 * @version 1.0
 * @company 河南喜乐融商贸有限公司
 * @Copyright: 2018 www.xlr.com Inc. All rights reserved. 
 */
package com.xlr.controller.system;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.xlr.pojo.Company;
import com.xlr.service.system.TotallicenceService;
import com.xlr.utils.ManagerResult;

/**
 * @author 李贺鹏
 * @date 2018年11月27日下午3:02:59
 */
@Controller
public class TotallicenceController {
	@Autowired
	private TotallicenceService totallicenceService;
	
	@RequestMapping(value = "/systemManage/totallicence")
	@ResponseBody
	public String findTotallicence() {
		List<Company> totallicenceList = totallicenceService.totallicenceList();
		String jsonString = JSON.toJSONString(totallicenceList);
		String totallicencejson = "{\"all\":" + jsonString + "}";
		return totallicencejson;
	}
	
	
	
	
	
	
}
