/**
 * @author 徐培珊
 * @date 2018年12月4日下午6:10:10
 * @version 1.0
 * @company 河南喜乐融商贸有限公司
 * @Copyright: 2018 www.xlr.com Inc. All rights reserved. 
 */
package com.xlr.controller.department;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xlr.pojo.Company;
import com.xlr.pojo.Userinfo;
import com.xlr.service.company.CompanyService;
import com.xlr.service.department.BelongToDeptService;
import com.xlr.utils.EasyUIDataGridResult;
import com.xlr.utils.ManagerResult;
import com.xlr.utils.UserinfoUtils;

/**
 * @author 徐培珊
 * @date 2018年12月4日下午6:10:10
 */
@Controller
public class BelongToDepartmentController {

	@Autowired
	BelongToDeptService belongToDeptService;

	@Autowired
	CompanyService companyService;

	/**
	 * 院系公司管理_根据session中的院系id加载公司列表_分页
	 * 
	 * @author 徐培珊
	 * @date 2018年12月4日下午6:13:25
	 * @param session
	 * @return
	 */
	@RequestMapping("/dept/belongToDeptByPage")
	@ResponseBody
	public EasyUIDataGridResult BelongsToDeptByPage(
			Company company,
			@RequestParam(value = "page", required = true, defaultValue = "1") Integer page,
			@RequestParam(value = "rows", required = true, defaultValue = "10") Integer rows) {
		Userinfo userinfo =UserinfoUtils.getUserinfo();
		Integer deptId = userinfo.getUser_facilityid();
		company.setCompany_departmentid(deptId);
		return belongToDeptService.findCompanyBypage(company, page, rows);
	}

	/**
	 * 院系公司管理_更新公司信息
	 * 
	 * @author 徐培珊
	 * @date 2018年12月4日下午7:23:47
	 * @param company
	 * @return
	 */
	@RequestMapping("/dept/updateCompanyInfo")
	@ResponseBody
	public ManagerResult updateCompanyInfo(Company company) {
		return companyService.updateCompanyInfo(company);
	}

	/**
	 * 入驻申请统计_根据公司id删除公司信息
	 * 
	 * @author 徐培珊
	 * @date 2018年11月27日下午4:32:17
	 * @param company_id
	 * @return
	 */
	@RequestMapping("/dept/delete/{company_id}")
	@ResponseBody
	public ManagerResult deleteCompanyInfoById(
			@PathVariable("company_id") Integer company_id) {
		return companyService.deleteCompanyInfoById(company_id);
	}

	/**
	 * 入驻申请统计_添加公司信息_院系id确定不变
	 * 
	 * @author 徐培珊
	 * @date 2018年11月27日下午5:30:09
	 * @param company
	 * @return
	 */
	@RequestMapping("/dept/addCompanyInfo")
	@ResponseBody
	public ManagerResult addCompanyInfo(Company company) {
		Userinfo userinfo = UserinfoUtils.getUserinfo();
		Integer facilityid = userinfo.getUser_facilityid();
		company.setCompany_id(facilityid);
		return companyService.addCompanyInfo(company);
	}
	/**
	 * @author 李贺鹏
	 * @date 2019年2月16日下午8:48:49
	 * @param q 前端输入的模糊负责人名
	 * @return 公司集合
	 */
	@RequestMapping(value = "/dept/leaderName", method = { RequestMethod.POST,
			RequestMethod.GET })
	@ResponseBody
	public List<Company> findDepCompanyLeaderName(String q){
		/*Company company=new Company();
		company.setCompany_leader(q);*/
		
		return belongToDeptService.findDepCompanyLeaderName(q);
	}
}
