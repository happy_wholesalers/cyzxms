package com.xlr.controller.department;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xlr.pojo.Company;
import com.xlr.pojo.Team;

import com.xlr.service.department.TeamService;
import com.xlr.utils.EasyUIDataGridResult;
import com.xlr.utils.ManagerResult;

/**
 * @author 王宏志 院系团队的Controller
 * @date 2018年12月11日下午4:42:19
 * @TOOD
 */
@Controller
@RequestMapping("/dep/team")
public class TeamController {

	@Autowired
	private TeamService teamService;

	/**
	 * @author 王宏志
	 * @date 2018年12月11日下午4:51:38 列出全部的团队信息
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping(value = "/get")
	@ResponseBody
	public EasyUIDataGridResult index(
			Team team,
			@RequestParam(value = "page", required = true, defaultValue = "1") Integer page,
			@RequestParam(value = "rows", required = true, defaultValue = "10") Integer rows) {
		return teamService.findAllTeamByPage(team, page, rows);
	}

	/**
	 * @author 王宏志 修改团队信息
	 * @date 2018年12月11日下午4:53:15
	 * @param team
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/update", produces = "application/json;charset=utf-8")
	public ManagerResult updateCompanyInfo(Team team) {
		return teamService.updateTeam(team);
	}

	/**
	 * @author 王宏志
	 * @date 2018年12月11日下午4:54:13 修改团队信息
	 * @param t_id
	 * @return
	 */

	@ResponseBody
	@RequestMapping(value = "/delete/{t_id}", produces = "application/json;charset=utf-8")
	public ManagerResult deleteTeam(@PathVariable("t_id") Integer t_id) {
		return teamService.deleteTeamById(t_id);
	}

	/**
	 * @author 王宏志
	 * @date 2018年12月11日下午4:55:20 添加团队信息
	 * @param team
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/add", produces = "application/json;charset=utf-8")
	public ManagerResult addTeam(Team team) {
		return teamService.addTeam(team);
	}
	/**
	 * @author 李贺鹏
	 * @date 2019年2月16日下午8:48:49
	 * @param q 前端输入的模糊负责人名
	 * @return 
	 */
	@RequestMapping(value = "/findLeaderName", method = { RequestMethod.POST,
			RequestMethod.GET })
	@ResponseBody
	public List<Team> findDeptTeamLeaderName(String q){
		/*Company company=new Company();
		company.setCompany_leader(q);*/
		
		return teamService.findDeptTeamLeaderName(q);
	}
	/**
	 * @author 李贺鹏
	 * @date 2019年2月16日下午8:48:49
	 * 
	 * @return 获取所有的团队
	 */
	
	@RequestMapping(value = "/findTeamName", method = { RequestMethod.POST,
			RequestMethod.GET })
	@ResponseBody
	public List<Team> findDeptTeamName(){
		/*Company company=new Company();
		company.setCompany_leader(q);*/
		
		return teamService.findDeptTeamLeaderName(null);
	}

}