/**
 * @author 高俊
 * @date 2018年12月3日下午7:44:54
 * @version 1.0
 * @company 河南喜乐融商贸有限公司
 * @Copyright: 2018 www.xlr.com Inc. All rights reserved. 
 */
package com.xlr.controller.assets;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xlr.service.assets.AssetlendService;
import com.xlr.utils.EasyUIDataGridResult;

/**
 * @author 高俊
 * @date 2018年12月3日下午7:44:54
 */
@Controller
@RequestMapping("/public/assetlend")
public class AssetlendController {
	
	@Autowired
	private AssetlendService assetlendService;
	
	@ResponseBody
	@RequestMapping("/get")
	public EasyUIDataGridResult assetlendList(@RequestParam(value="page", defaultValue="1")Integer page, @RequestParam(value="rows", defaultValue="10")Integer rows){
		EasyUIDataGridResult result = assetlendService.findAllCompanyUsedAssets(page,rows);
		return result;
	}
}
