package com.xlr.controller.assets;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xlr.pojo.PublicAssets;
import com.xlr.service.assets.PublicAssetsService;
import com.xlr.utils.EasyUIDataGridResult;
import com.xlr.utils.ManagerResult;



/**
 * @author 王宏志
 * 			公共资产的Controller
 * @date 2018年11月27日下午4:53:33
 */
@Controller
@RequestMapping("/public/asset")
public class PublicAssetsController {

	@Autowired
	private PublicAssetsService publicAssetsService;
	
	/**
	 * @author 王宏志
	 * 			展示总资产信息
	 * @date 2018年11月27日下午5:01:46
	 * @param page
	 * @param rows
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/get")
	public EasyUIDataGridResult assetList(
			@RequestParam(value="page", defaultValue="1")Integer page, 
			@RequestParam(value="rows", defaultValue="10")Integer rows){
		EasyUIDataGridResult easyUIDataGridResult = publicAssetsService.getAllAssets(page, rows);
		return easyUIDataGridResult;
	}
	/**
	 * @author 王宏志
	 * @date 2018年11月28日下午3:34:52
	 * 总资产添加
	 * @param publicAssets
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/add",produces="application/json;charset=utf-8")
	public ManagerResult addAssets(PublicAssets publicAssets){
		ManagerResult managerResult = publicAssetsService.addAssets(publicAssets);
		return managerResult;
	}
	/**
	 * @author 王宏志
	 * @date 2018年11月28日下午3:36:11
	 * 总资产信息修改
	 * @param publicAssets
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/update")
	public ManagerResult updateAssets(PublicAssets publicAssets){
		ManagerResult managerResult = publicAssetsService.updateAssets(publicAssets);
		return managerResult;
	}
	/**
	 * @author 王宏志
	 * @date 2018年11月28日下午3:49:08
	 * 从总资产中删除一项资产
	 * @param assetsId
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/delete/{assetsId}")
	public ManagerResult deleteAssets(@PathVariable Integer assetsId){
		ManagerResult managerResult = publicAssetsService.deleteAssets(assetsId);
		return managerResult;
	}
	
}
