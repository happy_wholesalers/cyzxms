package com.xlr.service.permission;

import java.util.List;

import com.xlr.pojo.Menus;
import com.xlr.utils.ManagerResult;
import com.xlr.utils.Tree;

/**
 * 菜单管理接口
 * @author Zoe
 * @date 2019年2月15日下午3:47:44
 */
public interface MenusService {

	/**
	 * 查找所有数据
	 * @author Zoe
	 * @date 2019年2月15日下午3:48:22
	 * @return
	 */
	List<Tree> findAll();

	/**
	 * 根据菜单id查找菜单，显示菜单详情
	 * @author Zoe
	 * @date 2019年2月15日下午8:16:07
	 * @param menuid 主键
	 * @return
	 */
	List<Menus> findById(String menuid);

	/**
	 * 添加数据
	 * @author Zoe
	 * @date 2019年2月15日下午9:50:29
	 * @param menus 菜单对象
	 * @return
	 */
	ManagerResult insert(Menus menus);
	
	/**
	 * 根据id删除数据[修改状态]
	 * @author Zoe
	 * @date 2019年2月15日下午9:49:00
	 * @param menuid 主键
	 * @return
	 */
	ManagerResult deleteById(String menuid);

	/**
	 * 根据id修改数据
	 * @author Zoe
	 * @date 2019年2月15日下午9:51:09
	 * @param menus 菜单对象
	 * @return
	 */
	ManagerResult updateById(Menus menus);
	
	/**
	 * 
	* @Title: findMenusByUserid 
	* @Description: 根据userid加载对应菜单 
	* @param userid
	* @return Menus
	* @author gj
	* @date 2019年2月16日下午8:43:39
	 */
	Menus findMenusByUserid(Integer userid);
	/**
	 * 
	* @Title: findMenuListByUserid 
	* @Description: 根据userid加载对应菜单无序列表 
	* @param userid
	* @return List<Menus>
	* @author gj
	* @date 2019年2月17日下午8:55:10
	 */
	List<Menus> findMenusListByUserid(Integer userid);
	
}
