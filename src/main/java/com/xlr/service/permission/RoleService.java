package com.xlr.service.permission;

import java.util.List;

import com.xlr.pojo.Role;
import com.xlr.utils.EasyUIDataGridResult;
import com.xlr.utils.EasyUIOptionalTreeNode;
import com.xlr.utils.ManagerResult;

/**
 * 
 * @ClassName: RoleService 
 * @Description: 处理角色相关业务
 * @author: gj
 * @date: 2019年2月14日 下午6:31:37
 */
public interface RoleService {
	/**
	 * 
	* @Title: findRoleByPage 
	* @Description: 根据查询条件所有角色 
	* @param page
	* @param rows
	* @param role
	* @return EasyUIDataGridResult
	* @author gj
	* @date 2019年2月14日下午7:08:40
	 */
	public EasyUIDataGridResult findRoleByPage(Integer page, Integer rows,Role role);
	/**
	 * 
	* @Title: findRoleByEnble 
	* @Description: 查询所有可用的角色 
	* @return EasyUIDataGridResult
	* @author gj
	* @date 2019年2月16日下午12:34:06
	 */
	public EasyUIDataGridResult findRoleByEnble();
	
	/**
	 * 
	* @Title: updateRole 
	* @Description: 更新角色的名称或者状态 
	* @param role
	* @return ManagerResult
	* @author gj
	* @date 2019年2月14日下午8:18:37
	 */
	public ManagerResult updateRole(Role role);
	/**
	 * 
	* @Title: addRole 
	* @Description: 添加角色 
	* @param role
	* @return ManagerResult
	* @author gj
	* @date 2019年2月14日下午10:11:23
	 */
	public ManagerResult addRole(Role role);
	/**
	 * 
	* @Title: findRoleMenu 
	* @Description: 获取角色菜单权限 
	-- 1.根据角色id获取对应的权限菜单id,比如角色id roleuuid=1
	select role_menus.menuuuid from role,role_menus WHERE role_menus.roleuuid=1;
	-- 2.获取所有权限菜单(menuid,menuname)
	SELECT menuid,menuname FROM menus;
	* @param roleuuid
	* @return List<EasyUIOptionalTreeNode>
	* @author gj
	* @date 2019年2月16日下午4:40:34
	 */
	public List<EasyUIOptionalTreeNode> findRoleMenuByRoleid(Integer roleuuid);
	/**
	 * 
	* @Title: updateRoleMenus 
	* @Description: 更新角色权限菜单 
	* @param roleuuid
	* @param checkedIds
	* @return ManagerResult
	* @author gj
	* @date 2019年2月16日下午8:10:20
	 */
	public ManagerResult updateRoleMenus(Integer roleuuid, String checkedIds);
}
