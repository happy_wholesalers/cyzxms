package com.xlr.service.assets;

import java.util.List;

import com.xlr.pojo.CompanyUsedAssets;
import com.xlr.pojo.PublicAssets;
import com.xlr.utils.EasyUIDataGridResult;
import com.xlr.utils.ManagerResult;

/**
 * @author 王宏志
 * @date 2018年11月27日下午4:36:53
 */
public interface PublicAssetsService {

	/**
	 * @author 王宏志
	 * 			查询总资产
	 * @date 2018年11月27日下午4:46:42
	 * @return
	 */
	public EasyUIDataGridResult getAllAssets(Integer page, Integer rows);
	/**
	 * @author 王宏志
	 * @date 2018年11月27日下午8:51:52
	 * 添加总资产
	 * @param publicAssets
	 * @return
	 */
	public ManagerResult addAssets(PublicAssets publicAssets);
	/**
	 * @author 王宏志
	 * @date 2018年11月28日下午3:30:10
	 * 修改资产信息
	 * @param publicAssets
	 * @return
	 */
	public ManagerResult updateAssets(PublicAssets publicAssets);
	/**
	 * @author 王宏志
	 * @date 2018年11月28日下午3:45:29
	 * 从总资产中删除一条记录
	 * @param assetsId
	 * @return
	 */
	public ManagerResult deleteAssets(Integer assetsId);
}
