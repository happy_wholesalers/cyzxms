/**
 * @author 高俊
 * @date 2018年12月3日下午7:55:37
 * @version 1.0
 * @company 河南喜乐融商贸有限公司
 * @Copyright: 2018 www.xlr.com Inc. All rights reserved. 
 */
package com.xlr.service.assets;

import com.xlr.utils.EasyUIDataGridResult;

/**
 * @author 高俊
 * @date 2018年12月3日下午7:55:37
 */
public interface AssetlendService {
	/**
	 * 查询所有外借物品
	 * @author 高俊
	 * @date 2018年12月3日下午7:56:13
	 * @return
	 */
	public EasyUIDataGridResult findAllCompanyUsedAssets(Integer page, Integer rows);
}
