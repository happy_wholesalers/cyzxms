package com.xlr.service.baseDict;

import java.util.List;

import com.xlr.pojo.BaseDict;
import com.xlr.utils.EasyUIDataGridResult;
import com.xlr.utils.ManagerResult;

/**
 * 数据字典逻辑接口
 * @author Zoe
 * @date 2019年2月13日下午8:40:05
 */
public interface BaseDictervice {

	/**
	 * 查询所有数据
	 * @author Zoe
	 * @param rows 每页记录数
	 * @param page 页号
	 * @date 2019年2月13日下午8:41:52
	 * @return
	 */
	EasyUIDataGridResult findAll(Integer page, Integer rows);

	/**
	 * 添加数据
	 * @author Zoe
	 * @date 2019年2月13日下午10:29:48
	 * @param baseDict 数据字典对象
	 * @return
	 */
	ManagerResult insert(BaseDict baseDict);

	/**
	 * 根据id删除数据[修改状态]
	 * @author Zoe
	 * @date 2019年2月14日上午10:12:41
	 * @param dict_id 主键id
	 */
	ManagerResult deleteById(Integer dict_id);

	/**
	 * 根据id修改数据
	 * @author Zoe
	 * @date 2019年2月14日下午12:01:05
	 * @param baseDict 数据字典对象
	 * @return
	 */
	ManagerResult updateById(BaseDict baseDict);

	/**
	 * 根据id查找数据
	 * @author Zoe
	 * @date 2019年2月14日下午1:00:57
	 * @param dict_id 主键id
	 * @return
	 */
	BaseDict findById(Integer dict_id);

	/**
	 * 查找类型名称
	 * @author Zoe
	 * @date 2019年2月14日下午3:10:35
	 * @return
	 */
	List<BaseDict> findTypeName();

	/**
	 *  多条件查询
	 * @author Zoe
	 * @date 2019年2月14日下午3:31:54
	 * @param baseDict 数据字典对象
	 * @param rows 每页记录数
	 * @param page 页数
	 * @return
	 */
	EasyUIDataGridResult findByCriteria(BaseDict baseDict, Integer page, Integer rows);
}
