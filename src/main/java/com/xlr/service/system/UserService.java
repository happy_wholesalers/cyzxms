/**
 * @author 徐培珊
 * @date 2018年11月1日下午8:42:29
 * @version 1.0
 * @company 河南喜乐融商贸有限公司
 * @Copyright: 2018 www.xlr.com Inc. All rights reserved. 
 */
package com.xlr.service.system;

import java.util.List;

import com.xlr.dto.SimpleUser;
import com.xlr.dto.UserCollegeAndTeam;
import com.xlr.pojo.Company;
import com.xlr.pojo.Menus;
import com.xlr.pojo.Role;
import com.xlr.pojo.Userinfo;
import com.xlr.utils.EasyUIDataGridResult;
import com.xlr.utils.EasyUIOptionalTreeNode;
import com.xlr.utils.ManagerResult;

/**
 * @author 徐培珊
 * @date 2018年11月1日下午8:42:29
 */
public interface UserService {

	/**
	 * 
	 * @Title: findUserInfo
	 * @Description: 根据用户名和密码查询用户信息
	 * @param user_code
	 * @param user_pwd
	 * @return Userinfo
	 * @author gj
	 * @date 2019年2月17日下午9:10:35
	 */
	Userinfo findUserInfo(String user_code, String user_pwd);

	/**
	 * 查询用户列表
	 * 
	 * @author 高俊
	 * @date 2018年11月1日下午10:06:47
	 * @param page
	 * @param rows
	 * @return
	 */
	EasyUIDataGridResult findUserinfoList(Userinfo userInfo, Integer page, Integer rows);

	/**
	 * 更新用户信息
	 * 
	 * @author 徐培珊
	 * @date 2018年11月2日下午2:21:05
	 * @param userInfo
	 * @return
	 */
	ManagerResult updateUserInfo(Userinfo userInfo);

	/**
	 * 添加用户信息
	 * 
	 * @author 徐培珊
	 * @date 2018年11月2日下午2:25:45
	 * @param userInfo
	 * @return
	 */
	ManagerResult insertUserInfo(Userinfo userInfo);

	/**
	 * 根据用户id删除用户信息
	 * 
	 * @author 徐培珊
	 * @date 2018年11月2日下午2:27:30
	 * @param userId
	 * @return
	 */
	ManagerResult deleteUserInfo(Integer userId);

	/**
	 * 根据user_type加载菜单
	 * 
	 * @author 高俊
	 * @date 2018年11月2日下午9:59:49
	 * @param user_type
	 * @return
	 */
	List<Menus> findMenuByUserType(String user_type);

	/**
	 * 查找所有公司
	 * 
	 * @author 徐培珊
	 * @date 2018年11月3日下午8:00:43
	 * @return
	 */
	EasyUIDataGridResult findAllCompanyToList(Company company, Integer page, Integer rows);

	/**
	 * 查询所有的userinfo一部分信息
	 * 
	 * @param page
	 * @param rows
	 * @return
	 */
	EasyUIDataGridResult findSimpleUser(SimpleUser user, Integer page, Integer rows);

	/**
	 * 重置密码
	 * 
	 * @param userId
	 * @param userPwd
	 * @return
	 */
	ManagerResult resetPwd(Integer userId, String userPwd);

	/**
	 * 
	 * 查询用户名
	 * 
	 * @param q
	 * @return
	 */
	public List<Userinfo> findUserName(String q);

	/**
	 * @Title: findUserRole
	 * @Description: 查询所有角色，并设置选中的用户角色为true 
	 * 				-- 1.获取用户对应的角色,例如user_id=221 select uuid
	 *               from user_role,role where user_role.userid=221 and
	 *               user_role.roleuuid = role.uuid and role.enble=1; 
	 *               -- 2.获取所有角色
	 *               select uuid,name from role where role.enble=1; --
	 *               3.封装返回值将用户对应的角色设置为true,uuid、name
	 * @param user_id
	 * @return List<Tree>
	 * @author gj
	 * @date 2019年2月16日下午2:56:10
	 */
	List<EasyUIOptionalTreeNode> findUserRole(Integer user_id);
	/**
	 * 
	* @Title: findUserRoleByUserid 
	* @Description: 根据用户id查询对应的角色  
	* @param user_id
	* @return List<Role>
	* @author gj
	* @date 2019年2月21日下午12:08:37
	 */
	List<Role> findUserRoleByUserid(Integer user_id);
	/**
	 * 
	 * @Title: updateUserRole
	 * @Description: 更新用户对应的角色
	 * @param user_id
	 * @param checkedIds
	 * @return Integer
	 * @author gj
	 * @date 2019年2月16日下午3:57:33
	 */
	ManagerResult updateUserRole(Integer user_id, String checkedIds);
	/**
	 * 
	* @Title: findUserCollegeAndTeambyUserid 
	* @Description: 根据userid查询该user的院系以及所属团队  
	* @param user_id
	* @return UserCollegeAndTeam
	* @author gj
	* @date 2019年2月20日下午8:50:01
	 */
	UserCollegeAndTeam findUserCollegeAndTeambyUserid(Integer user_id);

}
