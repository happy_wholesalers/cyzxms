/**
 * @author 徐培珊
 * @date 2018年12月11日下午3:40:16
 * @version 1.0
 * @company 河南喜乐融商贸有限公司
 * @Copyright: 2018 www.xlr.com Inc. All rights reserved. 
 */
package com.xlr.service.system;

import java.util.List;

import com.xlr.pojo.TeamCertificate;

/**
 * @author 徐培珊
 * @date 2018年12月11日下午3:40:16
 */
public interface TeamCertificateService {

	/**
	 * 	查找所有证书
	 * @author 徐培珊
	 * @date 2018年12月11日下午7:46:37
	 * @return
	 */
	List<TeamCertificate> findAllCertificate();

}
