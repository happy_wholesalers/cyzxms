/**
 * @date 2018年11月27日下午2:54:27
 * @version 1.0
 * @company 河南喜乐融商贸有限公司
 * @Copyright: 2018 www.xlr.com Inc. All rights reserved. 
 */
package com.xlr.service.system;

import java.util.List;

import com.xlr.pojo.Company;
import com.xlr.utils.ManagerResult;

/**
 * @author 李贺鹏
 * @date 2018年11月27日下午2:54:27
 */
public interface TotallicenceService {
	List<Company> totallicenceList();
}
