package com.xlr.service.department;

import java.util.List;

import com.xlr.pojo.Company;
import com.xlr.pojo.Team;
import com.xlr.utils.EasyUIDataGridResult;
import com.xlr.utils.ManagerResult;

public interface TeamService {

	/**
	 * @author 王宏志 查询全部的孵化团队 并实现分页
	 * @date 2018年12月11日下午4:33:10
	 * @param page
	 * @param rows
	 * @return
	 */
	public EasyUIDataGridResult findAllTeamByPage(Team team, Integer page,
			Integer rows);

	/**
	 * @author 王宏志 修改团队信息
	 * @date 2018年12月11日下午4:34:49
	 * @param team
	 * @return
	 */
	public ManagerResult updateTeam(Team team);

	/**
	 * @author 王宏志 删除团队信息
	 * @date 2018年12月11日下午4:35:03
	 * @param t_id
	 * @return
	 */
	public ManagerResult deleteTeamById(Integer t_id);

	/**
	 * @author 王宏志 添加团队信息
	 * @date 2018年12月11日下午4:35:20
	 * @param team
	 * @return
	 */
	public ManagerResult addTeam(Team team);
	/**
	 * @author 李贺鹏
	 * @date 2019年2月16日下午8:58:24
	 * @param company_leader
	 * @return List<Team>
	 */
	public List<Team> findDeptTeamLeaderName(String t_leader);

}
