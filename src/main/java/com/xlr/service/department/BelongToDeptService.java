/**
 * @author 徐培珊
 * @date 2018年12月4日下午6:23:29
 * @version 1.0
 * @company 河南喜乐融商贸有限公司
 * @Copyright: 2018 www.xlr.com Inc. All rights reserved. 
 */
package com.xlr.service.department;

import java.util.List;

import com.xlr.pojo.Company;
import com.xlr.utils.EasyUIDataGridResult;
import com.xlr.utils.ManagerResult;

/**
 * 	根据session中的院系id操作
 * @author 徐培珊
 * @date 2018年12月4日下午6:23:29
 */
public interface BelongToDeptService {

	/**
	 * 	院系公司管理_根据session中的院系id，加载公司列表_分页
	 * @author 徐培珊
	 * @date 2018年12月4日下午6:28:56
	 * @param deptId
	 * @param rows 
	 * @param page 
	 * @return
	 */

	EasyUIDataGridResult findCompanyBypage(Company company, Integer page,
			Integer rows);

	/**
	 * 	院系公司管理_更新公司信息
	 * @author 徐培珊
	 * @date 2018年12月4日下午7:24:58
	 * @param company
	 * @return
	 */
	
//	ManagerResult updateCompanyInfo(Company company);
	/**
	 * @author 李贺鹏
	 * @date 2019年2月16日下午8:58:24
	 * @param company_leader
	 * @return List<Company>
	 */
	public List<Company> findDepCompanyLeaderName(String company_leader);
	
}
