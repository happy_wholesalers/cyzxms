/**
 * @author 高俊
 * @date 2018年11月8日下午8:53:12
 * @version 1.0
 * @company 河南喜乐融商贸有限公司
 * @Copyright: 2018 www.xlr.com Inc. All rights reserved. 
 */
package com.xlr.service.company;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xlr.pojo.Company;
import com.xlr.pojo.Overview;
import com.xlr.utils.EasyUIDataGridResult;
import com.xlr.utils.ManagerResult;

/**
 * @author 高俊
 * @date 2018年11月8日下午8:53:12
 */
public interface CompanyService {
	/**
	 * 	查询公司全部信息_分页
	 * @author 徐培珊
	 * @date 2018年11月28日下午3:21:37
	 * @param page
	 * @param rows
	 * @return
	 */
	public EasyUIDataGridResult findAllCnameByPage(Integer page, Integer rows);
	
	
	public List<Company> findAllCompany();
	
	/**
	 * 根据公司id，更新公司营业执照
	 * @author 高俊
	 * @date 2018年11月14日下午8:27:22
	 * @param company
	 */
	public ManagerResult updateCompanyImage(Company company);

	/**
	 * 根据公司id查询公司营业执照
	 * @author 高俊
	 * @date 2018年11月14日下午8:45:41
	 * @param user_companyid
	 * @return
	 */
	public ManagerResult findCompanyImageId(Integer user_companyid);

	/**
	 * 	入驻申请统计_更新公司信息
	 * @author 徐培珊
	 * @date 2018年11月27日下午2:11:16
	 * @param company
	 * @return
	 */
	public ManagerResult updateCompanyInfo(Company company);

	/**
	 * 	入驻申请统计_根据公司id删除公司所有信息
	 * @author 徐培珊
	 * @date 2018年11月27日下午4:31:30
	 * @param company_id
	 * @return
	 */
	public ManagerResult deleteCompanyInfoById(Integer company_id);

	/**
	 * 	入驻申请统计_添加公司信息
	 * @author 徐培珊
	 * @date 2018年11月27日下午5:30:48
	 * @param company
	 * @return
	 */
	public ManagerResult addCompanyInfo(Company company);


	/**
	 * 	根据session中的用户名查询公司信息
	 * @author 徐培珊
	 * @date 2018年12月5日下午8:52:32
	 * @param user_code
	 * @param page
	 * @param rows
	 * @return
	 */
	public EasyUIDataGridResult findAllCurrentCompanyByPage(@Param("user_code")String user_code, Integer page, Integer rows);


	/**
	 * 	根据孵化类型查询公司
	 * @author 徐培珊
	 * @date 2018年12月6日上午11:47:55
	 * @param hatchType
	 * @param page
	 * @param rows
	 * @return
	 */
	public EasyUIDataGridResult findAllCompanyByPage(Company company, Integer page, Integer rows);
	/**
	 * 根据公司id查询其项目概况
	 * @author 李贺鹏
	 * @date 2019年2月23日上午9:15:36
	 * @param companyid
	 * @return
	 */
	public EasyUIDataGridResult findOverviewByCompanyid(Integer companyid);


	/**
	 * @author 李贺鹏
	 * @date 2019年2月23日上午11:05:37
	 * @param overview
	 * @return
	 */
	ManagerResult update(Overview overview);


}
