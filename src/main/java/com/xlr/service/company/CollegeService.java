package com.xlr.service.company;
import java.util.List;

import com.xlr.pojo.College;
/**
 * @author 王宏志
 * 	院系的service
 * @date 2018年12月5日下午8:52:58
 * @TOOD 
 */
public interface CollegeService {

	/**
	 * @author 王宏志
	 * 	获取全部的院系信息
	 * @date 2018年12月5日下午8:54:09
	 * @return
	 */
	public List<College> get();
}
