package com.xlr.service.company;
import org.apache.ibatis.annotations.Param;

import com.xlr.pojo.CompanyUsedAssets;
import com.xlr.utils.EasyUIDataGridResult;
import com.xlr.utils.ManagerResult;



/**
 * @author 王宏志
 * @date 2018年12月4日下午6:01:20
 * @TOOD 
 */
public interface CompanyUsedAssetsService {
	//添加公司资产
	public ManagerResult addCompanyAssets(CompanyUsedAssets companyUsedAssets);
	//删除公司资产通过ID
	public ManagerResult deleteCompanyAssetsByID(Integer itemId);
	//修改公司资产
	public ManagerResult updateCompanyAssets(CompanyUsedAssets companyUsedAssets);
	//通过资产ID查询资产信息
	public CompanyUsedAssets getCompanyAssets(Integer itemId);
	//查询公司ID查询该公司全部资产信息
	public EasyUIDataGridResult selectAllCompanyAssertsByCompanyId(@Param("companyId") Integer companyId, Integer page, Integer rows);
}
