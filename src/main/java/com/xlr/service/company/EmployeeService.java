package com.xlr.service.company;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xlr.pojo.Charger;
import com.xlr.pojo.CompanyUsedAssets;
import com.xlr.pojo.Employee;
import com.xlr.utils.EasyUIDataGridResult;
import com.xlr.utils.ManagerResult;

/**
 * @author 王宏志 入驻项目负责人的service层
 * @date 2018年12月2日上午10:51:29
 * @TOOD
 */
public interface EmployeeService {

	
	/**
	 * @author 王宏志
	 * 	添加员工
	 * @date 2018年12月4日下午6:03:56
	 * @param employee
	 * @return
	 */
	public ManagerResult add(Employee employee);

	
	/**
	 * @author 王宏志
	 * 	删除员工信息
	 * @date 2018年12月4日下午6:04:15
	 * @param employeeId
	 * @return
	 */
	public ManagerResult delete(Integer employeeId);

	
	/**
	 * @author 王宏志
	 * 	修改员工信息
	 * @date 2018年12月4日下午6:04:32
	 * @param employee
	 * @return
	 */
	public ManagerResult update(Employee employee);

	
	/**
	 * @author 王宏志
	 * 	通过公司ID获取员工信息
	 * @date 2018年12月4日下午6:04:51
	 * @param employeeId
	 * @param page
	 * @param rows
	 * @return
	 */
	public EasyUIDataGridResult get(@Param("e_cid") Integer e_cid, Integer page,
			Integer rows);

}
