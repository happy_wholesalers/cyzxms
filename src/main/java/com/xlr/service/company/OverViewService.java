package com.xlr.service.company;

import com.xlr.pojo.OverViewAll;
import com.xlr.pojo.Overview;
import com.xlr.utils.EasyUIDataGridResult;
import com.xlr.utils.ManagerResult;

/**
 * 项目概况
 * @author Zoe
 * @date 2019年3月1日上午10:56:39
 */
public interface OverViewService {
	/**
	 * 根据公司id查找所有表格数据
	 * @author Zoe
	 * @date 2019年2月28日下午8:11:44
	 * @param companyId 公司id
	 * @return
	 */
	public OverViewAll findAllTable(Integer companyId);
	/**
	 * 添加项目概况
	 * @author Zoe
	 * @date 2019年3月1日上午10:38:11
	 * @param overview 项目概况对象
	 * @return
	 */
	public ManagerResult insert(Overview overview, String[] o_type);
	/**
	 * 分页查找所有项目概况
	 * @author Zoe
	 * @param rows 每页记录数
	 * @param page 页数
	 * @date 2019年3月1日下午2:37:37
	 * @return
	 */
	public EasyUIDataGridResult listByPage(Integer page, Integer rows);
	/**
	 * 更新项目概况
	 * @author Zoe
	 * @date 2019年3月1日下午2:49:23
	 * @param overview 项目概况对象
	 * @param o_type 项目概况所属类型
	 * @return
	 */
	public ManagerResult update(Overview overview, String[] o_type);

}
