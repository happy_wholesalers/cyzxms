package com.xlr.service.company;

/**
 * @author 徐培珊
 * @date 2018年12月1日下午8:44:36
 * @version 1.0
 * @company 河南喜乐融商贸有限公司
 * @Copyright: 2018 www.xlr.com Inc. All rights reserved. 
 */
import com.xlr.pojo.Protocol;
import com.xlr.utils.EasyUIDataGridResult;
import com.xlr.utils.ManagerResult;

/**
 * @author 徐培珊
 * @date 2018年12月1日下午8:44:36
 */
public interface ProtocolService {

	/**
	 * @author 徐培珊
	 * @date 2018年12月1日下午8:40:58
	 * @param companyId
	 * @return
	 */
	ManagerResult selectProtocolByCompanyId(Integer companyId);

	/**
	 * @author 徐培珊
	 * @date 2018年12月1日下午8:36:48
	 * @param protocol
	 * @return
	 */
	ManagerResult updateProtocolByCompanyId(Protocol protocol);

	/**
	 * @author 徐培珊
	 * @date 2018年12月2日上午11:22:41
	 * @param page
	 * @param rows
	 * @return
	 */
	EasyUIDataGridResult selectAllProtocol(Integer page, Integer rows);

}