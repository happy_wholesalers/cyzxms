package com.xlr.service.company;

import com.xlr.pojo.Tutor;
import com.xlr.utils.EasyUIDataGridResult;
import com.xlr.utils.ManagerResult;

/**
 * @author 范英豪 导师类
 */
public interface TutorService {
	/*
	 * 根据id查询导师 *
	 */
	public EasyUIDataGridResult findByID(Integer t_companyid);

	/**
	 * 根据id修改导师
	 * 
	 * */
	public ManagerResult update(Tutor tutor);
}
