package com.xlr.service.company;
import com.xlr.pojo.Charger;
import com.xlr.utils.EasyUIDataGridResult;
import com.xlr.utils.ManagerResult;
/**
 * @author 王宏志
 * 	入驻项目负责人的service层
 * @date 2018年12月2日上午10:51:29
 * @TOOD 
 */
public interface ChargerService {

	/**
	 * @author 王宏志
	 * 	展示入驻项目负责人的全部信息
	 * @date 2018年12月2日上午10:58:03
	 * @return
	 */
	public EasyUIDataGridResult showChargerByUserId(Integer userId,String usercode);
	/**
	 * @author 王宏志
	 * 	修改入驻项目负责人信息
	 * @date 2018年12月2日上午11:14:03
	 * @param charger
	 * @return
	 */
	public ManagerResult updateCharger(Charger charger);

	
}
