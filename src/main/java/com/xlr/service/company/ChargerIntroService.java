package com.xlr.service.company;

import org.apache.ibatis.annotations.Param;

import com.xlr.pojo.ChargerIntro;




/**
 * @author 王宏志
 * 入驻项目负责人简历Service层
 * @date 2018年12月4日下午7:39:03
 * @TOOD 
 */
public interface ChargerIntroService {
	
	/**
	 * @author 王宏志
	 * 	通过公司ID查询入驻项目负责人简历
	 * @date 2018年12月4日下午7:42:14
	 * @param cid
	 * @return
	 */
	public ChargerIntro selectChargerIntro(Integer cid);
	
	/**
	 * @author 王宏志
	 * 	更新入驻项目负责人简历
	 * @date 2018年12月4日下午7:42:43
	 * @param chargerIntro
	 * @return
	 */
	
}
