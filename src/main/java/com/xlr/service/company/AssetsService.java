package com.xlr.service.company;

import java.util.List;

import com.xlr.pojo.AssetsNameAndSeq;

/**
 * @author 王宏志
 * 	公司资产的service
 * @date 2018年12月6日下午2:55:12
 * @TOOD 
 */
public interface AssetsService {

	/**
	 * @author 王宏志
	 * 	获取全部资产的名称与编号
	 * @date 2018年12月6日下午2:56:14
	 * @return
	 */
	public List<AssetsNameAndSeq> getAssetsNameAndSeq();
	/**
	 * @author 王宏志
	 * 	通过资产编号获取资产名称
	 * @date 2018年12月6日下午2:57:07
	 * @param seq
	 * @return
	 */
	public String getAssertNameBySeq(String seq);
	
}
