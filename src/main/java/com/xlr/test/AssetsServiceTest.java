package com.xlr.test;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.xlr.mapper.company.CompanyUsedAssetsMapper;
import com.xlr.pojo.CompanyUsedAssets;
import com.xlr.serviceImpl.company.CompanyUsedAssetsServiceImpl;
import com.xlr.utils.ManagerResult;

public class AssetsServiceTest {
	private CompanyUsedAssetsServiceImpl companyUserServiceImpl;
	private ApplicationContext application;

	@Before
	public void setUp() {
		application = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
		companyUserServiceImpl = application.getBean(CompanyUsedAssetsServiceImpl.class);
	}
	@Test
	public void test() {
		CompanyUsedAssets companyUsedAssets=new CompanyUsedAssets();
		companyUsedAssets.setCompanyId(1);
		companyUsedAssets.setItemName("~~~~~~~~~~~");
		ManagerResult addCompanyAssets = companyUserServiceImpl.addCompanyAssets(companyUsedAssets);
		System.out.println(addCompanyAssets);
	}

}
