package com.xlr.test;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.xlr.mapper.company.OverviewMapper;
import com.xlr.pojo.OverViewAll;

/**
 * @author Zoe
 * @date 2019年2月28日下午5:51:47
 */
public class OverViewMapperTest {
	private OverviewMapper overviewMapper;
	private ApplicationContext application;

	@Before
	public void setUp() {
		application = new ClassPathXmlApplicationContext("classpath*:applicationContext*.xml");
		overviewMapper = application.getBean(OverviewMapper.class);
	}

	@Test
	public void test1() {
		OverViewAll overViewAll = overviewMapper.findAllTable(1);
		System.out.println(overViewAll);
	}
	
	@Test
	public void testInsert() {
		overviewMapper.insertViewDict(1,34);
	}
}
