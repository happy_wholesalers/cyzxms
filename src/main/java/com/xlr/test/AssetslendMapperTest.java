package com.xlr.test;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.xlr.mapper.assets.AssetlendMapper;
import com.xlr.pojo.CompanyUsedAssets;
import com.xlr.service.assets.AssetlendService;

public class AssetslendMapperTest {
	private AssetlendMapper assetlendMapper;
	private AssetlendService assetlendService;
	private ApplicationContext application;

	@Before
	public void setUp() {
		application = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
		assetlendMapper = application.getBean(AssetlendMapper.class);
		assetlendService = application.getBean(AssetlendService.class);
	}

	@Test
	public void test1() {
		List<CompanyUsedAssets> list = assetlendMapper.selectAllCompanyUsedAssets();
		for (CompanyUsedAssets companyUsedAssets : list) {
			System.out.println(companyUsedAssets);
		}
//		EasyUIDataGridResult result = assetlendService.findAllCompanyUsedAssets(1, 10);
//		System.out.println(result);
	}

}
