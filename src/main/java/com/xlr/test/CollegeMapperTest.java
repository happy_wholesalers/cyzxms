package com.xlr.test;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.xlr.mapper.company.CollegeMapper;
import com.xlr.pojo.College;

public class CollegeMapperTest {

	private CollegeMapper collegeMapper;
	private ApplicationContext application;

	@Before
	public void setUp() {
		application = new ClassPathXmlApplicationContext("classpath*:applicationContext*.xml");
		collegeMapper = application.getBean(CollegeMapper.class);
	}

	@Test
	public void test() {
		List<College> colleges = collegeMapper.selectAllCollege();
		System.out.println(colleges.size());
	}

	@Test
	public void test1() {
		College college = collegeMapper.findByid(1);
		System.out.println(college.getCollege_name());
	}
	
}
