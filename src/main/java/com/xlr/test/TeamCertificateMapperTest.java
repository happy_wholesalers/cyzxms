/**
 * @author 徐培珊
 * @date 2018年12月11日下午7:51:38
 * @version 1.0
 * @company 河南喜乐融商贸有限公司
 * @Copyright: 2018 www.xlr.com Inc. All rights reserved. 
 */
package com.xlr.test;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.xlr.mapper.company.CompanyMapper;
import com.xlr.mapper.system.TeamCertificateMapper;
import com.xlr.pojo.TeamCertificate;

/**
 * @author 徐培珊
 * @date 2018年12月11日下午7:51:38
 */
public class TeamCertificateMapperTest {
	private TeamCertificateMapper companyCertificateMapper;
	private ApplicationContext application;

	@Before
	public void setUp() {
		application = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
		companyCertificateMapper = application.getBean(TeamCertificateMapper.class);
	}
	
	/**
	 * 	查询所有公司证书
	 * @author 徐培珊
	 * @date 2018年12月11日下午7:52:42
	 */
	@Test
	public void test() {
		List<TeamCertificate> list = companyCertificateMapper.findAllCertificate();
		System.out.println(list);
	}
}
