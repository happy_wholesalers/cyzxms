package com.xlr.test;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.xlr.mapper.company.ChargerMapper;
import com.xlr.pojo.Charger;

public class ChargerMapperTest {

	private ChargerMapper chargerMapper;
	private ApplicationContext application;

	@Before
	public void setUp() {
		application = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
		chargerMapper = application.getBean(ChargerMapper.class);
	}

	@Test
	public void test() {
		Charger charger = chargerMapper.selectUserinfo(22);
		System.out.println(charger);
	}
	@Test
	public void updata() {
		Charger charger = new Charger();
		charger.setUser_id(22);
		charger.setUser_code("石腾飞");
		charger.setUser_education("大专");
		charger.setUser_rewardsPunishment("严重处分");
		Integer i = chargerMapper.updateUserinfo(charger);
		System.out.println(i);
	}

	
}
