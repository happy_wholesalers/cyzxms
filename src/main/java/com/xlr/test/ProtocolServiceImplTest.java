/**
 * @author 徐培珊
 * @date 2018年12月2日上午11:29:11
 * @version 1.0
 * @company 河南喜乐融商贸有限公司
 * @Copyright: 2018 www.xlr.com Inc. All rights reserved. 
 */
package com.xlr.test;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.xlr.mapper.company.ProtocolMapper;
import com.xlr.serviceImpl.company.ProtocolServiceImpl;
import com.xlr.utils.EasyUIDataGridResult;

/**
 * @author 徐培珊
 * @date 2018年12月2日上午11:29:11
 */
public class ProtocolServiceImplTest {
	private ProtocolServiceImpl protocolServiceImpl;
	private ApplicationContext application;
	@Before
	public void setUp() {
		application = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
		protocolServiceImpl = application.getBean(ProtocolServiceImpl.class);
	}
	
	@Test
	public void selectAll() {
		EasyUIDataGridResult result = protocolServiceImpl.selectAllProtocol(1, 10);
		System.out.println(result.getTotal());
	}
}
