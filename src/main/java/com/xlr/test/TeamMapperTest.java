package com.xlr.test;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.xlr.mapper.company.CompanyMapper;
import com.xlr.mapper.department.TeamMapper;
import com.xlr.pojo.Company;
import com.xlr.pojo.Team;

public class TeamMapperTest {

	private TeamMapper teamMapper;
	private ApplicationContext application;

	@Before
	public void setUp() {
		application = new ClassPathXmlApplicationContext(
				"classpath:applicationContext.xml");
		teamMapper = application.getBean(TeamMapper.class);
	}

	@Test
	public void test() {
		Integer del = teamMapper.deleteTeamById(2);
		System.out.println(del);
	}

	@Test
	public void test1() {
		Team team = new Team();
		team.setT_name("ACM");
		team.setT_number(13);
		team.setT_leader("李永明");
		team.setT_field("算法公关");
		Integer del = teamMapper.insertTeam(team);
		System.out.println(del);
	}

	@Test
	public void test2() {
		Team team = new Team();
		team.setT_name("ACM++");
		team.setT_id(3);
		Integer del = teamMapper.updateTeam(team);
		System.out.println(del);
	}
}
