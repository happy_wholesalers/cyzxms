package com.xlr.test;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.xlr.mapper.company.CompanyMapper;
import com.xlr.mapper.company.EmployeeMapper;
import com.xlr.pojo.Company;
import com.xlr.pojo.Employee;

public class EmployeeMapperTest {

	private EmployeeMapper employeeMapper;
	private ApplicationContext application;

	@Before
	public void setUp() {
		application = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
		employeeMapper = application.getBean(EmployeeMapper.class);
	}

	@Test
	public void test() {
		Employee employee=new Employee();
		employee.setE_id(0);
		employee.setE_name("whz");
		Integer i = employeeMapper.insertEmployee(employee);
		System.out.println(i);
	}
	@Test
	public void test1() {
		Employee employee=new Employee();
		employee.setE_id(0);
		employee.setE_name("whz");
		Integer i = employeeMapper.deleteEmplyeeByID(13);
		System.out.println(i);
	}
	@Test
	public void test2() {
		Employee employee=new Employee();
		employee.setE_id(12);
		employee.setE_name("whz");
		Integer i = employeeMapper.updateEmplyee(employee);
		System.out.println(i);
	}
	@Test
	public void test3() {
		List<Employee> employee = employeeMapper.selectEmployeeByCompanyID(1);
		System.out.println(employee.size());
	}
	
	

}
