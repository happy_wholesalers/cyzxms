/**
 * @author 徐培珊
 * @date 2018年12月4日下午6:42:05
 * @version 1.0
 * @company 河南喜乐融商贸有限公司
 * @Copyright: 2018 www.xlr.com Inc. All rights reserved. 
 */
package com.xlr.test;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.xlr.mapper.department.BelongToMapper;
import com.xlr.pojo.Company;
import com.xlr.serviceImpl.department.BelongToDeptServiceImpl;
import com.xlr.utils.EasyUIDataGridResult;

/**
 * @author 徐培珊
 * @date 2018年12月4日下午6:42:05
 */
public class BelongToDeptServiceImplTest {
	private BelongToDeptServiceImpl belongToDeptServiceImpl;
	private ApplicationContext application;

	@Before
	public void setUp() {
		application = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
		belongToDeptServiceImpl = application.getBean(BelongToDeptServiceImpl.class);
	}
	
}
