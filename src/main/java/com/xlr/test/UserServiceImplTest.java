/**
 * @author 徐培珊
 * @date 2018年11月1日下午9:33:31
 * @version 1.0
 * @company 河南喜乐融商贸有限公司
 * @Copyright: 2018 www.xlr.com Inc. All rights reserved. 
 */
package com.xlr.test;


import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.alibaba.fastjson.JSON;
import com.xlr.pojo.Userinfo;
import com.xlr.service.system.UserService;
import com.xlr.utils.EasyUIDataGridResult;
import com.xlr.utils.ManagerResult;



/**
 * @author 徐培珊
 * @date 2018年11月1日下午9:33:31
 */
public class UserServiceImplTest {
	private ApplicationContext application;

	@Test
	public void insert() {
		application=new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
		Userinfo info = new Userinfo();
		info.setUser_code("122222");
		info.setUser_pwd("12222");
		info.setUser_type("超级管理员");
		
		UserService userService = (UserService) application.getBean("userService");
		ManagerResult result = userService.insertUserInfo(info);
		System.out.println(JSON.toJSONString(result));//{"msg":"用户添加成功","status":200}
	}
	
	@Test
	public void update() {
		application=new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
		Userinfo info = new Userinfo();
		info.setUser_id(6);
		info.setUser_code("122222");
		info.setUser_pwd("12222");
		info.setUser_type("超级管理员");
		
		UserService userService = (UserService) application.getBean("userService");
		ManagerResult result = userService.updateUserInfo(info);
		System.out.println(JSON.toJSONString(result));//
	}
	
	@Test
	public void delete() {
		application=new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
		
		UserService userService = (UserService) application.getBean("userService");
		ManagerResult result = userService.deleteUserInfo(6);
		System.out.println(JSON.toJSONString(result));//{"msg":"用户删除成功","status":200}
	}
	
	@Test
	public void findAllUserInfo() {
		application=new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
		UserService userService = (UserService) application.getBean("userService");
		
		Userinfo userInfo = null;
		//EasyUIDataGridResult findUserinfoList = userService.findUserinfoList(userInfo, 1, 3);
		//System.out.println(findUserinfoList.getTotal());
	}
	
//	@Test
//	public void testFindAllUserPermission() {
//		application=new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
//		UserService userService = (UserService) application.getBean("userService");
//		
//		ManagerResult findUserinfoList = userService.findAllUserPermission();
//		String jsonString = JSON.toJSONString(findUserinfoList);
//		System.out.println(jsonString);
//	}
}
