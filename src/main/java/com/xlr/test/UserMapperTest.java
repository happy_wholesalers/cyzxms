package com.xlr.test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.alibaba.fastjson.JSON;
import com.xlr.mapper.system.UserMapper;
import com.xlr.pojo.Menus;
import com.xlr.pojo.Userinfo;
import com.xlr.pojo.Userpermission;

public class UserMapperTest {

	private UserMapper userMapper;
	private ApplicationContext application;

	@Before
	public void setUp() {
		application = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
		userMapper = application.getBean(UserMapper.class);
	}

	@Test
	public void test() {
		Userinfo userinfo = userMapper.findUserInfoByUsernameAndPassword("123456", "123456");
		System.out.println(userinfo);
	}

	@Test
	public void test3() {
		Integer i = userMapper.deleteUserinfoByUserID(2);
		System.out.println(i);
	}

	@Test
	public void test4() {
		Userinfo userinfo = new Userinfo();
		userinfo.setUser_id(14);
		userinfo.setUser_code("王宏志");
		userinfo.setUser_pwd("stf19950201");
		Integer i = userMapper.updateUserinfo(userinfo);
		System.out.println(i);
	}

	@Test
	public void test5() {
		Userinfo userinfo = new Userinfo();
		userinfo.setUser_code("SB");
		userinfo.setUser_pwd("SB");
		userinfo.setUser_type("超级管理员");
		Integer i = userMapper.insertUserinfo(userinfo);
		System.out.println(i);
	}

	@Test
	public void test6() {
		List<String> userpermissions = userMapper.findMenuidByTypeId("超级管理员");
		System.out.println(userpermissions.size());
	}

	@Test
	public void test7() {
		List<Menus> permissions = userMapper.findMenusByPId(0);
		System.out.println(permissions.size());
	}

	// 根据user_type加载菜单
	@Test
	public void test8() {
		List<Menus> list = new LinkedList<Menus>();
		List<String> menuids = userMapper.findMenuidByTypeId("超级管理员");
		List<Menus> permissionList = userMapper.findMenusByPId(0);
		for (Menus permission : permissionList) {
			boolean flag = false;
			for (String menuid : menuids) {
				if (menuid.equals(permission.getMenuid())) {
					flag = true;
				}
			}
			if (flag) {
				// System.out.println(permission);
				List<Menus> permissionList1 = userMapper.findMenusByPId(Integer.parseInt(permission.getMenuid()));
				for (Menus permission1 : permissionList1) {
					// System.out.println(permission1);
					permission.getMenus().add(permission1);
				}
			}
			list.add(permission);
		}
		String jsonString = JSON.toJSONString(list);
		System.out.println(jsonString);
		/*
		 * for (Permission permission : list) { List<Permission> permission2 =
		 * permission.getPermission(); System.out.println(permission2+"");
		 * if(permission2!=null) { for (Permission permission3 : permission2) {
		 * System.out.println(permission3); } } }
		 */
	}

	@Test
	public void test9() {
		List<Menus> list = new LinkedList<Menus>();
		List<Menus> menusList = userMapper.findMenusByUserType("超级管理员");
		for (Menus menus : menusList) {
			List<Menus> menusList1 = userMapper.findMenusByPId(Integer.parseInt(menus.getMenuid()));
			for (Menus menus1 : menusList1) {
				menus.getMenus().add(menus1);
			}
			list.add(menus);
		}
		String jsonString = JSON.toJSONString(list);
		System.out.println(jsonString);
	}

	@Test
	public void test11() {
		Userinfo userinfo = new Userinfo();
		userinfo.setUser_code("whz11111");
		userinfo.setUser_pwd("111111111111");
		userinfo.setUser_type("超级管理员");
		userinfo.setUser_age(11);
		userinfo.setUser_nation("汉族");
		userinfo.setUser_email("155@qq.com");
		Integer i = userMapper.insertUserinfo(userinfo);
		System.out.println(i);
	}

/*	@Test
	public void test12() {
		Userinfo userInfo = new Userinfo();
		List<Userinfo> findAllUserinfo = userMapper.findAllUserinfo(userInfo.getUser_code(), userInfo.getUser_type(),
				userInfo.getUser_companyid(), userInfo.getUser_politics());
		for (Userinfo userinfo1 : findAllUserinfo) {
			System.out.println(userinfo1);
		}
	}*/

	@Test
	public void test13() {
		Integer authorization = userMapper.authorization("院系联系人", "600");
		System.out.println(authorization);
	}

	@Test
	public void test14() {
		List<Userpermission> selectAuthorization = userMapper.selectAuthorization("超级管理员");
		for (Userpermission userpermission : selectAuthorization) {
			System.out.println(userpermission);
		}
	}
	@Test
	public void test15() {
		Integer deleteAuthorization = userMapper.deleteAuthorization("院系联系人", "200");
		System.out.println(deleteAuthorization);
	}
	
}
