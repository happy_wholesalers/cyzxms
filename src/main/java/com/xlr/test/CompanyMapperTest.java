package com.xlr.test;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.xlr.mapper.company.CompanyMapper;
import com.xlr.pojo.Company;

public class CompanyMapperTest {

	private CompanyMapper companyMapper;
	private ApplicationContext application;

	@Before
	public void setUp() {
		application = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
		companyMapper = application.getBean(CompanyMapper.class);
	}

	@Test
	public void test() {
		
		Company company=new Company();
		company.setCompany_number("333333");
		company.setCompany_name("阿里baba");
		company.setCompany_product("ssss");
		companyMapper.insertCompany(company);
	}
	@Test
	public void test1() {
		Company company=new Company();
		company.setCompany_id(3);
		company.setCompany_number("888888888");
		company.setCompany_name("四十抢到");
		company.setCompany_area(22211);
		companyMapper.updateCompany(company);
	}
	@Test
	public void test2() {
		List<Company> name = companyMapper.selectAllCompany();
		System.out.println(name);
	}
	/**
	 * 	根据session中的用户名查询公司信息
	 * @author 徐培珊
	 * @date 2018年12月5日下午8:46:25
	 */
	@Test
	public void test3() {
		List<Company> name = companyMapper.selectAllCompanyByUserCode("石腾飞");
		System.out.println(name.size());
	}
	@Test
	public void test4() {
		Company company= companyMapper.selectCompanyById(1);
		System.out.println(company.getCompany_name());
	}

}
