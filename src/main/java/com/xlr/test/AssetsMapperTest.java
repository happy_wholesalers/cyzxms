package com.xlr.test;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.xlr.mapper.assets.AssetlendMapper;
import com.xlr.mapper.company.AssetsMapper;
import com.xlr.pojo.AssetsNameAndSeq;
import com.xlr.pojo.CompanyUsedAssets;
import com.xlr.service.assets.AssetlendService;

public class AssetsMapperTest {
	private AssetsMapper assetsMapper;
	private ApplicationContext application;

	@Before
	public void setUp() {
		application = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
		assetsMapper = application.getBean(AssetsMapper.class);
	}

	@Test
	public void test1() {
		List<AssetsNameAndSeq> list = assetsMapper.getAssetsNameAndSequence();
		System.out.println(list.size());
		for(AssetsNameAndSeq companyUsedAssets:list) {
			System.out.println(companyUsedAssets.getAssetsName());
		}
	}
	@Test
	public void test2() {
		String name = assetsMapper.getAssetsNameBySequence("111110");
		System.out.println(name);
		
	}

}
