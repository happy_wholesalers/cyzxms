package com.xlr.test;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.xlr.mapper.company.CompanyUsedAssetsMapper;
import com.xlr.pojo.CompanyUsedAssets;
import com.xlr.serviceImpl.company.CompanyUsedAssetsServiceImpl;
import com.xlr.utils.ManagerResult;

public class AssetsMapperTest2 {
	private CompanyUsedAssetsMapper companyUsedAssetsMapper;
	private ApplicationContext application;

	@Before
	public void setUp() {
		application = new ClassPathXmlApplicationContext("classpath*:applicationContext*.xml");
		companyUsedAssetsMapper = application.getBean(CompanyUsedAssetsMapper.class);
	}
	@Test
	public void test1() {
		
		CompanyUsedAssets companyUsedAssets=new CompanyUsedAssets();
		Integer i = companyUsedAssetsMapper.insertAssets(companyUsedAssets);
		System.out.println(i);
	}

}
