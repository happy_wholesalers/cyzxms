package com.xlr.test;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.xlr.mapper.assets.PublicAssetsMapper;
import com.xlr.mapper.company.CompanyUsedAssetsMapper;
import com.xlr.pojo.CompanyUsedAssets;
import com.xlr.pojo.CountsVo;
import com.xlr.pojo.PublicAssets;
import com.xlr.serviceImpl.company.CompanyUsedAssetsServiceImpl;
import com.xlr.utils.ManagerResult;

public class PlulicAssetsMapperTest {
	private PublicAssetsMapper publicAssetsMapper;
	private ApplicationContext application;

	@Before
	public void setUp() {
		application = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
		publicAssetsMapper = application.getBean(PublicAssetsMapper.class);
	}
	@Test
	public void test1() {
		List<PublicAssets> assets = publicAssetsMapper.selectAllAssets();
		System.out.println(assets);
	}
	@Test
	public void test2() {
		CountsVo countsVo = publicAssetsMapper.selectAssetsBySequence("222");
		System.out.println(countsVo);
	}
	@Test
	public void test3() {
		PublicAssets publicAssets=new PublicAssets();
		publicAssets.setAssetsName("桌子");
		publicAssets.setAssetsBuyDate(new Date());
		publicAssets.setAssetsMeasurement("张");
		publicAssets.setAssetsObjectName("精美家居");
		publicAssets.setAssetsPrice(22.3);
		publicAssets.setAssetsTotalNumber(23);
		publicAssets.setAssetsRemainingNumber(23);
		Integer i = publicAssetsMapper.insertAssets(publicAssets);
		System.out.println(i);
	}
	@Test
	public void test4() {
		PublicAssets publicAssets=new PublicAssets();
		publicAssets.setAssetsId(1);
		publicAssets.setAssetsName("xiaomi");
		Integer i = publicAssetsMapper.updateAssets(publicAssets);
		System.out.println(i);
	}
	@Test
	public void test5() {
		Integer i = publicAssetsMapper.deleteAssets(5);
		System.out.println(i);
	}
	@Test
	public void test6() {
		List<PublicAssets> assets = publicAssetsMapper.selectAllAssets();
		System.out.println(assets);
	}

}
