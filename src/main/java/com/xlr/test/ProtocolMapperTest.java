/**
 * @author 徐培珊
 * @date 2018年12月1日下午6:52:18
 * @version 1.0
 * @company 河南喜乐融商贸有限公司
 * @Copyright: 2018 www.xlr.com Inc. All rights reserved. 
 */
package com.xlr.test;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.xlr.mapper.company.ProtocolMapper;
import com.xlr.pojo.Protocol;

/**
 * @author 徐培珊
 * @date 2018年12月1日下午6:52:18
 */
public class ProtocolMapperTest {
	private ProtocolMapper protocolMapper;
	private ApplicationContext application;
	@Before
	public void setUp() {
		application = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
		protocolMapper = application.getBean(ProtocolMapper.class);
	}
	
	@Test
	public void insertTest() {
		Protocol protocol = new Protocol();
		protocol.setA_companyid(3);
		protocol.setA_b_companyname("小主粮仓");
		protocol.setA_b_address("平顶山学院创新创业中心一楼二号");
		protocol.setA_b_companyname("陆江华");
		protocol.setA_b_tel("12345678912");
		protocol.setA_a_companyname("创业中心");
		protocol.setA_a_address("平顶山学院创新创业中心");
		protocol.setA_a_contact("李婧珂");
		protocol.setA_a_tel("01234567890");
		protocol.setA_workstation(7);
		protocol.setA_b_training(7);
		protocol.setA_b_employ(7);
		protocol.setA_b_project(5);
		protocol.setA_a_represent("李婧珂");
		protocol.setA_b_represent("陆江华");
		
//		Protocol protocol = new Protocol();
//		protocol.setA_companyid(1);
//		protocol.setA_b_companyname("喜乐融");
//		protocol.setA_b_address("平顶山学院创新创业中心一楼一号");
//		protocol.setA_a_contact("石腾飞");
//		protocol.setA_b_tel("12345678900");
//		protocol.setA_a_companyname("创业中心");
//		protocol.setA_a_address("平顶山学院创新创业中心");
//		protocol.setA_a_contact("李婧珂");
//		protocol.setA_a_tel("01234567890");
//		protocol.setA_workstation(7);
//		protocol.setA_b_training(7);
//		protocol.setA_b_employ(7);
//		protocol.setA_b_project(5);
//		protocol.setA_a_represent("李婧珂");
//		protocol.setA_b_represent("石腾飞");
		
		
		Integer info = protocolMapper.insertProtocolInfo(protocol);
		System.out.println(info);
	}
	
	@Test
	public void updateTest() {
		Protocol protocol = new Protocol();
		protocol.setA_companyid(2);
		protocol.setA_b_companyname("小主粮仓");
		protocol.setA_b_address("平顶山学院创新创业中心一楼二号");
		protocol.setA_b_companyname("陆江华");
		protocol.setA_b_tel("12345678912");
		protocol.setA_a_companyname("创业中心");
		protocol.setA_a_address("平顶山学院创新创业中心");
		protocol.setA_a_contact("李婧珂");
		protocol.setA_a_tel("01234567890");
		protocol.setA_workstation(7);
		protocol.setA_b_training(7);
		protocol.setA_b_employ(7);
		protocol.setA_b_project(5);
		protocol.setA_a_represent("李婧珂");
		protocol.setA_b_represent("陆江华");
		Integer info = protocolMapper.updateProtocolInfo(protocol);
		System.out.println(info);
	}
	
	@Test
	public void selectTest() {
		Protocol id = protocolMapper.selectProtocolInfoByCompanyId(2);
		System.out.println(id);
	}
	
	@Test
	public void selectAllTest() {
		List<Protocol> selectAllProtocol = protocolMapper.selectAllProtocol();
		System.out.println(selectAllProtocol.size());
	}
}
