package com.xlr.test;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.xlr.mapper.company.ChargerIntroMapper;

import com.xlr.pojo.ChargerIntro;

public class ChargerIntroMapperTest {

	private ChargerIntroMapper chargerIntroMapper;
	private ApplicationContext application;

	@Before
	public void setUp() {
		application = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
		chargerIntroMapper = application.getBean(ChargerIntroMapper.class);
	}

	@Test
	public void test() {
		ChargerIntro chargerIntro = chargerIntroMapper.selectChargerIntroByCompanyId(1);
		System.out.println(chargerIntro.getS_workstation());
	}
	@Test
	public void test1() {
		ChargerIntro chargerIntro=new ChargerIntro();
		chargerIntro.setS_id(1);
		chargerIntro.setS_name("GAOJUN");
		chargerIntroMapper.updateChargerIntro(chargerIntro);
		System.out.println();
	}
	
}
