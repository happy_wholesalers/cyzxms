package com.xlr.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
/**
 * 
 * @ClassName: SessionUtil 
 * @Description: 获取session
 * @author: gj
 * @date: 2019年2月15日 上午11:57:22
 */
@Deprecated
public class SessionUtil {
	public static HttpSession getSession() {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
				.getRequest();
		return request.getSession();
	}
}
