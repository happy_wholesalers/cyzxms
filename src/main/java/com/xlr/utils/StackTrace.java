/**
 * @author 徐培珊
 * @date 2018年12月5日下午7:49:49
 * @version 1.0
 * @company 河南喜乐融商贸有限公司
 * @Copyright: 2018 www.xlr.com Inc. All rights reserved. 
 */
package com.xlr.utils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

/**
 * 	工具类_将抛出的异常堆栈信息转换为字符串
 * @author 徐培珊
 * @date 2018年12月5日下午7:49:49
 */
public class StackTrace {
	public static String getStackTrace(Throwable throwable) {
	    final Writer result = new StringWriter();
	    final PrintWriter printWriter = new PrintWriter(result);
	    throwable.printStackTrace(printWriter);
	    return result.toString();
	}
}
