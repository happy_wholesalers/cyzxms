package com.xlr.utils;

import org.apache.shiro.SecurityUtils;

import com.xlr.pojo.Userinfo;
/**
 * 
 * @ClassName: UserinfoUtils 
 * @Description: 获取当前登录的用户
 * @author: gj
 * @date: 2019年2月17日 下午9:37:28
 */
public class UserinfoUtils {
	public static Userinfo getUserinfo() {
		Userinfo userinfo = (Userinfo) SecurityUtils.getSubject().getPrincipal();
		return userinfo;
	}
}
