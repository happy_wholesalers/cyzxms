package com.xlr.mapper.baseDict;

import java.util.List;

import com.xlr.pojo.BaseDict;

/**
 * 数据字典底层mapper接口
 * @author Zoe
 * @date 2019年2月13日下午8:44:55
 */
public interface BaseDictMapper {

	/**
	 * 查询所有数据
	 * @author Zoe
	 * @date 2019年2月13日下午8:45:41
	 * @return
	 */
	List<BaseDict> findAll();

	/**
	 * 添加数据
	 * @author Zoe
	 * @date 2019年2月13日下午10:31:12
	 * @param baseDict 数据字典对象
	 */
	Integer insert(BaseDict baseDict);

	/**
	 * 根据id删除数据[修改状态]
	 * @author Zoe
	 * @date 2019年2月14日上午10:13:47
	 * @param dict_id 主键id
	 * @param dict_enble 是否可用
	 * @return
	 */
	Integer deleteById(Integer dict_id, String dict_enble);

	/**
	 * 根据id修改数据
	 * @author Zoe
	 * @date 2019年2月14日下午12:02:27
	 * @param baseDict 数据字典对象
	 * @return
	 */
	Integer updateById(BaseDict baseDict);

	/**
	 * 根据id查找数据
	 * @author Zoe
	 * @date 2019年2月14日下午1:02:05
	 * @param dict_id
	 * @return
	 */
	BaseDict findById(Integer dict_id);

	/**
	 * 查找类型名称
	 * @author Zoe
	 * @date 2019年2月14日下午3:11:40
	 * @return
	 */
	List<BaseDict> findTypeName();

	/**
	 * 
	 * @author Zoe
	 * @date 2019年2月14日下午3:32:59
	 * @param baseDict
	 * @return
	 */
	List<BaseDict> findByCriteria(BaseDict baseDict);
}
