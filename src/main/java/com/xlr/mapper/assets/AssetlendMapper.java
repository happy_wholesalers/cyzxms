package com.xlr.mapper.assets;

import java.util.List;

import com.xlr.pojo.CompanyUsedAssets;

/**
 * 资产外接
 * @author 高俊
 * @date 2018年12月3日下午7:46:42
 */
public interface AssetlendMapper {

	//查询全部的资产
	public List<CompanyUsedAssets> selectAllCompanyUsedAssets();
	
}
