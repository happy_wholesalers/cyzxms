package com.xlr.mapper.assets;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xlr.pojo.CountsVo;
import com.xlr.pojo.PublicAssets;

/**   
 * @ClassName:  PublicAssetsMapper   
 * @Description:公共资产管理Mapper 
 * @author: 王宏志
 * @date:   2018年11月27日 下午2:16:08   
 *     
 */ 
public interface PublicAssetsMapper {

	//查询全部的资产
	public List<PublicAssets> selectAllAssets();
	
	//根据资产编号查询是否已经存在该资产
	//返回值等于 0 为不存在,可以直接插入
	//返回值大于 0 为已经存在,转换为添加修改资产的数量的操作
	
	public CountsVo selectAssetsBySequence(@Param("assetsSequence")String assetsSequence);
	
	/**
	 * @author 王宏志
	 * @date 2018年11月27日下午8:01:07
	 * 插入一条资产记录
	 * @param publicAssets
	 * @return
	 */
	public Integer insertAssets(PublicAssets publicAssets);
	/**
	 * @author 王宏志
	 * @date 2018年11月27日下午8:24:44
	 * 修改资产记录
	 * @param publicAssets
	 * @return
	 */
	public Integer updateAssets(PublicAssets publicAssets);
	/**
	 * @author 王宏志
	 * @date 2018年11月28日下午3:39:24
	 * 根据资产ID删除资产记录
	 * @param assetsId
	 * @return
	 */
	public Integer deleteAssets(@Param("assetsId")Integer assetsId);
}
