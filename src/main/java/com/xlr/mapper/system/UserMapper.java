package com.xlr.mapper.system;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xlr.dto.SimpleUser;
import com.xlr.dto.UserCollegeAndTeam;
import com.xlr.pojo.Menus;
import com.xlr.pojo.Role;
import com.xlr.pojo.Userinfo;
import com.xlr.pojo.Userpermission;

/**
 * @ClassName: UserMapper
 * @Description:用户的Mapper接口
 * @author: 王宏志
 * @date: 2018年11月2日 下午2:13:52
 * 
 */
public interface UserMapper {

	/**
	 * @Title: findUserInfoByUsernameAndPassword @Description:
	 *         通过用户名与密码查询用户信息 @param: @param user_code @param: @param
	 *         user_pwd @param: @return @return: Userinfo @throws
	 */
	public Userinfo findUserInfoByUsernameAndPassword(@Param("user_code") String user_code,
			@Param("user_pwd") String user_pwd);

	/**
	 * @Title: deleteUserinfoByUserID @Description: 根据用户ID删除 @param: @param
	 *         user_id @param: @return @return: Integer @throws
	 */
	public Integer deleteUserinfoByUserID(@Param("user_id") Integer user_id);

	/**
	 * @Title: updateUserinfo @Description: 更新用户 @param: @param
	 *         userinfo @param: @return @return: Integer @throws
	 */
	public Integer updateUserinfo(Userinfo userinfo);

	/**
	 * @Title: insertUserinfo @Description: 添加用户 @param: @param
	 *         userinfo @param: @return @return: Integer @throws
	 */
	public Integer insertUserinfo(Userinfo userinfo);

	/**
	 * @Title: findMenuidByTypeId @Description: 根据用户类型ID查询父节点ID @param: @param
	 *         ru_ty_id @param: @return @return: List<Userpermission> @throws
	 */
	public List<String> findMenuidByTypeId(@Param("ru_ty_id") String ru_ty_id);

	/**
	 * @Title: findMenuidByPId @Description: 根据父节点ID确定子节点ID @param: @param
	 *         pid @param: @return @return: List<Permission> @throws
	 */
	public List<Menus> findMenusByPId(@Param("pid") Integer pid);

	/**
	 * @Title: findMenusByUserType @Description: 通过用户类型查找主菜单 @param: @param
	 *         user_type @param: @return @return: List<Menus> @throws
	 */
	public List<Menus> findMenusByUserType(@Param("user_type") String user_type);

	/**
	 * @Title: findUserinfoByParameter @Description:
	 *         根据用户名，用户类型，公司ID，政治面貌这些参数进行查询(参数可以为空)，
	 *         返回值包含Userinfo,Company,College对象 @param: @return @return:
	 *         List<Userinfo> @throws
	 */
	public List<Userinfo> findAllUserinfo(@Param("user_name") String user_name, @Param("user_type") String user_type,
			@Param("user_companyid") Integer user_companyid, @Param("beginTime") String beginTime,
			@Param("endTime") String endTime);

	/**
	 * @author 高俊
	 * @date 2018年11月11日上午11:48:35
	 * @param ru_ty_id
	 * @param rp_menuid
	 * @return
	 */
	public Integer authorization(@Param("ru_ty_id") String ru_ty_id, @Param("rp_menuid") String rp_menuid);

	/**
	 * @Title: selectAllAuthorization @Description: 查询所有的角色权限 @param:@Param
	 *         null @return:List<Userpermission> @throws
	 */
	public List<Userpermission> selectAllAuthorization();

	/**
	 * @author 高俊
	 * @date 2018年11月11日上午11:58:27
	 * @return
	 */
	public List<String> selectAllUserType();

	/**
	 * @Title: selectAuthorization @Description: 查询指定类型的角色权限 @param:@Param
	 *         ru_ty_id @return:List<Userpermission> @throws
	 */
	public List<Userpermission> selectAuthorization(@Param("ru_ty_id") String ru_ty_id);

	/**
	 * @Title: selectAuthorization @Description: 删除角色的权限 @param:@Param
	 *         ru_ty_id,@Param rp_menuid @return:Integer @throws
	 */
	public Integer deleteAuthorization(@Param("ru_ty_id") String ru_ty_id, @Param("rp_menuid") String rp_menuid);

	/**
	 * 查询用户一部分的信息
	 * @return
	 */
	public List<SimpleUser> findSimpleUser(SimpleUser user);

	/**
	 * 查询用户姓名，自动补全
	 * @return
	 * @param:@q 
	 */
	public List<Userinfo> findUserName(@Param("user_name")String user_name);
	/**
	 * 重置密码
	 * @return
	 */
	public Integer updatePwd(@Param("user_id") Integer userId, @Param("user_pwd") String userPwd);

	/**
	 * 
	 * @Title: selectUserRole
	 * @Description: 根据userid获取用户对应的角色,例如user_id=221
	 * @param userId
	 * @return List<Role>
	 * @author gj
	 * @date 2019年2月16日下午2:50:20
	 */
	public List<Role> selectUserRole(@Param("user_id") Integer user_id);
	
	/**
	 * 
	* @Title: deleteUserRole 
	* @Description: 删除用户关联的角色id
	* @param user_id
	* @return int
	* @author gj
	* @date 2019年2月16日下午3:51:24
	 */
	public void deleteUserRole(@Param("user_id") Integer user_id);
	/**
	 * 
	* @Title: addUserRole 
	* @Description: 给用户添加角色 
	* @param user_id
	* @param roleuuid
	* @return int
	* @author gj
	* @date 2019年2月16日下午3:53:14
	 */
	public void insertUserRole(@Param("user_id") Integer user_id,@Param("roleuuid") Integer roleuuid);
	/**
	 * 
	* @Title: selectUserCodeByUserid 
	* @Description: 根据user_id查找user_code 
	* @param user_id
	* @return int
	* @author gj
	* @date 2019年2月17日下午9:20:13
	 */
	public String selectUserCodeByUserid(@Param("user_id") Integer user_id);
	/**
	 * 
	* @Title: selectUserCollegeAndTeambyUserid 
	* @Description: 根据userid查询该user的院系以及所属团队 
	* @param user_id
	* @return String
	* @author gj
	* @date 2019年2月20日下午8:48:40
	 */
	public UserCollegeAndTeam selectUserCollegeAndTeambyUserid(@Param("user_id") Integer user_id);
	
}
