/**
 * @author 徐培珊
 * @date 2018年12月11日下午3:39:20
 * @version 1.0
 * @company 河南喜乐融商贸有限公司
 * @Copyright: 2018 www.xlr.com Inc. All rights reserved. 
 */
package com.xlr.mapper.system;

import java.util.List;

import com.xlr.pojo.TeamCertificate;

/**
 * @author 徐培珊
 * @date 2018年12月11日下午3:39:20
 */
public interface TeamCertificateMapper {

	/**
	 * 	查找所有证书
	 * @author 徐培珊
	 * @date 2018年12月11日下午7:48:23
	 * @return
	 */
	List<TeamCertificate> findAllCertificate();

}
