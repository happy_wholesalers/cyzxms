package com.xlr.mapper.company;

import org.apache.ibatis.annotations.Param;

import com.xlr.pojo.Charger;

/**
 * @author 王宏志
 * 入驻项目负责人Mapper
 * @date 2018年12月2日上午10:26:50
 * @TOOD 
 */
public interface ChargerMapper {
	/**
	 * @author 王宏志
	 *   根据公司ID查询入驻项目负责人信息
	 * @date 2018年12月2日上午10:26:47
	 * @param companyId
	 * @return
	 */
	public Charger selectUserinfo(@Param("userId")Integer userId);
	/**
	 * @author 王宏志
	 * 	修改项目入驻项目负责人信息
	 * @date 2018年12月2日上午10:29:42
	 * @param charger
	 * @return
	 */
	public Integer updateUserinfo(Charger charger);
	
	
}
