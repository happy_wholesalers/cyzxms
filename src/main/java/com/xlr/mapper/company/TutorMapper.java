package com.xlr.mapper.company;

import org.apache.ibatis.annotations.Param;

import com.xlr.pojo.Tutor;

/**
 * @author 范英豪 导师Mapper
 * @date 2019年2月18日下午15:26:50
 * @TOOD
 */
public interface TutorMapper {
	/*
	 * 根据id查导师
	 */
	public Tutor findById(@Param("t_companyid") Integer t_companyid);

	/**
	 * @author 范英豪
	 * @date 2019年2月18日下午16:46:50
	 * @return
	 */
	public void CreateOne(@Param("t_companyid") int t_companyid);

	/**
	 * @author 范英豪
	 * @date 2019年2月18日下午16:46:50
	 * @return
	 */
	public Integer updateByid(Tutor tutor);

}
