/**
 * @author 徐培珊
 * @date 2018年11月13日下午6:57:44
 * @version 1.0
 * @company 河南喜乐融商贸有限公司
 * @Copyright: 2018 www.xlr.com Inc. All rights reserved. 
 */
package com.xlr.mapper.company;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xlr.pojo.CompanyUsedAssets;


/**
 * @author 徐培珊
 * @date 2018年11月13日下午6:57:44 公司资产管理的Mapper
 */

public interface CompanyUsedAssetsMapper {
	// 添加一条公司资产记录
	public Integer insertAssets(CompanyUsedAssets companyUsedAssets);

	// 通过资产记录ID删除该条资产记录
	public Integer deleteAssetsByID(@Param("itemId") Integer itemId);

	// 修改公司资产记录
	public Integer updateAssets(CompanyUsedAssets companyUsedAssets);

	// 通过资产ID获取该条公司资产记录
	public CompanyUsedAssets selectCompanyAssessts(@Param("itemId") Integer itemId);

	// 查询特定公司ID下全部的资产
	public List<CompanyUsedAssets> selectAllCompanyAssertsByCompanyId(@Param("companyId") Integer companyId);
}
