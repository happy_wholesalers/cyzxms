/**
 * @author 徐培珊
 * @date 2018年12月1日下午6:05:54
 * @version 1.0
 * @company 河南喜乐融商贸有限公司
 * @Copyright: 2018 www.xlr.com Inc. All rights reserved. 
 */
package com.xlr.mapper.company;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.xlr.pojo.Protocol;
import com.xlr.utils.EasyUIDataGridResult;

/**
 * 	入驻协议
 * @author 徐培珊
 * @date 2018年12月1日下午6:05:54
 */
public interface ProtocolMapper {

	/**
	 * 	根据session中的公司id查询所有协议信息
	 * @author 徐培珊
	 * @date 2018年12月1日下午7:28:59
	 * @param companyId
	 * @return
	 */
	Protocol selectProtocolInfoByCompanyId(@Param("companyId")Integer companyId);
	
	/**
	 * 	根据session中的公司id更新协议信息
	 * @author 徐培珊
	 * @date 2018年12月1日下午6:27:58
	 * @param protocol
	 * @return
	 */
	Integer updateProtocolInfo(Protocol protocol);
	
	/**
	 * 	插入协议信息(公司第一次使用时，会用)
	 * @author 徐培珊
	 * @date 2018年12月1日下午6:27:47
	 * @param protocol
	 * @return
	 */
	Integer insertProtocolInfo(Protocol protocol);
	/**
	 * 	入驻协议统计_查询所有协议
	 * @author 徐培珊
	 * @date 2018年12月2日上午11:22:07
	 * @return
	 */
	List<Protocol> selectAllProtocol();
}
