package com.xlr.mapper.company;
import java.util.List;

import com.xlr.pojo.AssetsNameAndSeq;
import com.xlr.pojo.CompanyUsedAssets;
/**
 * @author 王宏志
 * 	针对于公司资产添加而设计的Mapper
 * @date 2018年12月6日下午2:22:12
 * @TOOD 
 */
public interface AssetsMapper {

	/**
	 * @author 王宏志
	 * 	查询全部的公共资产的名称与编号
	 * @date 2018年12月6日下午2:31:11
	 * @return
	 */
	public List<AssetsNameAndSeq> getAssetsNameAndSequence();
	
	/**
	 * @author 王宏志
	 * 	通过资产编号获取资产名称
	 * @date 2018年12月6日下午2:33:07
	 * @param sequence
	 * @return
	 */
	public String getAssetsNameBySequence(String sequence);
	
}
