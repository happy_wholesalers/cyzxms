/**
 * @author 李贺鹏
 * @date 2019年2月23日上午9:23:14
 * @version 1.0
 * @company 河南喜乐融商贸有限公司
 */
package com.xlr.mapper.company;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xlr.pojo.OverViewAll;
import com.xlr.pojo.Overview;

/**
 * @author 李贺鹏
 * @date 2019年2月23日上午9:23:14
 */
public interface OverviewMapper {
	/**
	 * 根据公司id查询
	 * @author 李贺鹏
	 * @date 2019年2月23日上午9:24:57
	 * @param companyid
	 * @return Overview项目概况
	 */
	public Overview findByCompanyid(@Param("o_companyid") Integer o_companyid);
	
	/**
	 * 插入一个overview
	 * @author 李贺鹏
	 * @date 2019年2月23日上午9:29:28
	 * @param overview
	 */
	public void insertOverview(@Param("o_companyid") Integer o_companyid);
	
	/**
	 * 更新overview
	 * @author 李贺鹏
	 * @date 2019年2月23日上午9:30:50
	 * @param overview
	 * 
	 */
	public Integer updateOverview(Overview overview);

	/**
	 * 入驻申请统计显示所有表格信息
	 * @author Zoe
	 * @date 2019年2月28日下午4:30:02
	 * @param companyId 公司id
	 * @return
	 */
	OverViewAll findAllTable(@Param("companyId")Integer companyId);

	/**
	 * 添加项目概况
	 * @author Zoe
	 * @date 2019年3月1日上午10:54:49
	 * @param overview 项目概况对象
	 */
	public Integer insert(Overview overview);

	/**
	 * 添加中间表的数据
	 * @author Zoe
	 * @date 2019年3月1日下午2:03:20
	 * @param oid 项目概况主键
	 * @param bid 数据字典主键
	 * @return
	 */
	public Integer insertViewDict(Integer oid, Integer bid);

	/**
	 * 分页查找所有项目概况
	 * @author Zoe
	 * @date 2019年3月1日下午2:40:52
	 * @return
	 */
	public List<Overview> listByPage();

	/**
	 * 更新项目概况
	 * @author Zoe
	 * @date 2019年3月1日下午2:51:46
	 * @param overview
	 * @return
	 */
	public Integer update(Overview overview);

	/**
	 * 根据项目概况主键删除中间表数据
	 * @author Zoe
	 * @date 2019年3月1日下午3:24:08
	 * @param o_id 项目概况主键
	 */
	public void deleteViewDict(Integer o_id);
}
