package com.xlr.mapper.company;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xlr.pojo.Company;

/**
 * @ClassName: CompanyMapper
 * @Description:公司的Mapper
 * @author: 王宏志
 * @date: 2018年11月5日 下午7:16:38
 * 
 */
public interface CompanyMapper {

	/**
	 * @Title: insertCompany
	 * @Description: 插入一条记录
	 * @param: @return
	 * @return: Integer
	 * @throws
	 */
	public Integer insertCompany(Company company);

	/**
	 * @Title: deleteCompanyByCompanyId
	 * @Description: 通过公司ID删除公司信息
	 * @param: @param company_id
	 * @param: @return
	 * @return: Integer
	 * @throws
	 */
	public Integer deleteCompanyByCompanyId(
			@Param("company_id") Integer company_id);

	/**
	 * @Title: updateCompany
	 * @Description: 更新公司信息
	 * @param: @param company
	 * @param: @return
	 * @return: Integer
	 * @throws
	 */
	public Integer updateCompany(Company company);

	/**
	 * @Title: selectAllCompany
	 * @Description:查询全部公司，不需要参数
	 * @param: @return
	 * @return: List<Company>
	 * @throws
	 */
	public List<Company> selectAllCompany();

	/**
	 * 根据公司id，更新公司营业执照
	 * 
	 * @author 高俊
	 * @date 2018年11月14日下午8:30:51
	 * @param company
	 * @return
	 */
	public Integer updateCompanyImage(Company company);

	/**
	 * 根据公司id查询公司营业执照
	 * 
	 * @author 高俊
	 * @date 2018年11月14日下午8:45:41
	 * @param user_companyid
	 * @return
	 */
	public Company findCompanyImageId(Integer user_companyid);

	/**
	 * 根据session中的用户名查询公司信息
	 * 
	 * @author 徐培珊
	 * @date 2018年12月5日下午8:53:11
	 * @param user_code
	 * @return
	 */
	public List<Company> selectAllCompanyByUserCode(
			@Param("user_code") String user_code);

	/**
	 * 根据孵化类型查询公司
	 * 
	 * @author 徐培珊
	 * @date 2018年12月6日上午11:48:53
	 * @param hatchType
	 * @return
	 */
	public List<Company> selectAllCompanyByHatchType(
			@Param("hatchType") Integer hatchType,
			@Param("company_name") String company_name,
			@Param("beginTime") String beginTime,
			@Param("endTime") String endTime);

	/**
	 * 根据id查询公司
	 * 
	 * @author 范英豪
	 * @date 2019年2月17日下午16:48:53
	 */

	public Company selectCompanyById(@Param("company_id") Integer company_id);
}
