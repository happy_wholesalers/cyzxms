package com.xlr.mapper.company;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xlr.pojo.College;

/**
 * @author 王宏志
 * 
 *         院系Mapper
 * @date 2018年12月5日下午8:46:09
 * @TOOD
 */
public interface CollegeMapper {

	/**
	 * @author 王宏志 查询全部的院系
	 * @date 2018年12月5日下午8:46:50
	 * @return
	 */
	public List<College> selectAllCollege();

	/**
	 * @author 范英豪 根据id查询院系
	 * @date 2019年2月17日下午16:25:50
	 * @return
	 */
	public College findByid(@Param("college_id")Integer college_id);
	
}
