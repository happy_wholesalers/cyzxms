package com.xlr.mapper.company;

import java.util.List;

import com.xlr.pojo.Employee;

/**
 * @author 王宏志
 * 	雇员的Mapper接口
 * @date 2018年12月3日下午8:31:59
 * @TOOD 
 */
public interface EmployeeMapper {

	/**
	 * @author 王宏志
	 * 	添加职员
	 * @date 2018年12月4日下午4:32:17
	 * @param employee
	 * @return
	 */
	public Integer insertEmployee(Employee employee);
	/**
	 * @author 王宏志
	 * 	通过ID删除职员
	 * @date 2018年12月4日下午4:34:06
	 * @param id
	 * @return
	 */
	public Integer deleteEmplyeeByID(Integer id);
	/**
	 * @author 王宏志
	 * 	修改职员信息
	 * @date 2018年12月4日下午4:35:03
	 * @param employee
	 * @return
	 */
	public Integer updateEmplyee(Employee employee);
	/**
	 * @author 王宏志
	 * 	通过公司ID查询职员信息
	 * @date 2018年12月4日下午4:37:02
	 * @param id
	 * @return
	 */
	public List<Employee> selectEmployeeByCompanyID(Integer id);
}
