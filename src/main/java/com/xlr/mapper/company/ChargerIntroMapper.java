package com.xlr.mapper.company;

import com.xlr.pojo.ChargerIntro;

/**
 * @author 王宏志
 * 入驻项目负责人简历的Mapper接口
 * @date 2018年12月4日下午7:02:47
 * @TOOD 
 */
public interface ChargerIntroMapper {
	
	/**
	 * @author 王宏志
	 * 	通过公司ID查询入驻项目负责人简历
	 * @date 2018年12月4日下午7:05:54
	 * @return
	 */
	public ChargerIntro selectChargerIntroByCompanyId(Integer cid);
	/**
	 * @author 王宏志
	 * 	修改项目负责人简历
	 * @date 2018年12月4日下午7:08:09
	 * @param chargerIntro
	 * @return
	 */
	public Integer updateChargerIntro(ChargerIntro chargerIntro);
	

}
