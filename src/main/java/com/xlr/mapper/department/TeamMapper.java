package com.xlr.mapper.department;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xlr.pojo.Team;

/**
 * @author 王宏志
 * 
 *         院系下团队的Mapper(孵化团队)
 * @date 2018年12月11日下午3:25:13
 * @TOOD
 */
public interface TeamMapper {

	/**
	 * @author 王宏志 获取全部的团队信息
	 * @date 2018年12月11日下午3:27:09
	 * @return
	 */
	public List<Team> getAllTeam(@Param("t_name") String t_name,
			@Param("t_leader") String t_leader,
			@Param("beginTime") String beginTime,
			@Param("endTime") String endTime);

	/**
	 * @author 王宏志 通过编号删除团队
	 * @date 2018年12月11日下午3:27:58
	 * @param id
	 * @return
	 */
	public Integer deleteTeamById(Integer id);

	/**
	 * @author 王宏志 修改团队信息
	 * @date 2018年12月11日下午3:28:43
	 * @param team
	 * @return
	 */
	public Integer updateTeam(Team team);

	/**
	 * @author 王宏志 添加团队信息
	 * @date 2018年12月11日下午3:29:24
	 * @param team
	 * @return
	 */
	public Integer insertTeam(Team team);

}
