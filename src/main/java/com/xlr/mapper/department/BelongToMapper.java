/**
 * @author 徐培珊
 * @date 2018年12月4日下午6:30:51
 * @version 1.0
 * @company 河南喜乐融商贸有限公司
 * @Copyright: 2018 www.xlr.com Inc. All rights reserved. 
 */
package com.xlr.mapper.department;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xlr.pojo.Company;

/**
 * @author 徐培珊
 * @date 2018年12月4日下午6:30:51
 */
public interface BelongToMapper {

	/**
	 * 根据session中的院系id，加载公司列表
	 * 
	 * @author 徐培珊
	 * @date 2018年12月4日下午6:31:59
	 * @param deptId
	 * @return
	 */
	List<Company> findCompanyBy(@Param("deptId") Integer deptId,
			@Param("company_name") String company_name,
			@Param("company_istechnology") Integer company_istechnology,
			@Param("company_leader") String company_leader,
			@Param("beginTime") String beginTime,
			@Param("endTime") String endTime);

}
