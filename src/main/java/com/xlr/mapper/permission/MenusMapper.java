package com.xlr.mapper.permission;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xlr.pojo.Menus;
import com.xlr.utils.Tree;

/**
 * 菜单管理底层实现接口
 * 
 * @author Zoe
 * @date 2019年2月15日下午3:52:10
 */
public interface MenusMapper {

	/**
	 * 查找所有数据
	 * 
	 * @author Zoe
	 * @date 2019年2月15日下午3:52:41
	 * @return
	 */
	List<Tree> findAll();

	/**
	 * 根据菜单id查找菜单，显示菜单详情
	 * 
	 * @author Zoe
	 * @date 2019年2月15日下午8:16:49
	 * @param menuid
	 * @return
	 */
	List<Menus> findById(@Param("menuid") String menuid);

	/**
	 * 添加数据
	 * 
	 * @author Zoe
	 * @date 2019年2月15日下午9:56:34
	 * @param menus 菜单对象
	 * @return
	 */
	Integer insert(Menus menus);

	/**
	 * 根据id删除数据[修改状态]
	 * 
	 * @author Zoe
	 * @date 2019年2月15日下午9:57:50
	 * @param menuid 主键
	 * @param status 状态
	 * @return
	 */
	Integer deleteById(String menuid, String status);

	/**
	 * 根据id修改数据
	 * 
	 * @author Zoe
	 * @date 2019年2月15日下午9:58:24
	 * @param menus 菜单对象
	 * @return
	 */
	Integer updateById(Menus menus);
	
	/**
	 * 
	 * @Title: selectMenusIdName
	 * @Description: 根据pid获取所有可用的权限菜单(menuid,menuname)
	 * @return List<Menus>
	 * @author gj
	 * @date 2019年2月16日下午7:05:10
	 */
	public List<Menus> selectMenusIdName(@Param("pid") String pid);

	/**
	 * 
	 * @Title: selectMenusByUserid
	 * @Description: 根据userid加载对应菜单
	 * @param userid
	 * @return List<Menus>
	 * @author gj
	 * @date 2019年2月16日下午8:40:39
	 */
	public List<Menus> selectMenusByUserid(@Param("userid") Integer userid);
	
	/**
	 * 
	* @Title: selectMenus 
	* @Description: 查询所有可用菜单的所有属性 
	* @param pid
	* @return List<Menus>
	* @author gj
	* @date 2019年2月16日下午9:04:26
	 */
	public List<Menus> selectMenus(@Param("pid") String pid);
	
}
