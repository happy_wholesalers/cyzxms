package com.xlr.mapper.permission;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xlr.pojo.Menus;
import com.xlr.pojo.Role;

public interface RoleMapper {
	/**
	 * 
	 * @Title: selectRoleByPage
	 * @Description: 根据条件查询所有角色
	 * @param role
	 * @return List<Role>
	 * @author gj
	 * @date 2019年2月14日下午7:12:56
	 */
	public List<Role> selectRoleByPage(Role role);

	/**
	 * 
	 * @Title: updateRole
	 * @Description: 更新角色信息
	 * @param role
	 * @return Integer
	 * @author gj
	 * @date 2019年2月14日下午8:22:57
	 */
	public Integer updateRole(Role role);

	/**
	 * 
	 * @Title: insertRole
	 * @Description: 添加角色
	 * @param role
	 * @return Integer
	 * @author gj
	 * @date 2019年2月15日上午11:50:36
	 */
	public Integer insertRole(Role role);

	/**
	 * 
	 * @Title: selectRole
	 * @Description: 查询所有可用的角色
	 * @param role
	 * @return List<Role>
	 * @author gj
	 * @date 2019年2月16日下午12:28:09
	 */
	public List<Role> selectRoleByEnble();

	/**
	 * 
	 * @Title: selectRoleMenuidByRoleid
	 * @Description: 根据角色id获取对应的权限菜单id
	 * @param roleuuid
	 * @return List<Integer>
	 * @author gj
	 * @date 2019年2月16日下午4:35:08
	 */
	public List<String> selectRoleMenuidByRoleid(@Param("roleuuid") Integer roleuuid);
	/**
	 * 
	* @Title: selectUseridByRoleuuid 
	* @Description: 根据角色id获取对应的用户id 
	* @param roleuuid
	* @return List<Integer>
	* @author gj
	* @date 2019年2月17日上午11:18:02
	 */
	public List<Integer> selectUseridByRoleuuid(@Param("roleuuid") Integer roleuuid);

	/**
	 * 
	 * @Title: deleteMenuidByRoleid
	 * @Description: 根据roleuuid删除拥有的角色信息
	 * @param roleuuid void
	 * @author gj
	 * @date 2019年2月16日下午8:05:53
	 */
	public void deleteMenuidByRoleid(@Param("roleuuid") Integer roleuuid);

	/**
	 * 
	 * @Title: insertRoleMenus
	 * @Description: 新增角色权限菜单关系
	 * @param menuuuid
	 * @param roleuuid void
	 * @author gj
	 * @date 2019年2月16日下午8:07:02
	 */
	public void insertRoleMenus(@Param("menuuuid") String menuuuid, @Param("roleuuid") Integer roleuuid);

}
