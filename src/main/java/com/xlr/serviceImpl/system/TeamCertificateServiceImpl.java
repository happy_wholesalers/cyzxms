/**
 * @author 徐培珊
 * @date 2018年12月11日下午3:40:47
 * @version 1.0
 * @company 河南喜乐融商贸有限公司
 * @Copyright: 2018 www.xlr.com Inc. All rights reserved. 
 */
package com.xlr.serviceImpl.system;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlr.mapper.system.TeamCertificateMapper;
import com.xlr.pojo.TeamCertificate;
import com.xlr.service.system.TeamCertificateService;

/**
 * @author 徐培珊
 * @date 2018年12月11日下午3:40:47
 */
@Service
public class TeamCertificateServiceImpl implements TeamCertificateService {

	@Autowired
	private TeamCertificateMapper teamCertificateMapper;
	/**
	 * @author 徐培珊
	 * @date 2018年12月11日下午7:46:59
	 * @return
	 */
	@Override
	public List<TeamCertificate> findAllCertificate() {
		return teamCertificateMapper.findAllCertificate();
	}
	
}
