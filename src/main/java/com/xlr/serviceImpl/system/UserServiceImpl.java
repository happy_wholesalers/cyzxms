/**
 * @author 徐培珊
 * @date 2018年11月1日下午8:07:48
 * @version 1.0
 * @company 河南喜乐融商贸有限公司
 * @Copyright: 2018 www.xlr.com Inc. All rights reserved. 
 */
package com.xlr.serviceImpl.system;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xlr.dto.SimpleUser;
import com.xlr.dto.UserCollegeAndTeam;
import com.xlr.mapper.permission.RoleMapper;
import com.xlr.mapper.system.UserMapper;
import com.xlr.pojo.Company;
import com.xlr.pojo.Menus;
import com.xlr.pojo.Role;
import com.xlr.pojo.Userinfo;
import com.xlr.service.system.UserService;
import com.xlr.serviceImpl.permission.JedisClient;
import com.xlr.utils.EasyUIDataGridResult;
import com.xlr.utils.EasyUIOptionalTreeNode;
import com.xlr.utils.ManagerResult;

/**
 * @author 徐培珊
 * @date 2018年11月1日下午8:07:48
 */
@Service("userService")
public class UserServiceImpl implements UserService {
	
	private int hashIterations = 2;
	@Autowired
	private UserMapper userMapper;
	@Autowired
	private RoleMapper roleMapper;
	@Autowired
	private JedisClient jedisClient;

	/**
	 * (non Javadoc) 
	 * @Title: findUserInfo
	 * @Description: 根据用户名和密码查询用户信息
	 * @param user_code
	 * @param user_pwd
	 * @return 
	 * @see com.xlr.service.system.UserService#findUserInfo(java.lang.String, java.lang.String)
	 */
	@Override
	public Userinfo findUserInfo(String user_code,String user_pwd) {
		//密码加密
		user_pwd = encrypt(user_pwd, user_code);
		System.out.println(user_pwd);
		// 获取数据库用户信息
		Userinfo userinfo = userMapper.findUserInfoByUsernameAndPassword(user_code,
				user_pwd);
		return userinfo;
	}
	
	/**
	 * 加密
	 * @param source
	 * @param salt
	 * @return
	 */
	private String encrypt(String source, String salt){
		Md5Hash md5 = new Md5Hash(source, salt, hashIterations);
		return md5.toString();
	}

	@Override
	public EasyUIDataGridResult findUserinfoList(Userinfo userInfo, Integer page, Integer rows) {
		if (userInfo == null) {
			// 将参数置为空
			userInfo = new Userinfo();
			userInfo.setUser_code(null);
			userInfo.setUser_name(null);
			userInfo.setUser_type(null);
			userInfo.setUser_companyid(null);
			userInfo.setUser_politics(null);
			userInfo.setBeginDate(null);
			userInfo.setEndDate(null);
		}
		if ("".equals(userInfo.getUser_name())) {
			userInfo.setUser_name(null);
		}
		if ("".equals(userInfo.getUser_type())) {
			userInfo.setUser_type(null);
		}
		if ("".equals(userInfo.getUser_companyid())) {
			userInfo.setUser_companyid(null);
		}
		if ("".equals(userInfo.getEndDate())) {
			userInfo.setEndDate(null);
		}
		if ("".equals(userInfo.getBeginDate())) {
			userInfo.setBeginDate(null);
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if (userInfo.getBeginDate() != null && !"".equals(userInfo.getBeginDate())) {
			userInfo.setBeginTime(sdf.format(userInfo.getBeginDate()));
		}
		if (userInfo.getEndDate() != null && !"".equals(userInfo.getEndDate())) {
			userInfo.setEndTime(sdf.format(userInfo.getEndDate()));
		}
		PageHelper.startPage(page, rows);
		List<Userinfo> list = userMapper.findAllUserinfo(userInfo.getUser_name(), userInfo.getUser_type(),
				userInfo.getUser_companyid(), userInfo.getBeginTime(), userInfo.getEndTime());
		PageInfo<Userinfo> pageInfo = new PageInfo<>(list);
		// 4.封装EasyUIDataGridResult
		EasyUIDataGridResult result = new EasyUIDataGridResult();
		result.setTotal((int) pageInfo.getTotal());
		result.setRows(pageInfo.getList());
		// 5.返回分页的结果
		return result;
	}

	/**
	 * 更新用户信息
	 * 
	 * @author 徐培珊
	 * @date 2018年11月6日上午9:07:37
	 * @param userInfo
	 * @return
	 */
	@Override
	public ManagerResult updateUserInfo(Userinfo userInfo) {
		if (userInfo == null) {
			return new ManagerResult(400, "用户信息为空，修改失败！", 400);
		}
		// 1. 调用用户更新方法
		/*
		 * 2. 判断方法的返回值是否为0 1. 如果为0，表示更新失败， status=400 2. 如果非0，表示跟新成功， status=200
		 */
		Integer integer = userMapper.updateUserinfo(userInfo);
		if (integer == 0) {
			return new ManagerResult(400, "用户信息更新失败", null);
		} else {
			return new ManagerResult(200, "用户信息更新成功", null);
		}
	}

	/**
	 * 添加用户信息
	 * 
	 * @author 徐培珊
	 * @date 2018年11月6日上午9:07:28
	 * @param userInfo
	 * @return
	 */
	@Override
	public ManagerResult insertUserInfo(Userinfo userInfo) {
		if (userInfo == null) {
			return new ManagerResult(400, "用户信息为空，添加失败！", 400);
		}
		// 1. 调用用户插入方法
		/*
		 * 2. 判断方法的返回值是否为0 1. 如果为0，表示更新失败， status=400 2. 如果非0，表示跟新成功， status=200
		 */
		Integer integer = userMapper.insertUserinfo(userInfo);
		if (integer == 0) {
			return new ManagerResult(400, "用户添加失败", null);
		} else {
			return new ManagerResult(200, "用户添加成功", null);
		}
	}

	/**
	 * 根据用户id删除用户信息
	 * 
	 * @author 徐培珊
	 * @date 2018年11月6日上午9:07:15
	 * @param userId
	 * @return
	 */
	@Override
	public ManagerResult deleteUserInfo(Integer userId) {
		if (userId == null) {
			return new ManagerResult(400, "用户id为空，添加失败！", 400);
		}
		Integer integer = userMapper.deleteUserinfoByUserID(userId);
		if (integer == 0) {
			return new ManagerResult(400, "用户删除失败", null);
		} else {
			return new ManagerResult(200, "用户删除成功", null);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.xlr.service.UserService#findMenuByUserType(java.lang.Integer)
	 */
	@Override
	public List<Menus> findMenuByUserType(String user_type) {
		List<Menus> list = new LinkedList<Menus>();
		List<Menus> menusList = userMapper.findMenusByUserType(user_type);
		for (Menus menus : menusList) {
			List<Menus> menusList1 = userMapper.findMenusByPId(Integer.parseInt(menus.getMenuid()));
			for (Menus menus1 : menusList1) {
				menus.getMenus().add(menus1);
			}
			list.add(menus);
		}
		return list;
	}

	/**
	 * 查询所有公司
	 * 
	 * @author 徐培珊
	 * @date 2018年11月3日下午8:03:24
	 * @return
	 */
	@Override
	public EasyUIDataGridResult findAllCompanyToList(Company company, Integer page, Integer rows) {
		if (company == null) {
			// 将参数置为空
		}
		// 1.设置分页信息
		PageHelper.startPage(page, rows);
		// TODO 2.执行查询， 查询所有公司
		// List<Userinfo> list =
		// 3.封装pageInfo
		// PageInfo<Userinfo> pageInfo = new PageInfo<>(list);
		// 4.封装EasyUIDataGridResult
		// EasyUIDataGridResult result = new EasyUIDataGridResult();
		// result.setTotal((int) pageInfo.getTotal());
		// result.setRows(pageInfo.getList());
		// 5.返回分页的结果
		// return result;
		return null;
	}

	/**
	 * 查询userinfo一部分数据
	 */
	@Override
	public EasyUIDataGridResult findSimpleUser(SimpleUser user,Integer page, Integer rows) {
		// 1.查询分页数据
		PageHelper.startPage(page, rows);
		List<SimpleUser> list = userMapper.findSimpleUser(user);
		PageInfo<SimpleUser> pageInfo = new PageInfo<>(list);
		// 2.封装EasyUIDataGridResult
		EasyUIDataGridResult result = new EasyUIDataGridResult();
		result.setTotal((int) pageInfo.getTotal());
		result.setRows(pageInfo.getList());
		// 3.返回分页的结果
		return result;
	}

	/**
	 * (non Javadoc) 
	 * @Title: resetPwd
	 * @Description: 系统管理员重置用户密码
	 * @param userId
	 * @param userPwd
	 * @return 
	 * @see com.xlr.service.system.UserService#resetPwd(java.lang.Integer, java.lang.String)
	 */
	@Override
	public ManagerResult resetPwd(Integer userId, String userPwd) {
		try {
			if (userId != null && StringUtils.isNoneBlank(userPwd)) {
				String user_code = userMapper.selectUserCodeByUserid(userId);
				Integer row = userMapper.updatePwd(userId, encrypt(userPwd,user_code));
				if (row > 0) {
					return new ManagerResult(200, "密码重置成功", null);
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new ManagerResult(400, "密码重置失败", null);
	}


	/* (non-Javadoc)
	 * 查询用户名
	 */
	@Override
	public List<Userinfo> findUserName(String q) {
		List<Userinfo> list = userMapper.findUserName(q);
		return list;
	}


	/**
	 * (non Javadoc)
	 * 
	 * @Title: findUserRole
	 * @Description: TODO
	 * @param user_id
	 * @return
	 * @see com.xlr.service.system.UserService#findUserRole(java.lang.Integer)
	 */
	@Override
	public List<EasyUIOptionalTreeNode> findUserRole(Integer user_id) {
		// 1.获取当前用户的所有角色
		List<Role> userRoleList = userMapper.selectUserRole(user_id);
		// 2.获取系统中所有可用角色
		List<Role> roleList = roleMapper.selectRoleByEnble();
		// 3.设置返回值
		List<EasyUIOptionalTreeNode> treeList = new ArrayList<EasyUIOptionalTreeNode>();
		EasyUIOptionalTreeNode t1 = null;
		// 4.封装返回值将用户对应的角色设置为true
		for (Role role : roleList) {
			t1 = new EasyUIOptionalTreeNode();
			t1.setId(role.getUuid()+"");
			t1.setText(role.getName());
			// 如果用户拥有这个角色，设为true
			for (Role userRole : userRoleList) {
				if (userRole.getUuid() == role.getUuid()) {
					t1.setChecked(true);
				}
			}
			treeList.add(t1);
		}
		return treeList;
	}
	/**
	 * (non Javadoc) 
	 * @Title: findUserRoleByUserid
	 * @Description: TODO
	 * @param user_id
	 * @return 
	 * @see com.xlr.service.system.UserService#findUserRoleByUserid(java.lang.Integer)
	 */
	@Override
	public List<Role> findUserRoleByUserid(Integer user_id) {
		return userMapper.selectUserRole(user_id);
	}
	
	/**
	 * (non Javadoc) 
	 * @Title: updateUserRole
	 * @Description: TODO
	 * @param user_id
	 * @param checkedIds
	 * @return 
	 * @see com.xlr.service.system.UserService#updateUserRole(java.lang.Integer, java.lang.String)
	 */
	@Override
	public ManagerResult updateUserRole(Integer user_id, String checkedIds) {
		try {
			// 先删除用户下的所有角色
			userMapper.deleteUserRole(user_id);
			if (checkedIds != null) {
				String[] ids = checkedIds.split(",");
				for (String roleuuid : ids) {
					// 设置用户的角色
					userMapper.insertUserRole(user_id, Integer.parseInt(roleuuid));
				}
			}
			//清除缓存
			jedisClient.del("easyuiMenus_"+user_id);
			jedisClient.del("menusList_"+user_id);
			System.out.println("更新用户对应的角色 ，清除缓存");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ManagerResult.build(200, "保存成功");
	}
	/**
	 * (non Javadoc) 
	 * @Title: findUserCollegeAndTeambyUserid
	 * @Description: TODO
	 * @param user_id
	 * @return 
	 * @see com.xlr.service.system.UserService#findUserCollegeAndTeambyUserid(java.lang.Integer)
	 */
	@Override
	public UserCollegeAndTeam findUserCollegeAndTeambyUserid(Integer user_id) {
		return userMapper.selectUserCollegeAndTeambyUserid(user_id);
	}

	

}
