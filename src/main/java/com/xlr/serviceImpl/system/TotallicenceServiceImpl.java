/**
 * @author 徐培珊
 * @date 2018年11月27日下午2:59:19
 * @version 1.0
 * @company 河南喜乐融商贸有限公司
 * @Copyright: 2018 www.xlr.com Inc. All rights reserved. 
 */
package com.xlr.serviceImpl.system;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlr.mapper.system.TotallicenceMapper;
import com.xlr.pojo.Company;
import com.xlr.service.system.TotallicenceService;
import com.xlr.utils.ManagerResult;

/**
 * @author 李贺鹏
 * @date 2018年11月27日下午2:59:19
 */
@Service("TotallicenceService")
public class TotallicenceServiceImpl implements TotallicenceService{

	@Autowired
	private TotallicenceMapper totallicenceMapper;
	
	@Override
	public List<Company> totallicenceList() {
		List<Company> findAllLicence = totallicenceMapper.findAllLicence();
		return findAllLicence;
	}
}
