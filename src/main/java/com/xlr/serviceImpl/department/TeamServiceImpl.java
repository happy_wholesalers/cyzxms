package com.xlr.serviceImpl.department;

import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xlr.mapper.company.CompanyMapper;
import com.xlr.mapper.department.TeamMapper;
import com.xlr.pojo.Company;
import com.xlr.pojo.Team;
import com.xlr.pojo.Userinfo;
import com.xlr.service.company.CompanyService;
import com.xlr.service.department.TeamService;
import com.xlr.utils.EasyUIDataGridResult;
import com.xlr.utils.ManagerResult;

@Service("teamService")
public class TeamServiceImpl implements TeamService {

	@Autowired
	private TeamMapper teamMapper;

	@Override
	public EasyUIDataGridResult findAllTeamByPage(Team team, Integer page,
			Integer rows) {
		if (team == null) {
			// 将参数置为空
			team = new Team();
			team.setT_leader(null);
			team.setT_name(null);
			team.setBeginDate(null);
			team.setEndDate(null);
		}
		if ("".equals(team.getT_leader())) {
			team.setT_leader(null);
		}
		if ("".equals(team.getT_name())) {
			team.setT_name(null);
		}
		if ("".equals(team.getBeginDate())) {
			team.setBeginDate(null);
		}
		if ("".equals(team.getEndDate())) {
			team.setEndDate(null);
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if (team.getBeginDate() != null && !"".equals(team.getBeginDate())) {
			team.setBeginTime(sdf.format(team.getBeginDate()));
		}
		if (team.getEndDate() != null && !"".equals(team.getEndDate())) {
			team.setEndTime(sdf.format(team.getEndDate()));
		}
		// 1.设置分页信息
		PageHelper.startPage(page, rows);
		// 2.执行查询
		List<Team> list = teamMapper.getAllTeam(team.getT_name(),
				team.getT_leader(), team.getBeginTime(), team.getEndTime());
		// 3.封装pageInfo
		PageInfo<Team> pageInfo = new PageInfo<>(list);
		// 4.封装EasyUIDataGridResult
		EasyUIDataGridResult result = new EasyUIDataGridResult();
		result.setTotal((int) pageInfo.getTotal());
		result.setRows(pageInfo.getList());
		// 5.返回分页的结果
		return result;
	}

	@Override
	public ManagerResult updateTeam(Team team) {

		Integer row = teamMapper.updateTeam(team);
		if (row > 0) {
			return ManagerResult.build(200, "公司信息更新成功");
		} else {
			return ManagerResult.build(400, "公司信息更新失败");
		}

	}

	@Override
	public ManagerResult deleteTeamById(Integer t_id) {
		// 资产编号为空
		if (null == t_id) {
			return ManagerResult.build(400, "删除失败, 资产编号为空");
		}
		Integer i = teamMapper.deleteTeamById(t_id);
		if (i > 0) {
			return ManagerResult.build(200, "删除成功");
		}
		return ManagerResult.build(400, "删除失败");

	}

	@Override
	public ManagerResult addTeam(Team team) {

		if (team == null) {
			return ManagerResult.build(400, "添加公司信息失败");
		}
		Integer i = teamMapper.insertTeam(team);
		if (i > 0) {
			return ManagerResult.build(200, "添加公司信息成功");
		}
		return ManagerResult.build(400, "添加公司信息失败");

	}

	/* (non-Javadoc)
	 * @see com.xlr.service.department.TeamService#findDeptTeamLeaderName(java.lang.String)
	 */
	@Override
	public List<Team> findDeptTeamLeaderName(String t_leader) {
		
		return teamMapper.getAllTeam(null, t_leader, null, null);
	}
}
