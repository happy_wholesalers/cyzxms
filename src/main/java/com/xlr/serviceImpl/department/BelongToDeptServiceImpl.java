/**
 * @author 徐培珊
 * @date 2018年12月4日下午6:19:56
 * @version 1.0
 * @company 河南喜乐融商贸有限公司
 * @Copyright: 2018 www.xlr.com Inc. All rights reserved. 
 */
package com.xlr.serviceImpl.department;

import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xlr.mapper.department.BelongToMapper;
import com.xlr.pojo.Company;
import com.xlr.service.department.BelongToDeptService;
import com.xlr.utils.EasyUIDataGridResult;

/**
 * 根据session中的院系id操作
 * 
 * @author 徐培珊
 * @date 2018年12月4日下午6:19:56
 */
@Service
public class BelongToDeptServiceImpl implements BelongToDeptService {

	@Autowired
	BelongToMapper belongToMapper;

	/**
	 * @author 徐培珊
	 * @date 2018年12月4日下午6:29:25
	 * @param deptId
	 * @return
	 */
	@Override
	public EasyUIDataGridResult findCompanyBypage(Company company,
			Integer page, Integer rows) {
		if (company == null) {
			company.setBeginDate(null);
			company.setEndDate(null);
			company.setCompany_name(null);
			company.setCompany_istechnology(null);
			company.setCompany_leader(null);
		}
		if ("".equals(company.getCompany_istechnology())) {
			company.setCompany_istechnology(null);
		}
		if ("".equals(company.getCompany_leader())) {
			company.setCompany_leader(null);
		}
		if ("".equals(company.getCompany_name())) {
			company.setCompany_name(null);
		}
		if ("".equals(company.getEndDate())) {
			company.setEndDate(null);
		}
		if ("".equals(company.getBeginDate())) {
			company.setBeginDate(null);
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if (company.getBeginDate() != null
				&& !"".equals(company.getBeginDate())) {
			company.setBeginTime(sdf.format(company.getBeginDate()));
		}
		if (company.getEndDate() != null && !"".equals(company.getEndDate())) {
			company.setEndTime(sdf.format(company.getEndDate()));
		}
		System.out.println(company.getCompany_leader());
		// 1.设置分页信息
		PageHelper.startPage(page, rows);
		// 2.执行查询
		List<Company> list = belongToMapper.findCompanyBy(
				company.getCompany_departmentid(), company.getCompany_name(),
				company.getCompany_istechnology(), company.getCompany_leader(),
				company.getBeginTime(), company.getEndTime());
		// 3.封装pageInfo
		PageInfo<Company> pageInfo = new PageInfo<>(list);
		// 4.封装EasyUIDataGridResult
		EasyUIDataGridResult result = new EasyUIDataGridResult();
		result.setTotal((int) pageInfo.getTotal());
		result.setRows(pageInfo.getList());
		// 5.返回分页的结果
		return result;
	}
	/**
	 * @author 李贺鹏
	 * @date 2019年2月16日下午8:58:24
	 * @param company_leader
	 * @return List<Company>
	 */
	@Override
	public List<Company> findDepCompanyLeaderName(String company_leader) {
		List<Company> company=belongToMapper.findCompanyBy(null, null, null, company_leader, null, null);
		return company;
	}

}
