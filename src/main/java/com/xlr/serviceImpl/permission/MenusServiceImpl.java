package com.xlr.serviceImpl.permission;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.xlr.mapper.permission.MenusMapper;
import com.xlr.pojo.Menus;
import com.xlr.service.permission.MenusService;
import com.xlr.utils.ManagerResult;
import com.xlr.utils.Tree;

/**
 * 菜单管理接口实现
 * 
 * @author Zoe
 * @date 2019年2月15日下午3:49:54
 */
@Service
public class MenusServiceImpl implements MenusService {

	@Autowired
	private MenusMapper menusMapper;

	@Autowired
	private JedisClient jedisClient;

	/**
	 * 查找所有数据
	 * 
	 * @author Zoe
	 * @date 2019年2月15日下午3:49:54
	 * @return
	 */
	@Override
	public List<Tree> findAll() {
		List<Tree> tree = menusMapper.findAll();
		return tree;
	}

	/**
	 * 根据菜单id查找菜单，显示菜单详情
	 * 
	 * @author Zoe
	 * @date 2019年2月15日下午8:16:22
	 * @param menuid
	 * @return
	 */
	@Override
	public List<Menus> findById(String menuid) {
		return menusMapper.findById(menuid);
	}

	/**
	 * 添加菜单[同时将已有的标签更新为父标签]
	 * 
	 * @author Zoe
	 * @date 2019年2月15日下午9:51:50
	 * @param menus 菜单对象
	 * @return
	 */
	@Override
	public ManagerResult insert(Menus menus) {
		// 设置默认添加的菜单的状态为使用中
		menus.setStatus(1);
		Integer insertCount = menusMapper.insert(menus);
		if (insertCount != null && insertCount > 0) {
			// 更新标签为父标签
			Menus m = new Menus();
			m.setMenuid(menus.getPid());
			m.setIs_parent(1);
			if (200 == updateById(m).getStatus()) {
				return new ManagerResult(200, "数据添加成功", null);
			} else {
				return new ManagerResult(400, "数据添加失败", null);
			}
		} else {
			return new ManagerResult(400, "数据添加失败", null);
		}
	}

	/**
	 * 根据id删除数据
	 * 
	 * @author Zoe
	 * @date 2019年2月15日下午9:51:50
	 * @param menuid 主键
	 * @return
	 */
	@Override
	public ManagerResult deleteById(String menuid) {
		// 0：已删除
		Integer deleteCount = menusMapper.deleteById(menuid, "0");
		if (deleteCount != null && deleteCount > 0) {
			return new ManagerResult(200, "数据删除成功", null);
		} else {
			return new ManagerResult(400, "数据删除失败", null);
		}
	}

	/**
	 * @author Zoe
	 * @date 2019年2月15日下午9:51:50
	 * @param menus
	 * @return
	 */
	@Override
	public ManagerResult updateById(Menus menus) {
		Integer updateCount = menusMapper.updateById(menus);
		if (updateCount != null && updateCount > 0) {
			return new ManagerResult(200, "数据修改成功", null);
		} else {
			return new ManagerResult(400, "数据修改失败", null);
		}
	}

	/**
	 * 复制menu
	 * 
	 * @param src
	 * @return
	 */
	private Menus cloneMenu(Menus src) {
		Menus menu = new Menus();
		menu.setIcon(src.getIcon());
		menu.setMenuid(src.getMenuid());
		menu.setMenuname(src.getMenuname());
		menu.setUrl(src.getUrl());
		menu.setMenus(new ArrayList<Menus>());
		return menu;
	}

	/**
	 * (non Javadoc)
	 * 
	 * @Title: findMenusByUserid
	 * @Description: TODO
	 * @param userid
	 * @return
	 * @see com.xlr.service.permission.MenusService#findMenusByUserid(java.lang.Integer)
	 */
	@Override
	public Menus findMenusByUserid(Integer userid) {
		// 从缓存中读取数据
		String easyuiMenusJson = jedisClient.get("easyuiMenus_" + userid);
		Menus menu = null;
		if (easyuiMenusJson == null) {
			// 获取根菜单
			List<Menus> root = menusMapper.selectMenus("-1");
			// 用户下的菜单集合
//			List<Menus> userMenus = menusMapper.selectMenusByUserid(userid);
			List<Menus> userMenus = findMenusListByUserid(userid);
			// 根菜单
			menu = cloneMenu(root.get(0));
			// 暂存一级菜单
			Menus _m1 = null;
			// 暂存二级菜单
			Menus _m2 = null;
			// 获取全部的一级菜单
			List<Menus> parentMenus = menusMapper.selectMenus("0");
			// 循环一级菜单
			for (Menus m1 : parentMenus) {
				_m1 = cloneMenu(m1);
				// 获取当前一级菜单的所有二级菜单
				List<Menus> leafMenus = menusMapper.selectMenus(_m1.getMenuid());
				// 循环匹配二级菜单
				for (Menus m2 : leafMenus) {
					for (Menus userMenu : userMenus) {
						if (userMenu.getMenuid().equals(m2.getMenuid())) {
							// 将二级菜单加入一级菜单
							_m2 = cloneMenu(m2);
							_m1.getMenus().add(_m2);
						}
					}
				}
				// 有二级菜单我们才加进来
				if (_m1.getMenus().size() > 0) {
					// 把一级菜单加入到根菜单下
					menu.getMenus().add(_m1);
				}
			}
			System.out.println("从数据库读取，设置缓存");
			jedisClient.set("easyuiMenus_" + userid, JSON.toJSONString(menu));
		} else {
			menu = JSON.parseObject(easyuiMenusJson, Menus.class);
			System.out.println("从缓存读取");
		}
		return menu;
	}

	/**
	 * (non Javadoc)
	 * 
	 * @Title: findMenusListByUserid
	 * @Description: TODO
	 * @param userid
	 * @return
	 * @see com.xlr.service.permission.MenusService#findMenusListByUserid(java.lang.Integer)
	 */
	@Override
	public List<Menus> findMenusListByUserid(Integer userid) {
		String menusListJson = jedisClient.get("menusList_" + userid);
		List<Menus> menusList = null;
		if (menusListJson == null) {
			// 1.从数据库中查出来，放入缓存中
			menusList = menusMapper.selectMenusByUserid(userid);
			jedisClient.set("menusList_" + userid, JSON.toJSONString(menusList));
			System.out.println("从数据库中查询menusList");
		} else {
			// 2.直接从缓存中拿
			menusList = JSON.parseArray(menusListJson, Menus.class);
			System.out.println("从缓存中查询menusList");
		}
		return menusList;
	}

}
