package com.xlr.serviceImpl.permission;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xlr.mapper.permission.MenusMapper;
import com.xlr.mapper.permission.RoleMapper;
import com.xlr.pojo.Menus;
import com.xlr.pojo.Role;
import com.xlr.service.permission.RoleService;
import com.xlr.utils.EasyUIDataGridResult;
import com.xlr.utils.EasyUIOptionalTreeNode;
import com.xlr.utils.ManagerResult;

/**
 * 
 * @ClassName: RoleServiceImpl
 * @Description: 处理角色相关业务
 * @author: gj
 * @date: 2019年2月14日 下午6:35:19
 */
@Service
public class RoleServiceImpl implements RoleService {
	@Autowired
	private RoleMapper roleMapper;
	@Autowired
	private MenusMapper menusMapper;
	@Autowired
	private JedisClient jedisClient;
	/**
	 * (non Javadoc)
	 * 
	 * @Title: findRoleByPage
	 * @Description: TODO
	 * @param page
	 * @param rows
	 * @param role
	 * @return
	 * @see com.xlr.service.permission.RoleService#findRoleByPage(java.lang.Integer,
	 *      java.lang.Integer, com.xlr.pojo.Role)
	 */
	@Override
	public EasyUIDataGridResult findRoleByPage(Integer page, Integer rows, Role role) {
		// 1.分页查询
		PageHelper.startPage(page, rows);
		List<Role> list = roleMapper.selectRoleByPage(role);
		PageInfo<Role> pageInfo = new PageInfo<>(list);
		// 2.封装EasyUIDataGridResult
		EasyUIDataGridResult result = new EasyUIDataGridResult();
		result.setTotal((int) pageInfo.getTotal());
		result.setRows(pageInfo.getList());
		// 3.返回分页的结果
		return result;
	}

	/**
	 * (non Javadoc)
	 * 
	 * @Title: updateRole
	 * @Description: TODO
	 * @param role
	 * @return
	 * @see com.xlr.service.permission.RoleService#updateRole(com.xlr.pojo.Role)
	 */
	@Override
	public ManagerResult updateRole(Role role) {
		try {
			if (role != null) {
				if (checkData(role)) {
					Integer row = roleMapper.updateRole(role);
					if (row > 0) {
						return new ManagerResult(200, "角色信息更新成功", null);
					}
				} else {
					return new ManagerResult(400, "信息异常", null);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ManagerResult(400, "角色信息更新失败", null);
	}

	/**
	 * (non Javadoc)
	 * 
	 * @Title: addRole
	 * @Description: TODO
	 * @param role
	 * @return
	 * @see com.xlr.service.permission.RoleService#addRole(com.xlr.pojo.Role)
	 */
	@Override
	public ManagerResult addRole(Role role) {
		if (role != null) {
			if (checkData(role)) {
				role.setCreate_data(new Date());
				Integer row = roleMapper.insertRole(role);
				if (row > 0) {
					return new ManagerResult(200, "角色信息添加成功", null);
				}
			} else {
				return new ManagerResult(400, "信息异常", null);
			}
		}
		return new ManagerResult(400, "角色信息添加失败", null);
	}

	/**
	 * 
	 * @Title: checkData
	 * @Description: 校验数据
	 * @param role
	 * @return boolean
	 * @author gj
	 * @date 2019年2月15日下午12:20:54
	 */
	private boolean checkData(Role role) {
		if (role.getEnble() >= 0 && role.getEnble() <= 1) {
			return true;
		}
		return false;
	}

	/**
	 * (non Javadoc)
	 * 
	 * @Title: findRoleByEnble
	 * @Description: TODO
	 * @return
	 * @see com.xlr.service.permission.RoleService#findRoleByEnble()
	 */
	@Override
	public EasyUIDataGridResult findRoleByEnble() {
		List<Role> list = roleMapper.selectRoleByEnble();
		EasyUIDataGridResult result = new EasyUIDataGridResult();
		result.setRows(list);
		result.setTotal(list.size());
		return result;
	}

	/**
	 * (non Javadoc)
	 * 
	 * @Title: findRoleMenu
	 * @Description: TODO
	 * @param roleuuid
	 * @return
	 * @see com.xlr.service.permission.RoleService#findRoleMenuByRoleid(java.lang.Integer)
	 */
	@Override
	public List<EasyUIOptionalTreeNode> findRoleMenuByRoleid(Integer roleuuid) {
		// 1.根据角色id获取角色对应的菜单id
		List<String> menuidList = roleMapper.selectRoleMenuidByRoleid(roleuuid);
		// 2.获取一级菜单
		List<Menus> Parentmenus = menusMapper.selectMenusIdName("0");
		// 3.当前角色对象对应的菜单权限
		List<EasyUIOptionalTreeNode> treeList = new ArrayList<EasyUIOptionalTreeNode>();
		// 暂存一级菜单
		EasyUIOptionalTreeNode t1 = null;
		// 暂存二级菜单
		EasyUIOptionalTreeNode t2 = null;
		// 一级菜单遍历
		for (Menus m1 : Parentmenus) {
			t1 = new EasyUIOptionalTreeNode();
			t1.setId(m1.getMenuid());
			t1.setText(m1.getMenuname());
			List<Menus> leafMenus = menusMapper.selectMenusIdName(m1.getMenuid());
			// 二级菜单遍历
			for (Menus m2  : leafMenus) {
				t2 = new EasyUIOptionalTreeNode();
				t2.setId(m2.getMenuid());
				t2.setText(m2.getMenuname());
				// 如果角色下包含有这个权限菜单，让它勾选上
				for (String menuid : menuidList) {
					if(m2.getMenuid().equals(menuid)) {
						t2.setChecked(true);
					}
				}
				t1.getChildren().add(t2);
			}
			treeList.add(t1);
		}
		return treeList;
	}
	/**
	 * (non Javadoc) 
	 * @Title: updateRoleMenus
	 * @Description: TODO
	 * @param roleuuid
	 * @param checkedIds
	 * @return 
	 * @see com.xlr.service.permission.RoleService#updateRoleMenus(java.lang.Integer, java.lang.String)
	 */
	@Override
	public ManagerResult updateRoleMenus(Integer roleuuid, String checkedIds) {
		try {
			//清空角色下的权限菜单
			roleMapper.deleteMenuidByRoleid(roleuuid);
			//权限角色id
			if(checkedIds != null) {
				String[] ids = checkedIds.split(",");
				for (String menuuuid : ids) {
					roleMapper.insertRoleMenus(menuuuid, roleuuid);
				}
			}
			List<Integer> useridList = roleMapper.selectUseridByRoleuuid(roleuuid);
			for (Integer userid : useridList) {
				jedisClient.del("easyuiMenus_"+userid);
				jedisClient.del("menusList_"+userid);
			}
			System.out.println("更新角色对应的对应的权限菜单 ，清除缓存");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ManagerResult.build(200, "权限设置成功");
	}

}
