package com.xlr.serviceImpl.baseDict;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xlr.mapper.baseDict.BaseDictMapper;
import com.xlr.pojo.BaseDict;
import com.xlr.service.baseDict.BaseDictervice;
import com.xlr.utils.EasyUIDataGridResult;
import com.xlr.utils.ManagerResult;

/**
 * 数据字典逻辑实现
 * @author Zoe
 * @date 2019年2月13日下午8:43:08
 */
@Service
public class BaseDictServiceImpl implements BaseDictervice {

	@Autowired
	private BaseDictMapper baseDictMapper;
	/**
	 * 查询所有数据_分页
	 * @author Zoe
	 * @date 2019年2月13日下午8:43:08
	 * @return
	 */
	@Override
	public EasyUIDataGridResult findAll(Integer page, Integer rows) {
		// 1.设置分页信息
		PageHelper.startPage(page, rows);
		// 2.执行查询
		List<BaseDict> list = baseDictMapper.findAll();
		// 3.封装pageInfo
		PageInfo<BaseDict> pageInfo = new PageInfo<>(list);
		// 4.封装EasyUIDataGridResult
		EasyUIDataGridResult result = new EasyUIDataGridResult();
		result.setTotal((int) pageInfo.getTotal());
		result.setRows(pageInfo.getList());
		// 5.返回分页的结果
		return result;
	}
	/**
	 * 添加数据
	 * @author Zoe
	 * @date 2019年2月13日下午10:30:17
	 * @param baseDict
	 * @return
	 */
	@Override
	public ManagerResult insert(BaseDict baseDict) {
		// 设置默认添加的数据字典的状态为使用中
		baseDict.setDict_enble("1");
		Integer insertCount = baseDictMapper.insert(baseDict);
		if (insertCount != null && insertCount > 0) {
			return new ManagerResult(200, "数据添加成功", null);
		} else {
			return new ManagerResult(400, "数据添加失败", null);
		}
	}
	/**
	 * 根据id删除数据
	 * @author Zoe
	 * @date 2019年2月14日上午10:13:12
	 * @param dict_id 主键id
	 * @return
	 */
	@Override
	public ManagerResult deleteById(Integer dict_id) {
		// 0：停用
		Integer deleteCount = baseDictMapper.deleteById(dict_id, "0");
		if (deleteCount != null && deleteCount > 0) {
			return new ManagerResult(200, "数据删除成功", null);
		} else {
			return new ManagerResult(400, "数据删除失败", null);
		}
	}
	/**
	 * 根据id修改数据
	 * @author Zoe
	 * @date 2019年2月14日下午12:01:40
	 * @param baseDict 数据字典对象
	 * @return
	 */
	@Override
	public ManagerResult updateById(BaseDict baseDict) {
		Integer updateCount = baseDictMapper.updateById(baseDict);
		if (updateCount != null && updateCount > 0) {
			return new ManagerResult(200, "数据修改成功", null);
		} else {
			return new ManagerResult(400, "数据修改失败", null);
		}
	}
	/**
	 * 根据id查找数据
	 * @author Zoe
	 * @date 2019年2月14日下午1:01:29
	 * @param dict_id 主键id
	 * @return
	 */
	@Override
	public BaseDict findById(Integer dict_id) {
		return baseDictMapper.findById(dict_id);
	}
	/**
	 * @author Zoe
	 * @date 2019年2月14日下午3:11:14
	 * @return
	 */
	@Override
	public List<BaseDict> findTypeName() {
		return baseDictMapper.findTypeName();
	}
	/**
	 * 多条件查询
	 * @author Zoe
	 * @date 2019年2月14日下午3:32:19
	 * @param baseDict 数据字典对象
	 * @return
	 */
	@Override
	public EasyUIDataGridResult findByCriteria(BaseDict baseDict, Integer page, Integer rows) {
		// 默认查询使用中的数据
		if (null == baseDict.getDict_enble()) {
			baseDict.setDict_enble("1");
		}
		// 1.设置分页信息
		PageHelper.startPage(page, rows);
		// 2.执行查询
		List<BaseDict> list = baseDictMapper.findByCriteria(baseDict);
		// 3.封装pageInfo
		PageInfo<BaseDict> pageInfo = new PageInfo<>(list);
		// 4.封装EasyUIDataGridResult
		EasyUIDataGridResult result = new EasyUIDataGridResult();
		result.setTotal((int) pageInfo.getTotal());
		result.setRows(pageInfo.getList());
		// 5.返回分页的结果
		return result;
	}

}
