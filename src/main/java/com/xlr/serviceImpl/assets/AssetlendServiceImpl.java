/**
 * @author 高俊
 * @date 2018年12月3日下午7:56:48
 * @version 1.0
 * @company 河南喜乐融商贸有限公司
 * @Copyright: 2018 www.xlr.com Inc. All rights reserved. 
 */
package com.xlr.serviceImpl.assets;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xlr.mapper.assets.AssetlendMapper;
import com.xlr.pojo.CompanyUsedAssets;
import com.xlr.service.assets.AssetlendService;
import com.xlr.utils.EasyUIDataGridResult;

/**
 * @author 高俊
 * @date 2018年12月3日下午7:56:48
 */
@Service
public class AssetlendServiceImpl implements AssetlendService {
	@Autowired
	private AssetlendMapper assetlendMapper;

	@Override
	public EasyUIDataGridResult findAllCompanyUsedAssets(Integer page, Integer rows) {
		// 1.设置分页信息
		PageHelper.startPage(page, rows);
		// 2.执行查询
		List<CompanyUsedAssets> list = assetlendMapper.selectAllCompanyUsedAssets();
		// 3.封装pageInfo
		PageInfo<CompanyUsedAssets> pageInfo = new PageInfo<>(list);
		// 4.封装EasyUIDataGridResult
		EasyUIDataGridResult result = new EasyUIDataGridResult();
		result.setTotal((int) pageInfo.getTotal());
		result.setRows(pageInfo.getList());
		// 5.返回分页的结果
		return result;
	}

}
