package com.xlr.serviceImpl.assets;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xlr.mapper.assets.PublicAssetsMapper;
import com.xlr.pojo.CompanyUsedAssets;
import com.xlr.pojo.CountsVo;
import com.xlr.pojo.PublicAssets;
import com.xlr.service.assets.PublicAssetsService;
import com.xlr.utils.EasyUIDataGridResult;
import com.xlr.utils.ManagerResult;
@Service
public class PublicAssetsServiceImpl implements PublicAssetsService {

	@Autowired
	private PublicAssetsMapper publicAssetsMapper;
	@Override
	public EasyUIDataGridResult getAllAssets(Integer page, Integer rows) {
		EasyUIDataGridResult easyUIDataGridResult=new EasyUIDataGridResult();
		// 1.设置分页信息
		PageHelper.startPage(page, rows);
		// 2.执行查询
		List<PublicAssets> assets = publicAssetsMapper.selectAllAssets();
		
		if(assets.size()>0&&assets!=null) {
			for(int i=0;i<assets.size();i++) {
				if(assets.get(i).getAssetsRemainingNumber()==null) {
					Integer number = assets.get(i).getAssetsTotalNumber();
					assets.get(i).setAssetsRemainingNumber(number);
				}
			}
		}
		// 3.封装pageInfo
		PageInfo<PublicAssets> pageInfo = new PageInfo<PublicAssets>(assets);
		// 4.封装EasyUIDataGridResult
		EasyUIDataGridResult result = new EasyUIDataGridResult();
		result.setTotal((int) pageInfo.getTotal());
		result.setRows(pageInfo.getList());
		// 5.返回分页的结果
		easyUIDataGridResult.setTotal(result.getTotal());
		easyUIDataGridResult.setRows(result.getRows());
		return easyUIDataGridResult;
	}
	@Override
	public ManagerResult addAssets(PublicAssets publicAssets) {
		Integer update = null;
		if(publicAssets!=null) {
			if(publicAssets.getAssetsSequence()!=null) {
				String assetsSequence=publicAssets.getAssetsSequence();
				CountsVo countsVo = publicAssetsMapper.selectAssetsBySequence(assetsSequence);
				//公共资产中已经存在该资产，只需要在原来的基础上添加数量
				if(countsVo!=null && countsVo.getCounts()>0) {
					publicAssets.setAssetsId(countsVo.getId());
					publicAssets.setAssetsTotalNumber(publicAssets.getAssetsTotalNumber()+countsVo.getTotalNumber());
					publicAssets.setAssetsRemainingNumber(publicAssets.getAssetsRemainingNumber()+countsVo.getOldNumber());
					update = publicAssetsMapper.updateAssets(publicAssets);
					if(update>0)
					return ManagerResult.ok("该项资产已存在，并且数量已经添加完毕!");
				}
				//该项资产不存在与数据库，则将该项资产添加到数据库
				else {
					update = publicAssetsMapper.insertAssets(publicAssets);
					if(update>0)
					return ManagerResult.ok("资产添加成功!");
				}
			}
		}
		return ManagerResult.build(400, "添加失败!");
	}
	@Override
	public ManagerResult updateAssets(PublicAssets publicAssets) {
		if(publicAssets!=null) {
			Integer update = publicAssetsMapper.updateAssets(publicAssets);
			if(update>0) {
				return ManagerResult.ok("资产信息修改成功!");
			}
		}
		return ManagerResult.build(400, "修改失败!");
	}
	@Override
	public ManagerResult deleteAssets(Integer assetsId) {
		if(assetsId!=null) {
			Integer delete = publicAssetsMapper.deleteAssets(assetsId);
			if(delete>0) {
				return ManagerResult.ok("该资产已经删除!");
			}
		}
		return ManagerResult.build(400, "删除失败!");
	}
}
