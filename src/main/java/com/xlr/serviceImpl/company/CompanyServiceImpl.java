/**
 * @author 高俊
 * @date 2018年11月8日下午8:53:25
 * @version 1.0
 * @company 河南喜乐融商贸有限公司
 * @Copyright: 2018 www.xlr.com Inc. All rights reserved. 
 */
package com.xlr.serviceImpl.company;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xlr.mapper.company.CompanyMapper;
import com.xlr.mapper.company.OverviewMapper;
import com.xlr.pojo.Company;
import com.xlr.pojo.Overview;
import com.xlr.service.company.CompanyService;
import com.xlr.utils.EasyUIDataGridResult;
import com.xlr.utils.ManagerResult;

/**
 * @author 高俊
 * @date 2018年11月8日下午8:53:25
 */
@Service
public class CompanyServiceImpl implements CompanyService {

	@Autowired
	private CompanyMapper companyMapper;
	@Autowired
	private OverviewMapper overviewmapper;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.xlr.service.CompanyService#findAllCompany()
	 */
	@Override
	public EasyUIDataGridResult findAllCnameByPage(Integer page, Integer rows) {
		// 1.设置分页信息
		PageHelper.startPage(page, rows);
		// 2.执行查询
		List<Company> list = companyMapper.selectAllCompany();
		// 3.封装pageInfo
		PageInfo<Company> pageInfo = new PageInfo<>(list);
		// 4.封装EasyUIDataGridResult
		EasyUIDataGridResult result = new EasyUIDataGridResult();
		result.setTotal((int) pageInfo.getTotal());
		result.setRows(pageInfo.getList());
		// 5.返回分页的结果
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.xlr.service.company.CompanyService#updateCompanyImage(com.xlr.pojo.
	 * Company)
	 */
	@Override
	public ManagerResult updateCompanyImage(Company company) {
		Integer row = companyMapper.updateCompanyImage(company);
		if (row > 0) {
			return ManagerResult.build(200, "营业执照上传成功",
					company.getCompany_image());
		} else {
			return ManagerResult.build(400, "营业执照上传错误");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.xlr.service.company.CompanyService#findCompanyImageId(java.lang.Integer
	 * )
	 */
	@Override
	public ManagerResult findCompanyImageId(Integer user_companyid) {
		Company company = companyMapper.findCompanyImageId(user_companyid);
		if (company == null) {
			return ManagerResult.build(400, "未上传营业执照");
		}
		String company_image = company.getCompany_image();
		if (company_image.equals("")) {
			return ManagerResult.build(400, "未上传营业执照");
		}
		return ManagerResult.build(200, "营业执照", company_image);
	}

	/**
	 * @author 徐培珊
	 * @date 2018年11月27日下午2:12:18
	 * @param company
	 * @return
	 */
	@Override
	public ManagerResult updateCompanyInfo(Company company) {
		Integer row = companyMapper.updateCompany(company);
		if (row > 0) {
			return ManagerResult.build(200, "公司信息更新成功");
		} else {
			return ManagerResult.build(400, "公司信息更新失败");
		}
	}

	/**
	 * @author 徐培珊
	 * @date 2018年11月27日下午4:32:02
	 * @param company_id
	 * @return
	 */
	@Override
	public ManagerResult deleteCompanyInfoById(Integer company_id) {
		// 资产编号为空
		if (null == company_id) {
			return ManagerResult.build(400, "删除失败, 资产编号为空");
		}
		Integer i = companyMapper.deleteCompanyByCompanyId(company_id);
		if (i > 0) {
			return ManagerResult.build(200, "删除成功");
		}
		return ManagerResult.build(400, "删除失败");
	}

	/**
	 * @author 徐培珊
	 * @date 2018年11月27日下午5:31:33
	 * @param company
	 * @return
	 */
	@Override
	public ManagerResult addCompanyInfo(Company company) {
		if (company == null) {
			return ManagerResult.build(400, "添加公司信息失败");
		}
		Integer i = companyMapper.insertCompany(company);
		if (i > 0) {
			return ManagerResult.build(200, "添加公司信息成功");
		}
		return ManagerResult.build(400, "添加公司信息失败");
	}

	/**
	 * 无脑查询所有
	 */
	@Override
	public List<Company> findAllCompany() {
		return companyMapper.selectAllCompany();
	}

	/**
	 * @author 徐培珊
	 * @date 2018年12月5日下午8:52:50
	 * @param user_code
	 * @param page
	 * @param rows
	 * @return
	 */
	@Override
	public EasyUIDataGridResult findAllCurrentCompanyByPage(String user_code,
			Integer page, Integer rows) {
		// 1.设置分页信息
		PageHelper.startPage(page, rows);
		// 2.执行查询
		List<Company> list = companyMapper
				.selectAllCompanyByUserCode(user_code);
		// 3.封装pageInfo
		PageInfo<Company> pageInfo = new PageInfo<>(list);
		// 4.封装EasyUIDataGridResult
		EasyUIDataGridResult result = new EasyUIDataGridResult();
		result.setTotal((int) pageInfo.getTotal());
		result.setRows(pageInfo.getList());
		// 5.返回分页的结果
		return result;
	}

	/**
	 * @author 徐培珊
	 * @date 2018年12月6日上午11:48:11
	 * @param hatchType
	 * @param page
	 * @param rows
	 * @return
	 */
	@Override
	public EasyUIDataGridResult findAllCompanyByPage(Company company,
			Integer page, Integer rows) {
		// 1.设置分页信息
		if (company == null) {
			company.setBeginDate(null);
			company.setEndDate(null);
			company.setCompany_name(null);
		}
		if ("".equals(company.getCompany_name())) {
			company.setCompany_name(null);
		}
		if ("".equals(company.getEndDate())) {
			company.setEndDate(null);
		}
		if ("".equals(company.getBeginDate())) {
			company.setBeginDate(null);
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if (company.getBeginDate() != null
				&& !"".equals(company.getBeginDate())) {
			company.setBeginTime(sdf.format(company.getBeginDate()));
		}
		if (company.getEndDate() != null && !"".equals(company.getEndDate())) {
			company.setEndTime(sdf.format(company.getEndDate()));
		}
		System.out.println(company.getCompany_name());
		PageHelper.startPage(page, rows);
		// 2.执行查询
		List<Company> list = companyMapper.selectAllCompanyByHatchType(
				company.getCompany_hatchtype(), company.getCompany_name(),
				company.getBeginTime(), company.getEndTime());
		// 3.封装pageInfo
		PageInfo<Company> pageInfo = new PageInfo<>(list);
		// 4.封装EasyUIDataGridResult
		EasyUIDataGridResult result = new EasyUIDataGridResult();
		result.setTotal((int) pageInfo.getTotal());
		result.setRows(pageInfo.getList());
		// 5.返回分页的结果
		return result;
	}
	
	/**
	 * 根据公司id查询其项目概况
	 * @author 李贺鹏
	 * @date 2019年2月23日上午9:15:36
	 * @param companyid
	 * @return
	 */
	public EasyUIDataGridResult findOverviewByCompanyid(Integer o_companyid) {
		//1根据公司id查询 overview
		Overview overview=overviewmapper.findByCompanyid(o_companyid);
		
		List<Overview> list =new ArrayList<Overview>();
		EasyUIDataGridResult result = new EasyUIDataGridResult();
		result.setTotal(1);
		//2.判断是否为空
		if(null==overview) {
			//3.为空：实例化一个空的overview对象 并给其赋值companyid
			Overview newOverview=new Overview();
			newOverview.setO_companyid(o_companyid);
			overviewmapper.insertOverview(o_companyid);
			list.add(newOverview);
			result.setRows(list);
			return result;
		}else {
			list.add(overview);
			result.setRows(list);
			return result;
		}	
	}
	@Override
	public ManagerResult update(Overview overview) {
		System.out.println(overview.toString());
		int i = overviewmapper.updateOverview(overview);
		if (i == 0) {
			return ManagerResult.build(400, "修改失败");
		} else {
			return ManagerResult.build(200, "修改成功");
		}
	}



}
