/**
 * @author 徐培珊
 * @date 2018年12月1日下午8:33:34
 * @version 1.0
 * @company 河南喜乐融商贸有限公司
 * @Copyright: 2018 www.xlr.com Inc. All rights reserved. 
 */
package com.xlr.serviceImpl.company;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xlr.mapper.company.ProtocolMapper;
import com.xlr.pojo.Company;
import com.xlr.pojo.Protocol;
import com.xlr.service.company.ProtocolService;
import com.xlr.utils.EasyUIDataGridResult;
import com.xlr.utils.ManagerResult;

/**
 * @author 徐培珊
 * @date 2018年12月1日下午8:33:34
 */
@Service
public class ProtocolServiceImpl implements ProtocolService {

	@Autowired
	private ProtocolMapper protocolMapper;

	/**
	 * @author 徐培珊
	 * @date 2018年12月1日下午8:44:36
	 * @param companyId
	 * @return
	 */
	@Override
	public ManagerResult selectProtocolByCompanyId(Integer companyId) {
		Protocol protocol = protocolMapper.selectProtocolInfoByCompanyId(companyId);
		if (protocol != null) {
			return ManagerResult.build(400, "", protocol);
		} else {
			return ManagerResult.build(200, "协议信息不存在");
		}
	}

	/**
	 * @author 徐培珊
	 * @date 2018年12月1日下午8:44:36
	 * @param protocol
	 * @return
	 */
	@Override
	public ManagerResult updateProtocolByCompanyId(Protocol protocol) {
		/*
		 * 1. 根据companyId查询协议是否存在 2. 如果存在，调用更新方法 3. 如果不存在，调用插入方法
		 */
		Integer integer = null;
		Protocol id = protocolMapper.selectProtocolInfoByCompanyId(protocol.getA_companyid());
		if (id != null) {
			integer = protocolMapper.updateProtocolInfo(protocol);
		} else {
			integer = protocolMapper.insertProtocolInfo(protocol);
		}
		if (integer > 0) {
			return ManagerResult.build(400, "协议更新成功");
		} else {
			return ManagerResult.build(200, "协议更新失败");
		}
	}

	/**
	 * 入驻协议统计_查询所有协议
	 * 
	 * @author 徐培珊
	 * @date 2018年12月2日上午11:15:11
	 * @return
	 */
	@Override
	public EasyUIDataGridResult selectAllProtocol(Integer page, Integer rows) {
		// 1.设置分页信息
		PageHelper.startPage(page, rows);
		// 2.执行查询
		List<Protocol> list = protocolMapper.selectAllProtocol();
		// 3.封装pageInfo
		PageInfo<Protocol> pageInfo = new PageInfo<>(list);
		// 4.封装EasyUIDataGridResult
		EasyUIDataGridResult result = new EasyUIDataGridResult();
		result.setTotal((int) pageInfo.getTotal());
		result.setRows(pageInfo.getList());
		// 5.返回分页的结果
		return result;
	}
}
