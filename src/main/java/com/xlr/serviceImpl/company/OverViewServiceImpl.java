package com.xlr.serviceImpl.company;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xlr.mapper.company.OverviewMapper;
import com.xlr.pojo.OverViewAll;
import com.xlr.pojo.Overview;
import com.xlr.service.company.OverViewService;
import com.xlr.utils.EasyUIDataGridResult;
import com.xlr.utils.ManagerResult;

/**
 * @author Zoe
 * @date 2019年2月28日下午8:11:06
 */
@Service("OverViewService")
public class OverViewServiceImpl implements OverViewService {

	@Autowired
	private OverviewMapper overviewMapper;
	/**
	 * 根据公司id查找所有表格数据
	 * @author Zoe
	 * @date 2019年2月28日下午8:11:44
	 * @param companyId 公司id
	 * @return
	 */
	public OverViewAll findAllTable(Integer companyId) {
		return overviewMapper.findAllTable(companyId);
	}
	/**
	 * 添加项目概况
	 * @author Zoe
	 * @date 2019年3月1日上午10:38:11
	 * @param overview 项目概况对象
	 * @return
	 */
	public ManagerResult insert(Overview overview, String[] o_type) {
		Integer insert = overviewMapper.insert(overview);
		if (null != insert) {
			Integer insertViewDict = null;
			// 添加多次数据
			for (int i = 0; i < o_type.length; i++) {
				insertViewDict = overviewMapper.insertViewDict(overview.getO_id(), Integer.valueOf(o_type[i]));
			}
			if (null != insertViewDict && insertViewDict > 0) {
				return new ManagerResult(200, "数据插入成功", null);
			} else {
				return new ManagerResult(400, "数据插入失败", null);
			}
		} else {
			return new ManagerResult(400, "数据插入失败", null);
		}
	}
	/**
	 * 分页查找所有项目概况
	 * @author Zoe
	 * @date 2019年3月1日下午2:39:36
	 * @param page 页数
	 * @param rows 每页记录数
	 * @return
	 */
	@Override
	public EasyUIDataGridResult listByPage(Integer page, Integer rows) {
		// 1.设置分页信息
		PageHelper.startPage(page, rows);
		// 2.执行查询
		List<Overview> list = overviewMapper.listByPage();
		// 3.封装pageInfo
		PageInfo<Overview> pageInfo = new PageInfo<>(list);
		// 4.封装EasyUIDataGridResult
		EasyUIDataGridResult result = new EasyUIDataGridResult();
		result.setTotal((int) pageInfo.getTotal());
		result.setRows(pageInfo.getList());
		// 5.返回分页的结果
		return result;
	}
	/**
	 * 更新项目概况
	 * @author Zoe
	 * @date 2019年3月1日下午2:49:57
	 * @param overview 项目概况对象
	 * @param o_type 项目概况所属类型
	 * @return
	 */
	@Override
	public ManagerResult update(Overview overview, String[] o_type) {
		Integer update = overviewMapper.update(overview);
		if (null != update) {
			// 根据oid删除中间表中原有的数据
			overviewMapper.deleteViewDict(overview.getO_id());
			Integer insertViewDict = null;
			// 添加多次数据
			for (int i = 0; i < o_type.length; i++) {
				insertViewDict = overviewMapper.insertViewDict(overview.getO_id(), Integer.valueOf(o_type[i]));
			}
			if (null != insertViewDict && insertViewDict > 0) {
				return new ManagerResult(200, "数据修改成功", null);
			} else {
				return new ManagerResult(400, "数据修改失败", null);
			}
		} else {
			return new ManagerResult(400, "数据修改失败", null);
		}
	}

}
