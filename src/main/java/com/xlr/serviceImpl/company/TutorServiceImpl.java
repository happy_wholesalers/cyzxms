package com.xlr.serviceImpl.company;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlr.mapper.company.TutorMapper;
import com.xlr.pojo.Tutor;
import com.xlr.service.company.TutorService;
import com.xlr.utils.EasyUIDataGridResult;
import com.xlr.utils.ManagerResult;

@Service
public class TutorServiceImpl implements TutorService {

	@Autowired
	private TutorMapper tutorMapper;

	@Override
	public EasyUIDataGridResult findByID(Integer t_companyid) {
		System.out.println(t_companyid);
		Tutor tutor = tutorMapper.findById(t_companyid);
		List<Tutor> list =new ArrayList<Tutor>();
		EasyUIDataGridResult result = new EasyUIDataGridResult();
		result.setTotal(1);
		if(tutor ==null){
			tutorMapper.CreateOne(t_companyid);
			Tutor tutor1 = new Tutor();
			tutor1.setT_companyid(t_companyid);
			list.add(tutor1);
			result.setRows(list);
			return result;
		}else{
			list.add(tutor);
			result.setRows(list);
			return result;
		}
	}
	@Override
	public ManagerResult update(Tutor tutor) {
		System.out.println(tutor.toString());
		int i = tutorMapper.updateByid(tutor);
		if (i == 0) {
			return ManagerResult.build(400, "修改失败");
		} else {
			return ManagerResult.build(200, "修改成功");
		}
	}

}
