package com.xlr.serviceImpl.company;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xlr.mapper.company.EmployeeMapper;
import com.xlr.pojo.Company;
import com.xlr.pojo.Employee;
import com.xlr.service.company.EmployeeService;
import com.xlr.utils.EasyUIDataGridResult;
import com.xlr.utils.ManagerResult;

/**
 * @author 王宏志
 * 	公司职员的信息管理
 * @date 2018年12月4日下午6:07:06
 * @TOOD 
 */
@Service("employeeService")
public class EmployeeServiceImpl implements EmployeeService{

	@Autowired
	private EmployeeMapper employeeMapper;
	@Override
	public ManagerResult add(Employee employee) {
		Integer add = null;
		if(employee!=null) {
			add = employeeMapper.insertEmployee(employee);
		}
		if(add>0) {
			return ManagerResult.ok("添加成功");
		}
		return ManagerResult.build(400, "添加失败", "请输入具体信息");
	}

	@Override
	public ManagerResult delete(Integer employeeId) {
		Integer delete = null;
		if(null!=employeeId) {
			delete = employeeMapper.deleteEmplyeeByID(employeeId);
		}
		if(delete>0) {
			return ManagerResult.ok("删除成功");
		}
		return ManagerResult.build(400, "删除失败");
	}

	@Override
	public ManagerResult update(Employee employee) {
		Integer update = null;
		if(null!=employee) {
			update = employeeMapper.updateEmplyee(employee);
		}
		if(update>0) {
			return ManagerResult.ok("修改成功");
		}
		return ManagerResult.build(400, "修改失败");
	}

	@Override
	public EasyUIDataGridResult get(Integer e_cid, Integer page, Integer rows) {
		EasyUIDataGridResult result = null;
		if(null!=e_cid) {
			PageHelper.startPage(page, rows);
			List<Employee> employees = employeeMapper.selectEmployeeByCompanyID(e_cid);
			PageInfo<Employee> pageInfo = new PageInfo<>(employees);
			result = new EasyUIDataGridResult();
			result.setTotal((int) pageInfo.getTotal());
			result.setRows(pageInfo.getList());
		}
		return result;
	}

}
