package com.xlr.serviceImpl.company;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlr.mapper.company.AssetsMapper;
import com.xlr.pojo.AssetsNameAndSeq;
import com.xlr.service.company.AssetsService;

/**
 * @author 王宏志
 * 	资产的Service
 * @date 2018年12月6日下午2:58:04
 * @TOOD 
 */
@Service("assetsService")
public class AssetsServiceImpl implements AssetsService {

	@Autowired
	private AssetsMapper assetsMapper;
	
	@Override
	public List<AssetsNameAndSeq> getAssetsNameAndSeq() {
		return assetsMapper.getAssetsNameAndSequence();
	}

	@Override
	public String getAssertNameBySeq(String seq) {
		return assetsMapper.getAssetsNameBySequence(seq);
	}

}
