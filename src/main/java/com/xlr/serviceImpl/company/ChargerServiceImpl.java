package com.xlr.serviceImpl.company;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlr.mapper.company.ChargerMapper;
import com.xlr.mapper.company.CompanyMapper;
import com.xlr.mapper.company.CollegeMapper;
import com.xlr.pojo.Charger;
import com.xlr.pojo.College;
import com.xlr.pojo.Company;
import com.xlr.service.company.ChargerService;
import com.xlr.utils.EasyUIDataGridResult;
import com.xlr.utils.ManagerResult;

/**
 * @author 王宏志 入驻项目负责人Service层实现
 * @date 2018年12月2日上午11:00:41
 * @TOOD
 */
@Service
public class ChargerServiceImpl implements ChargerService {

	@Autowired
	private ChargerMapper ChargerMapper;
	@Autowired
	private CompanyMapper CompanyMapper;
	@Autowired
	private CollegeMapper CollegeMapper;

	@Override
	public EasyUIDataGridResult showChargerByUserId(Integer userId,String usercode) {
		List<Charger> list = new ArrayList<Charger>();
		Charger charger = ChargerMapper.selectUserinfo(userId);
		Company company = new Company();
		company = CompanyMapper.selectCompanyById(charger.getUser_companyid());
		charger.setCompany(company);
		list.add(charger);
		College college = CollegeMapper.findByid(charger.getUser_facilityid());
		charger.setCollege(college);
		EasyUIDataGridResult result = new EasyUIDataGridResult();
		result.setRows(list);
		result.setTotal(1);
		return result;
	}

	@Override
	public ManagerResult updateCharger(Charger charger) {
		Integer i = ChargerMapper.updateUserinfo(charger);
		if (i > 0) {
			return ManagerResult.ok("修改成功");
		}
		return ManagerResult.build(400, "修改失败");
	}

}
