package com.xlr.serviceImpl.company;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlr.mapper.company.CollegeMapper;
import com.xlr.pojo.College;
import com.xlr.service.company.CollegeService;

@Service("collegeService")
public class CollegeServiceImpl implements CollegeService{

	@Autowired
	private CollegeMapper collegeMapper;
	@Override
	public List<College> get() {
		return collegeMapper.selectAllCollege();
	}

}
