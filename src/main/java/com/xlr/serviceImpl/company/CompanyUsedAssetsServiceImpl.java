/**
 * @author 徐培珊
 * @date 2018年11月13日下午8:10:33
 * @version 1.0
 * @company 河南喜乐融商贸有限公司
 * @Copyright: 2018 www.xlr.com Inc. All rights reserved. 
 */
package com.xlr.serviceImpl.company;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xlr.mapper.company.AssetsMapper;
import com.xlr.mapper.company.CompanyUsedAssetsMapper;
import com.xlr.pojo.CompanyUsedAssets;
import com.xlr.pojo.Userinfo;
import com.xlr.service.company.CompanyUsedAssetsService;
import com.xlr.utils.EasyUIDataGridResult;
import com.xlr.utils.ManagerResult;

/**
 * @author 徐培珊
 * @date 2018年11月13日下午8:10:33
 */
@Service
public class CompanyUsedAssetsServiceImpl implements CompanyUsedAssetsService{

	@Autowired
	private CompanyUsedAssetsMapper companyUsedAssetsMapper;
	@Autowired
	private AssetsMapper assetsMapper;
	
	/**
	 * @author 徐培珊
	 * @date 2018年11月13日下午8:11:06
	 * @param companyUsedAssets
	 * @return
	 */
	@Override
	public ManagerResult addCompanyAssets(CompanyUsedAssets companyUsedAssets) {
		
		if(null != companyUsedAssets) {
			if(null !=companyUsedAssets.getItemSequence()) {
				String sequence = companyUsedAssets.getItemSequence();
				String name = assetsMapper.getAssetsNameBySequence(sequence);
				companyUsedAssets.setItemName(name);
			}
		Integer i = companyUsedAssetsMapper.insertAssets(companyUsedAssets);
		if(i > 0) {
			return ManagerResult.build(200,"添加公司资产成功");
		}
		}
		return ManagerResult.build(400,"添加公司资产失败");
	}

	/**
	 * @author 徐培珊
	 * @date 2018年11月13日下午8:11:06
	 * @param itemId
	 * @return
	 */
	@Override
	public ManagerResult deleteCompanyAssetsByID(Integer itemId) {
		//资产编号为空
		if(null==itemId) {
			return ManagerResult.build(400, "删除失败","资产编号为空");
		}
		Integer i = companyUsedAssetsMapper.deleteAssetsByID(itemId);
		if(i > 0) {
			return ManagerResult.build(200, "删除成功");
		}
		return ManagerResult.build(400, "删除失败");
	}

	/**
	 * @author 徐培珊
	 * @date 2018年11月13日下午8:11:06
	 * @param companyUsedAssets
	 * @return
	 */
	@Override
	public ManagerResult updateCompanyAssets(CompanyUsedAssets companyUsedAssets) {
		if(null==companyUsedAssets) {
			return ManagerResult.build(400,"修改失败","数据为空");
		}
		Integer i = companyUsedAssetsMapper.updateAssets(companyUsedAssets);
		if(i > 0) {
			return ManagerResult.build(200,"修改成功");
		}
		return ManagerResult.build(400,"修改失败");
	}

	/**
	 * @author 徐培珊
	 * @date 2018年11月13日下午8:11:06
	 * @param itemId
	 * @return
	 */
	@Override
	public CompanyUsedAssets getCompanyAssets(Integer itemId) {
		
		return companyUsedAssetsMapper.selectCompanyAssessts(itemId);
	}

	/**
	 * @author 徐培珊
	 * @date 2018年11月13日下午8:58:17
	 * @param companyId
	 * @return
	 */
	@Override
	public EasyUIDataGridResult selectAllCompanyAssertsByCompanyId(@Param("companyId") Integer companyId, Integer page, Integer rows){
		EasyUIDataGridResult easyUIDataGridResult=new EasyUIDataGridResult();
		//没有公司ID
		if(null==companyId) {
			return easyUIDataGridResult;
		}
		// 1.设置分页信息
		PageHelper.startPage(page, rows);
		// 2.执行查询
		List<CompanyUsedAssets> list = companyUsedAssetsMapper.selectAllCompanyAssertsByCompanyId(companyId);
		// 3.封装pageInfo
		PageInfo<CompanyUsedAssets> pageInfo = new PageInfo<CompanyUsedAssets>(list);
		// 4.封装EasyUIDataGridResult
		EasyUIDataGridResult result = new EasyUIDataGridResult();
		result.setTotal((int) pageInfo.getTotal());
		result.setRows(pageInfo.getList());
		// 5.返回分页的结果
		easyUIDataGridResult.setTotal(result.getTotal());
		easyUIDataGridResult.setRows(result.getRows());
		return easyUIDataGridResult;
	}

}
