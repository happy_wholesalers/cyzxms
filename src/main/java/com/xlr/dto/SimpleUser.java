package com.xlr.dto;

/**
 * 
 * @ClassName: SimpleUser
 * @Description: 用于重置密码时，展示的简单用户信息
 * @author: 高俊
 * @date: 2019年2月14日 下午3:20:49
 */
public class SimpleUser {
	private Integer user_id;
	private String user_code;
	private String user_name;
	private String user_sno;
	private String user_major;
	public Integer getUser_id() {
		return user_id;
	}
	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}
	public String getUser_code() {
		return user_code;
	}
	public void setUser_code(String user_code) {
		this.user_code = user_code;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getUser_sno() {
		return user_sno;
	}
	public void setUser_sno(String user_sno) {
		this.user_sno = user_sno;
	}
	public String getUser_major() {
		return user_major;
	}
	public void setUser_major(String user_major) {
		this.user_major = user_major;
	}
	@Override
	public String toString() {
		return "SimpleUser [user_id=" + user_id + ", user_code=" + user_code + ", user_name=" + user_name
				+ ", user_sno=" + user_sno + ", user_major=" + user_major + "]";
	}
	
}
