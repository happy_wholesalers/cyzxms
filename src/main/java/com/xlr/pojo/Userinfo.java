/**
 * @author 徐培珊
 * @date 2018年11月1日下午7:18:53
 * @version 1.0
 * @company 河南喜乐融商贸有限公司
 * @Copyright: 2018 www.xlr.com Inc. All rights reserved. 
 */
package com.xlr.pojo;

import java.io.Serializable;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author 徐培珊
 * @date 2018年11月1日下午7:18:53
 */
@JsonIgnoreProperties(value = { "handler" })
public class Userinfo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String user_name;
	private Integer user_id;
	private String user_code;
	private String user_pwd;
	private String user_type;
	private String user_tel;
	private String user_wx;
	private String user_sno;
	private String user_nation;
	private String user_card;
	private String user_email;
	private String user_familyphone;
	private String user_politics;
	private String user_sex;
	private Integer user_age;
	private String user_image;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date user_enterSchool;
	private String user_major;
	private String user_homeaddr;
	private String user_education;
	private Date user_graduation;
	private String user_educationBackground;
	private String user_experience;
	private String user_rewardsPunishment;
	private Integer user_facilityid;
	private Integer user_companyid;
	private Integer user_teamid;
	private Company company;
	private College college;
	private Date beginDate;
	private Date endDate;
	private String beginTime;
	private String endTime;
	
	
	public String getUser_education() {
		return user_education;
	}
	public void setUser_education(String user_education) {
		this.user_education = user_education;
	}
	public Integer getUser_teamid() {
		return user_teamid;
	}
	public void setUser_teamid(Integer user_teamid) {
		this.user_teamid = user_teamid;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd")
	public Date getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd")
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the user_enterSchool
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd")
	public Date getUser_enterSchool() {
		return user_enterSchool;
	}

	/**
	 * @param user_enterSchool the user_enterSchool to set
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd")
	public void setUser_enterSchool(Date user_enterSchool) {
		this.user_enterSchool = user_enterSchool;
	}

	public Integer getUser_id() {
		return user_id;
	}

	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}

	public String getUser_code() {
		return user_code;
	}

	public void setUser_code(String user_code) {
		this.user_code = user_code;
	}

	public String getUser_pwd() {
		return user_pwd;
	}

	public void setUser_pwd(String user_pwd) {
		this.user_pwd = user_pwd;
	}

	public String getUser_type() {
		return user_type;
	}

	public void setUser_type(String user_type) {
		this.user_type = user_type;
	}

	public String getUser_tel() {
		return user_tel;
	}

	public void setUser_tel(String user_tel) {
		this.user_tel = user_tel;
	}

	public String getUser_wx() {
		return user_wx;
	}

	public void setUser_wx(String user_wx) {
		this.user_wx = user_wx;
	}

	public String getUser_sno() {
		return user_sno;
	}

	public void setUser_sno(String user_sno) {
		this.user_sno = user_sno;
	}

	public String getUser_nation() {
		return user_nation;
	}

	public void setUser_nation(String user_nation) {
		this.user_nation = user_nation;
	}

	public String getUser_card() {
		return user_card;
	}

	public void setUser_card(String user_card) {
		this.user_card = user_card;
	}

	public String getUser_email() {
		return user_email;
	}

	public void setUser_email(String user_email) {
		this.user_email = user_email;
	}

	public String getUser_familyphone() {
		return user_familyphone;
	}

	public void setUser_familyphone(String user_familyphone) {
		this.user_familyphone = user_familyphone;
	}

	public String getUser_politics() {
		return user_politics;
	}

	public void setUser_politics(String user_politics) {
		this.user_politics = user_politics;
	}

	public String getUser_sex() {
		return user_sex;
	}

	public void setUser_sex(String user_sex) {
		this.user_sex = user_sex;
	}

	public Integer getUser_age() {
		return user_age;
	}

	public void setUser_age(Integer user_age) {
		this.user_age = user_age;
	}

	public String getUser_image() {
		return user_image;
	}

	public void setUser_image(String user_image) {
		this.user_image = user_image;
	}

	public String getUser_major() {
		return user_major;
	}

	public void setUser_major(String user_major) {
		this.user_major = user_major;
	}

	public String getUser_homeaddr() {
		return user_homeaddr;
	}

	public void setUser_homeaddr(String user_homeaddr) {
		this.user_homeaddr = user_homeaddr;
	}

	public Integer getUser_facilityid() {
		return user_facilityid;
	}

	public void setUser_facilityid(Integer user_facilityid) {
		this.user_facilityid = user_facilityid;
	}

	public Integer getUser_companyid() {
		return user_companyid;
	}

	public void setUser_companyid(Integer user_companyid) {
		this.user_companyid = user_companyid;
	}

	public Date getUser_graduation() {
		return user_graduation;
	}

	public void setUser_graduation(Date user_graduation) {
		this.user_graduation = user_graduation;
	}

	public String getUser_educationBackground() {
		return user_educationBackground;
	}

	public void setUser_educationBackground(String user_educationBackground) {
		this.user_educationBackground = user_educationBackground;
	}

	public String getUser_experience() {
		return user_experience;
	}

	public void setUser_experience(String user_experience) {
		this.user_experience = user_experience;
	}

	public String getUser_rewardsPunishment() {
		return user_rewardsPunishment;
	}

	public void setUser_rewardsPunishment(String user_rewardsPunishment) {
		this.user_rewardsPunishment = user_rewardsPunishment;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public College getCollege() {
		return college;
	}

	public void setCollege(College college) {
		this.college = college;
	}

	/**
	 * @author Zoe
	 * @date 2019年2月28日下午7:13:08
	 * @return
	 */
	@Override
	public String toString() {
		return "Userinfo [user_name=" + user_name + ", user_id=" + user_id + ", user_code=" + user_code + ", user_pwd="
				+ user_pwd + ", user_type=" + user_type + ", user_tel=" + user_tel + ", user_wx=" + user_wx
				+ ", user_sno=" + user_sno + ", user_nation=" + user_nation + ", user_card=" + user_card
				+ ", user_email=" + user_email + ", user_familyphone=" + user_familyphone + ", user_politics="
				+ user_politics + ", user_sex=" + user_sex + ", user_age=" + user_age + ", user_image=" + user_image
				+ ", user_enterSchool=" + user_enterSchool + ", user_major=" + user_major + ", user_homeaddr="
				+ user_homeaddr + ", user_education=" + user_education + ", user_graduation=" + user_graduation
				+ ", user_educationBackground=" + user_educationBackground + ", user_experience=" + user_experience
				+ ", user_rewardsPunishment=" + user_rewardsPunishment + ", user_facilityid=" + user_facilityid
				+ ", user_companyid=" + user_companyid + ", user_teamid=" + user_teamid + ", company=" + company
				+ ", college=" + college + ", beginDate=" + beginDate + ", endDate=" + endDate + ", beginTime="
				+ beginTime + ", endTime=" + endTime + "]";
	}

	

	

}
