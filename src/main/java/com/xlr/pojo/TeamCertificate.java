/**
 * @author 徐培珊
 * @date 2018年12月11日下午7:35:28
 * @version 1.0
 * @company 河南喜乐融商贸有限公司
 * @Copyright: 2018 www.xlr.com Inc. All rights reserved. 
 */
package com.xlr.pojo;

/**
 * 	公司证书实体类
 * @author 徐培珊
 * @date 2018年12月11日下午7:35:28
 */
public class TeamCertificate {
	private Integer certificate_id;
	private String certificate_path;
	private Integer t_id;
	/**
	 * @return the certificate_id
	 */
	public Integer getCertificate_id() {
		return certificate_id;
	}
	/**
	 * @param certificate_id the certificate_id to set
	 */
	public void setCertificate_id(Integer certificate_id) {
		this.certificate_id = certificate_id;
	}
	/**
	 * @return the certificate_path
	 */
	public String getCertificate_path() {
		return certificate_path;
	}
	/**
	 * @param certificate_path the certificate_path to set
	 */
	public void setCertificate_path(String certificate_path) {
		this.certificate_path = certificate_path;
	}
	/**
	 * @return the t_id
	 */
	public Integer getT_id() {
		return t_id;
	}
	/**
	 * @param t_id the t_id to set
	 */
	public void setT_id(Integer t_id) {
		this.t_id = t_id;
	}
	/**
	 * @author 徐培珊
	 * @date 2018年12月11日下午7:38:19
	 * @return
	 */
	@Override
	public String toString() {
		return "CompanyCertificate [certificate_id=" + certificate_id + ", certificate_path=" + certificate_path
				+ ", t_id=" + t_id + "]";
	}
}
