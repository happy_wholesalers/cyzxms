/**
 * @author 徐培珊
 * @date 2018年11月1日下午7:28:14
 * @version 1.0
 * @company 河南喜乐融商贸有限公司
 * @Copyright: 2018 www.xlr.com Inc. All rights reserved. 
 */
package com.xlr.pojo;

import java.io.Serializable;

/**
 * @author 徐培珊
 * @date 2018年11月1日下午7:28:14
 */

public class Userpermission implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 0（超级管理员） 1（院系负责人） 2（公司负责人） 3（普通管理员）
	 */
	private Integer role_id;
	private String ru_ty_id;
	private String rp_menuid;
	
	private String menuname;

	/**
	 * @return the role_id
	 */
	public synchronized Integer getRole_id() {
		return role_id;
	}

	/**
	 * @param role_id the role_id to set
	 */
	public synchronized void setRole_id(Integer role_id) {
		this.role_id = role_id;
	}

	/**
	 * @return the ru_ty_id
	 */
	public synchronized String getRu_ty_id() {
		return ru_ty_id;
	}

	/**
	 * @param ru_ty_id the ru_ty_id to set
	 */
	public synchronized void setRu_ty_id(String ru_ty_id) {
		this.ru_ty_id = ru_ty_id;
	}

	/**
	 * @return the rp_menuid
	 */
	public synchronized String getRp_menuid() {
		return rp_menuid;
	}

	/**
	 * @param rp_menuid the rp_menuid to set
	 */
	public synchronized void setRp_menuid(String rp_menuid) {
		this.rp_menuid = rp_menuid;
	}

	/**
	 * @return the menuname
	 */
	public synchronized String getMenuname() {
		return menuname;
	}

	/**
	 * @param menuname the menuname to set
	 */
	public synchronized void setMenuname(String menuname) {
		this.menuname = menuname;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Userpermission [role_id=" + role_id + ", ru_ty_id=" + ru_ty_id + ", rp_menuid=" + rp_menuid
				+ ", menuname=" + menuname + "]";
	}

}
