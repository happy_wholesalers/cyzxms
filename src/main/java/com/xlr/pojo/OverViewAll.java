package com.xlr.pojo;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 入驻申请所有表格的数据展示pojo
 * @author Zoe
 * @date 2019年2月28日下午4:01:19
 */
@JsonIgnoreProperties(value={"hibernateLazyInitializer","handler"})
public class OverViewAll implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private Userinfo userInfo;// 入驻项目负责人简历
	private Overview overView;// 项目概况
	private Tutor tutor;// 创业导师
	private List<Employee> employeeList;// 入驻企业团队成员
	@Override
	public String toString() {
		return "OverViewAll [userInfo=" + userInfo + ", overView=" + overView + ", tutor=" + tutor + ", employeeList=" + employeeList
				+ "]";
	}
	public Userinfo getUserInfo() {
		return userInfo;
	}
	public void setUserInfo(Userinfo userInfo) {
		this.userInfo = userInfo;
	}
	public Overview getOverView() {
		return overView;
	}
	public void setOverView(Overview overView) {
		this.overView = overView;
	}
	public Tutor getTutor() {
		return tutor;
	}
	public void setTutor(Tutor tutor) {
		this.tutor = tutor;
	}
	public List<Employee> getEmployeeList() {
		return employeeList;
	}
	public void setEmployeeList(List<Employee> employeeList) {
		this.employeeList = employeeList;
	}

}
