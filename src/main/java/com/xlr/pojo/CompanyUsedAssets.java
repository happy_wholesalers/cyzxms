/**
 * @author 徐培珊
 * @date 2018年11月12日下午8:43:32
 * @version 1.0
 * @company 河南喜乐融商贸有限公司
 * @Copyright: 2018 www.xlr.com Inc. All rights reserved. 
 */
package com.xlr.pojo;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 公司资产管理
 * @author 徐培珊
 * @date 2018年11月12日下午8:43:32
 */
public class CompanyUsedAssets {
	private Integer itemId;
	private String itemName;
	private String itemMeasurement;
	private Integer itemNumber;
	private Double itemPrice;
	private String itemObjectName;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date itemBuyDate;
	private Integer companyId;
	private String itemUser;
	private String itemSequence;
	/**
	 * @return the itemId
	 */
	public Integer getItemId() {
		return itemId;
	}
	/**
	 * @param itemId the itemId to set
	 */
	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}
	/**
	 * @return the itemName
	 */
	public String getItemName() {
		return itemName;
	}
	/**
	 * @param itemName the itemName to set
	 */
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	/**
	 * @return the itemMeasurement
	 */
	public String getItemMeasurement() {
		return itemMeasurement;
	}
	/**
	 * @param itemMeasurement the itemMeasurement to set
	 */
	public void setItemMeasurement(String itemMeasurement) {
		this.itemMeasurement = itemMeasurement;
	}
	/**
	 * @return the itemNumber
	 */
	public Integer getItemNumber() {
		return itemNumber;
	}
	/**
	 * @param itemNumber the itemNumber to set
	 */
	public void setItemNumber(Integer itemNumber) {
		this.itemNumber = itemNumber;
	}
	/**
	 * @return the itemPrice
	 */
	public Double getItemPrice() {
		return itemPrice;
	}
	/**
	 * @param itemPrice the itemPrice to set
	 */
	public void setItemPrice(Double itemPrice) {
		this.itemPrice = itemPrice;
	}
	/**
	 * @return the itemObjectName
	 */
	public String getItemObjectName() {
		return itemObjectName;
	}
	/**
	 * @param itemObjectName the itemObjectName to set
	 */
	public void setItemObjectName(String itemObjectName) {
		this.itemObjectName = itemObjectName;
	}
	/**
	 * @return the itemBuyDate
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd")
	public Date getItemBuyDate() {
		return itemBuyDate;
	}
	/**
	 * @param itemBuyDate the itemBuyDate to set
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd")
	public void setItemBuyDate(Date itemBuyDate) {
		this.itemBuyDate = itemBuyDate;
	}
	/**
	 * @return the companyId
	 */
	
	/**
	 * @return the itemUser
	 */
	public String getItemUser() {
		return itemUser;
	}
	/**
	 * @return the companyId
	 */
	public Integer getCompanyId() {
		return companyId;
	}
	/**
	 * @param companyId the companyId to set
	 */
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	/**
	 * @param itemUser the itemUser to set
	 */
	public void setItemUser(String itemUser) {
		this.itemUser = itemUser;
	}
	/**
	 * @return the itemSequence
	 */
	public String getItemSequence() {
		return itemSequence;
	}
	/**
	 * @param itemSequence the itemSequence to set
	 */
	public void setItemSequence(String itemSequence) {
		this.itemSequence = itemSequence;
	}
	/**
	 * @author 徐培珊
	 * @date 2018年11月13日下午7:59:08
	 * @return
	 */
	@Override
	public String toString() {
		return "CompanyUsedAssets [itemId=" + itemId + ", itemName=" + itemName + ", itemMeasurement=" + itemMeasurement
				+ ", itemNumber=" + itemNumber + ", itemPrice=" + itemPrice + ", itemObjectName=" + itemObjectName
				+ ", itemBuyDate=" + itemBuyDate + ", companyId=" + companyId + ", itemUser=" + itemUser
				+ ", itemSequence=" + itemSequence + "]";
	}
	
}
