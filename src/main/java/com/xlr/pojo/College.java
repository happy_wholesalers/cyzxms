package com.xlr.pojo;

import java.io.Serializable;

public class College implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int college_id;
	private String college_name;
	private String college_intro;
	private String college_tel;
	private String college_location;
	private String college_img;

	/**
	 * 获取院系编号
	 */
	public int getCollege_id() {
		return college_id;
	}

	/**
	 * 设置院系编号
	 */
	public void setCollege_id(int value) {
		this.college_id = value;
	}

	/**
	 * 获取院系名称
	 */
	public String getCollege_name() {
		return college_name;
	}

	/**
	 * 设置院系名称
	 */
	public void setCollege_name(String value) {
		this.college_name = value;
	}

	/**
	 * 获取院系简介
	 */
	public String getCollege_intro() {
		return college_intro;
	}

	/**
	 * 设置院系简介
	 */
	public void setCollege_intro(String value) {
		this.college_intro = value;
	}

	/**
	 * 获取院系联系电话
	 */
	public String getCollege_tel() {
		return college_tel;
	}

	/**
	 * 设置院系联系电话
	 */
	public void setCollege_tel(String value) {
		this.college_tel = value;
	}

	/**
	 * 获取院系位置
	 */
	public String getCollege_location() {
		return college_location;
	}

	/**
	 * 设置院系位置
	 */
	public void setCollege_location(String value) {
		this.college_location = value;
	}

	/**
	 * 获取院系图片
	 */
	public String getCollege_img() {
		return college_img;
	}

	/**
	 * 设置院系图片
	 */
	public void setCollege_img(String value) {
		this.college_img = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "college [college_id=" + college_id + ", college_name=" + college_name + ", college_intro="
				+ college_intro + ", college_tel=" + college_tel + ", college_location=" + college_location
				+ ", college_img=" + college_img + "]";
	}

}
