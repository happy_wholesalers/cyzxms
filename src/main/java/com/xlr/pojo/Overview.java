/**
 * @author 李贺鹏
 * @date 2019年2月21日下午3:25:39
 * @version 1.0
 * @company 河南喜乐融商贸有限公司
 */
package com.xlr.pojo;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 项目概况
 * @author 李贺鹏
 * @date 2019年2月21日下午3:25:39
 */
@JsonIgnoreProperties(value={"hibernateLazyInitializer","handler"})
public class Overview implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer o_id;
	//项目简介
	private String o_introduction;
	//运行状态进度
	private String o_schedule;
	//市场预测
	private String o_forecast;
	//所属类型
//	private String[] o_type;
	private List<String> o_typeName;// 所属类型名称
	/*是否工商注册
	 * 1:已注册
	 * 0：未注册
	 * */
	private Integer o_register;
	//申请经营范围
	private String o_scope;
	//项目主要优势
	private String o_advant;
	//所属公司id
	private Integer o_companyid;
	public List<String> getO_typeName() {
		return o_typeName;
	}
	public void setO_typeName(List<String> o_typeName) {
		this.o_typeName = o_typeName;
	}
	/**
	 * @return the o_id
	 */
	public Integer getO_id() {
		return o_id;
	}
	/**
	 * @param o_id the o_id to set
	 */
	public void setO_id(Integer o_id) {
		this.o_id = o_id;
	}
	/**
	 * @return the o_introduction
	 */
	public String getO_introduction() {
		return o_introduction;
	}
	/**
	 * @param o_introduction the o_introduction to set
	 */
	public void setO_introduction(String o_introduction) {
		this.o_introduction = o_introduction;
	}
	/**
	 * @return the o_schedule
	 */
	public String getO_schedule() {
		return o_schedule;
	}
	/**
	 * @param o_schedule the o_schedule to set
	 */
	public void setO_schedule(String o_schedule) {
		this.o_schedule = o_schedule;
	}
	/**
	 * @return the o_forecast
	 */
	public String getO_forecast() {
		return o_forecast;
	}
	/**
	 * @param o_forecast the o_forecast to set
	 */
	public void setO_forecast(String o_forecast) {
		this.o_forecast = o_forecast;
	}
	/**
	 * @return the o_type
	 */
//	public String[] getO_type() {
//		return o_type;
//	}
	/**
	 * @param o_type the o_type to set
	 */
//	public void setO_type(String[] o_type) {
//		this.o_type = o_type;
//	}
	/**
	 * @return the o_register
	 */
	public Integer getO_register() {
		return o_register;
	}
	/**
	 * @param o_register the o_register to set
	 */
	public void setO_register(Integer o_register) {
		this.o_register = o_register;
	}
	/**
	 * @return the o_scope
	 */
	public String getO_scope() {
		return o_scope;
	}
	/**
	 * @param o_scope the o_scope to set
	 */
	public void setO_scope(String o_scope) {
		this.o_scope = o_scope;
	}
	/**
	 * @return the o_advant
	 */
	public String getO_advant() {
		return o_advant;
	}
	/**
	 * @param o_advant the o_advant to set
	 */
	public void setO_advant(String o_advant) {
		this.o_advant = o_advant;
	}
	/**
	 * @return the o_companyid
	 */
	public Integer getO_companyid() {
		return o_companyid;
	}
	/**
	 * @param o_companyid the o_companyid to set
	 */
	public void setO_companyid(Integer o_companyid) {
		this.o_companyid = o_companyid;
	}
	/**
	 * 
	 */
	public Overview() {
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param o_id
	 * @param o_introduction
	 * @param o_schedule
	 * @param o_forecast
	 * @param o_type
	 * @param o_register
	 * @param o_scope
	 * @param o_advant
	 * @param o_companyid
	 */ 
//	String[] o_type,
	public Overview(Integer o_id, String o_introduction, String o_schedule, String o_forecast,
			Integer o_register, String o_scope, String o_advant, Integer o_companyid) {
		super();
		this.o_id = o_id;
		this.o_introduction = o_introduction;
		this.o_schedule = o_schedule;
		this.o_forecast = o_forecast;
//		this.o_type = o_type;
		this.o_register = o_register;
		this.o_scope = o_scope;
		this.o_advant = o_advant;
		this.o_companyid = o_companyid;
	}
	/**
	 * @author Zoe
	 * @date 2019年2月28日下午7:21:55
	 * @return
	 */
	@Override
	public String toString() {
		return "Overview [o_id=" + o_id + ", o_introduction=" + o_introduction + ", o_schedule=" + o_schedule
				+ ", o_forecast=" + o_forecast  + ", o_typeName=" + o_typeName
				+ ", o_register=" + o_register + ", o_scope=" + o_scope + ", o_advant=" + o_advant + ", o_companyid="
				+ o_companyid + "]";
	}
	

}
