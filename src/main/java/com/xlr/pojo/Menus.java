/**
 * @author 徐培珊
 * @date 2018年11月1日下午7:15:55
 * @version 1.0
 * @company 河南喜乐融商贸有限公司
 * @Copyright: 2018 www.xlr.com Inc. All rights reserved. 
 */
package com.xlr.pojo;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * @author 徐培珊
 * @date 2018年11月1日下午7:15:55
 */
public class Menus implements Serializable{
	private static final long serialVersionUID = 1L;
	private String menuid;// 编号
	private String menuname;// 名称
	private String url;// 对应URL
	private String icon;// 图标样式
	private String pid;// 上一级菜单编号
	private Integer status;// 状态。可选值:1(正常),0(删除)
	private Integer is_parent;// 该菜单是否为父菜单，1为true，0为false
	private List<Menus> menus =new LinkedList<Menus>();
	
	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getIs_parent() {
		return is_parent;
	}
	public void setIs_parent(Integer is_parent) {
		this.is_parent = is_parent;
	}
	/**
	 * @return the menuid
	 */
	public String getMenuid() {
		return menuid;
	}
	/**
	 * @param menuid the menuid to set
	 */
	public void setMenuid(String menuid) {
		this.menuid = menuid;
	}
	/**
	 * @return the menuname
	 */
	public String getMenuname() {
		return menuname;
	}
	/**
	 * @param menuname the menuname to set
	 */
	public void setMenuname(String menuname) {
		this.menuname = menuname;
	}
	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	/**
	 * @return the icon
	 */
	public String getIcon() {
		return icon;
	}
	/**
	 * @param icon the icon to set
	 */
	public void setIcon(String icon) {
		this.icon = icon;
	}
	/**
	 * @return the menus
	 */
	public List<Menus> getMenus() {
		return menus;
	}
	/**
	 * @param menus the menus to set
	 */
	public void setMenus(List<Menus> menus) {
		this.menus = menus;
	}
	@Override
	public String toString() {
		return "Menus [menuid=" + menuid + ", menuname=" + menuname + ", url=" + url + ", icon=" + icon + ", pid=" + pid
				+ ", status=" + status + ", is_parent=" + is_parent + ", menus=" + menus + "]";
	}
}
