package com.xlr.pojo;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

/**   
 * @ClassName:  PublicAssets   
 * @Description:公共资产POJO  
 * @author: 王宏志
 * @date:   2018年11月26日 下午8:38:00   
 *     
 */ 
public class PublicAssets {
	//资产ID
	private Integer assetsId;
	//物品名称
	private String assetsName;
	//计量单位
	private String assetsMeasurement;
	//总数量
	private Integer assetsTotalNumber;
	//剩余数量
	private Integer assetsRemainingNumber;
	//单价
	private Double assetsPrice;
	//仪器名称
	private String assetsObjectName;
	//购买时间
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date assetsBuyDate;
	//编号
	private String assetsSequence;
	
	public Integer getAssetsId() {
		return assetsId;
	}
	public void setAssetsId(Integer assetsId) {
		this.assetsId = assetsId;
	}
	public String getAssetsName() {
		return assetsName;
	}
	public void setAssetsName(String assetsName) {
		this.assetsName = assetsName;
	}
	public String getAssetsMeasurement() {
		return assetsMeasurement;
	}
	public void setAssetsMeasurement(String assetsMeasurement) {
		this.assetsMeasurement = assetsMeasurement;
	}
	
	
	public Integer getAssetsRemainingNumber() {
		return assetsRemainingNumber;
	}
	public void setAssetsRemainingNumber(Integer assetsRemainingNumber) {
		this.assetsRemainingNumber = assetsRemainingNumber;
	}
	public Integer getAssetsTotalNumber() {
		return assetsTotalNumber;
	}
	public void setAssetsTotalNumber(Integer assetsTotalNumber) {
		this.assetsTotalNumber = assetsTotalNumber;
	}
	public Double getAssetsPrice() {
		return assetsPrice;
	}
	public void setAssetsPrice(Double assetsPrice) {
		this.assetsPrice = assetsPrice;
	}
	public String getAssetsObjectName() {
		return assetsObjectName;
	}
	public void setAssetsObjectName(String assetsObjectName) {
		this.assetsObjectName = assetsObjectName;
	}
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd")
	public Date getAssetsBuyDate() {
		return assetsBuyDate;
	}
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd")
	public void setAssetsBuyDate(Date assetsBuyDate) {
		this.assetsBuyDate = assetsBuyDate;
	}
	public String getAssetsSequence() {
		return assetsSequence;
	}
	public void setAssetsSequence(String assetsSequence) {
		this.assetsSequence = assetsSequence;
	}
	@Override
	public String toString() {
		return "PublicAssets [assetsId=" + assetsId + ", assetsName=" + assetsName + ", assetsMeasurement="
				+ assetsMeasurement + ", assetsTotalNumber=" + assetsTotalNumber + ", assetsRemainingNumber="
				+ assetsRemainingNumber + ", assetsPrice=" + assetsPrice + ", assetsObjectName=" + assetsObjectName
				+ ", assetsBuyDate=" + assetsBuyDate + ", assetsSequence=" + assetsSequence + "]";
	}
	
	
}
