package com.xlr.pojo;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * @author 王宏志 孵化团队的POJO
 * @date 2018年12月11日下午3:07:20
 * @TOOD
 */
public class Team {

	// 标号
	private Integer t_id;
	// 团队名称
	private String t_name;
	// 团队负责人
	private String t_leader;
	// 团队人数
	private Integer t_number;
	// 入孵时间
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date t_enterdate;
	// 科技领域
	private String t_field;
	// 知识产权情况
	private String t_knowledge;
	// 联系人
	private String t_collectionPerson;
	// 联系电话
	private String t_tel;
	// 院系
	private Integer t_dep;

	private Date beginDate;
	private Date endDate;
	private String beginTime;
	private String endTime;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd")
	public Date getBeginDate() {
		return beginDate;
	}
	
	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd")
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getT_name() {
		return t_name;
	}

	public void setT_name(String t_name) {
		this.t_name = t_name;
	}

	public Integer getT_id() {
		return t_id;
	}

	public void setT_id(Integer t_id) {
		this.t_id = t_id;
	}

	public String getT_leader() {
		return t_leader;
	}

	public void setT_leader(String t_leader) {
		this.t_leader = t_leader;
	}

	public Integer getT_number() {
		return t_number;
	}

	public void setT_number(Integer t_number) {
		this.t_number = t_number;
	}

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd")
	public Date getT_enterdate() {
		return t_enterdate;
	}

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd")
	public void setT_enterdate(Date t_enterdate) {
		this.t_enterdate = t_enterdate;
	}

	public String getT_field() {
		return t_field;
	}

	public void setT_field(String t_field) {
		this.t_field = t_field;
	}

	public String getT_knowledge() {
		return t_knowledge;
	}

	public void setT_knowledge(String t_knowledge) {
		this.t_knowledge = t_knowledge;
	}

	public String getT_collectionPerson() {
		return t_collectionPerson;
	}

	public void setT_collectionPerson(String t_collectionPerson) {
		this.t_collectionPerson = t_collectionPerson;
	}

	public String getT_tel() {
		return t_tel;
	}

	public void setT_tel(String t_tel) {
		this.t_tel = t_tel;
	}

	public Integer getT_dep() {
		return t_dep;
	}

	public void setT_dep(Integer t_dep) {
		this.t_dep = t_dep;
	}

}
