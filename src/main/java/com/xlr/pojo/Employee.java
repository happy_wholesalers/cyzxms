/**
 * @author 石腾飞
 * @date 2018年11月4日上午11:39:22
 * @version 1.0
 * @company 河南喜乐融商贸有限公司
 * @Copyright: 2018 www.xlr.com Inc. All rights reserved. 
 */
package com.xlr.pojo;

import java.io.Serializable;

/**
 * @author 石腾飞
 * @date 2018年11月4日上午11:39:22
 */
public class Employee implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer e_id;
	private String e_name;
	private String e_sex;
	private Integer e_fid;
	private String e_major;
	private String e_tel;
	private Integer e_cid;
	private Integer e_uid;
	private String e_position;
	private String college_name;// 院系名称
	

	public String getCollege_name() {
		return college_name;
	}



	public void setCollege_name(String college_name) {
		this.college_name = college_name;
	}



	public Integer getE_id() {
		return e_id;
	}



	public void setE_id(Integer e_id) {
		this.e_id = e_id;
	}



	public String getE_name() {
		return e_name;
	}



	public void setE_name(String e_name) {
		this.e_name = e_name;
	}



	public String getE_sex() {
		return e_sex;
	}



	public void setE_sex(String e_sex) {
		this.e_sex = e_sex;
	}



	public Integer getE_fid() {
		return e_fid;
	}



	public void setE_fid(Integer e_fid) {
		this.e_fid = e_fid;
	}



	public String getE_major() {
		return e_major;
	}



	public void setE_major(String e_major) {
		this.e_major = e_major;
	}



	public String getE_tel() {
		return e_tel;
	}



	public void setE_tel(String e_tel) {
		this.e_tel = e_tel;
	}



	public Integer getE_cid() {
		return e_cid;
	}



	public void setE_cid(Integer e_cid) {
		this.e_cid = e_cid;
	}



	public Integer getE_uid() {
		return e_uid;
	}



	public void setE_uid(Integer e_uid) {
		this.e_uid = e_uid;
	}



	public String getE_position() {
		return e_position;
	}



	public void setE_position(String e_position) {
		this.e_position = e_position;
	}



	/**
	 * @author Zoe
	 * @date 2019年2月28日下午4:27:25
	 * @return
	 */
	@Override
	public String toString() {
		return "Employee [e_id=" + e_id + ", e_name=" + e_name + ", e_sex=" + e_sex + ", e_fid=" + e_fid + ", e_major="
				+ e_major + ", e_tel=" + e_tel + ", e_cid=" + e_cid + ", e_uid=" + e_uid + ", e_position=" + e_position
				+ ", college_name=" + college_name + "]";
	}

}
