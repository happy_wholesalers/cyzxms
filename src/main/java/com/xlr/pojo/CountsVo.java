package com.xlr.pojo;

/**
 * @author 王宏志
 * 瞬时态的Vo,用于返回该项资产是否存在，并输出总数量与剩余数量。
 * @date 2018年11月28日下午3:08:21
 * @TOOD 
 */
public class CountsVo {

	private Integer id;
	//如果counts大于0则该项资产已经存在
	private Integer counts;
	//该项资产总数量
	private Integer totalNumber;
	//该项资产剩余
	private Integer oldNumber;
	public Integer getCounts() {
		return counts;
	}
	public void setCounts(Integer counts) {
		this.counts = counts;
	}
	public Integer getTotalNumber() {
		return totalNumber;
	}
	public void setTotalNumber(Integer totalNumber) {
		this.totalNumber = totalNumber;
	}
	public Integer getOldNumber() {
		return oldNumber;
	}
	public void setOldNumber(Integer oldNumber) {
		this.oldNumber = oldNumber;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Override
	public String toString() {
		return "CountsVo [id=" + id + ", counts=" + counts + ", totalNumber=" + totalNumber + ", oldNumber=" + oldNumber
				+ "]";
	}
	
	
}
