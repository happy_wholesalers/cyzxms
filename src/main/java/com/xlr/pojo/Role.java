package com.xlr.pojo;

import java.util.Date;

/**
 * 角色，本质上是一个用户组
 * @ClassName: Role 
 * @Description: TODO
 * @author: 高俊
 * @date: 2019年2月14日 下午6:23:33
 */
public class Role {
	//编号
	private Integer uuid;
	//名称
	private String name;
	//1：启用 2：禁用
	private Integer enble;
	
	private String create_name;
	private Date create_data;
	public Integer getUuid() {
		return uuid;
	}
	public void setUuid(Integer uuid) {
		this.uuid = uuid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getEnble() {
		return enble;
	}
	public void setEnble(Integer enble) {
		this.enble = enble;
	}
	public String getCreate_name() {
		return create_name;
	}
	public void setCreate_name(String create_name) {
		this.create_name = create_name;
	}
	public Date getCreate_data() {
		return create_data;
	}
	public void setCreate_data(Date create_data) {
		this.create_data = create_data;
	}
	@Override
	public String toString() {
		return "Role [uuid=" + uuid + ", name=" + name + ", enble=" + enble + ", create_name=" + create_name
				+ ", create_data=" + create_data + "]";
	}
	
}
