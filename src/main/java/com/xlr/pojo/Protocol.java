/**
 * @author 徐培珊
 * @date 2018年12月1日下午5:49:46
 * @version 1.0
 * @company 河南喜乐融商贸有限公司
 * @Copyright: 2018 www.xlr.com Inc. All rights reserved. 
 */
package com.xlr.pojo;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author 徐培珊
 * @date 2018年12月1日下午5:49:46
 */
@JsonIgnoreProperties(value = { "handler" })
public class Protocol {

	private Integer a_id;
	private Integer a_companyid;// 乙方公司id
	private String a_b_companyname;// 乙方公司名字
	private String a_b_address;// 乙方公司地址
	private String a_b_contact;// 乙方联系人
	private String a_b_tel;// 乙方联系人电话
	private String a_a_companyname;// 甲方公司名字
	private String a_a_address;// 甲方公司地址
	private String a_a_contact;// 甲方联系人
	private String a_a_tel;// 甲方联系人电话
	private Integer a_workstation;// 乙方申请工位数量
	private Integer a_b_training;// 乙方提供的实训岗位数量
	private Integer a_b_employ;// 乙方提供的就业岗位数量
	private Integer a_b_project;// 乙方提供的创业项目数量
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date a_term_begin;// 协议生效日期
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date a_term_end;// 协议结束日期
	private String a_a_represent;// 甲方代表签字
	private String a_b_represent;// 乙方代表签字
	/**
	 * @return the a_id
	 */
	public Integer getA_id() {
		return a_id;
	}
	/**
	 * @param a_id the a_id to set
	 */
	public void setA_id(Integer a_id) {
		this.a_id = a_id;
	}
	/**
	 * @return the a_companyid
	 */
	public Integer getA_companyid() {
		return a_companyid;
	}
	/**
	 * @param a_companyid the a_companyid to set
	 */
	public void setA_companyid(Integer a_companyid) {
		this.a_companyid = a_companyid;
	}
	/**
	 * @return the a_b_companyname
	 */
	public String getA_b_companyname() {
		return a_b_companyname;
	}
	/**
	 * @param a_b_companyname the a_b_companyname to set
	 */
	public void setA_b_companyname(String a_b_companyname) {
		this.a_b_companyname = a_b_companyname;
	}
	/**
	 * @return the a_b_address
	 */
	public String getA_b_address() {
		return a_b_address;
	}
	/**
	 * @param a_b_address the a_b_address to set
	 */
	public void setA_b_address(String a_b_address) {
		this.a_b_address = a_b_address;
	}
	/**
	 * @return the a_b_contact
	 */
	public String getA_b_contact() {
		return a_b_contact;
	}
	/**
	 * @param a_b_contact the a_b_contact to set
	 */
	public void setA_b_contact(String a_b_contact) {
		this.a_b_contact = a_b_contact;
	}
	/**
	 * @return the a_b_tel
	 */
	public String getA_b_tel() {
		return a_b_tel;
	}
	/**
	 * @param a_b_tel the a_b_tel to set
	 */
	public void setA_b_tel(String a_b_tel) {
		this.a_b_tel = a_b_tel;
	}
	/**
	 * @return the a_a_companyname
	 */
	public String getA_a_companyname() {
		return a_a_companyname;
	}
	/**
	 * @param a_a_companyname the a_a_companyname to set
	 */
	public void setA_a_companyname(String a_a_companyname) {
		this.a_a_companyname = a_a_companyname;
	}
	/**
	 * @return the a_a_address
	 */
	public String getA_a_address() {
		return a_a_address;
	}
	/**
	 * @param a_a_address the a_a_address to set
	 */
	public void setA_a_address(String a_a_address) {
		this.a_a_address = a_a_address;
	}
	/**
	 * @return the a_a_contact
	 */
	public String getA_a_contact() {
		return a_a_contact;
	}
	/**
	 * @param a_a_contact the a_a_contact to set
	 */
	public void setA_a_contact(String a_a_contact) {
		this.a_a_contact = a_a_contact;
	}
	/**
	 * @return the a_a_tel
	 */
	public String getA_a_tel() {
		return a_a_tel;
	}
	/**
	 * @param a_a_tel the a_a_tel to set
	 */
	public void setA_a_tel(String a_a_tel) {
		this.a_a_tel = a_a_tel;
	}
	/**
	 * @return the a_workstation
	 */
	public Integer getA_workstation() {
		return a_workstation;
	}
	/**
	 * @param a_workstation the a_workstation to set
	 */
	public void setA_workstation(Integer a_workstation) {
		this.a_workstation = a_workstation;
	}
	/**
	 * @return the a_b_training
	 */
	public Integer getA_b_training() {
		return a_b_training;
	}
	/**
	 * @param a_b_training the a_b_training to set
	 */
	public void setA_b_training(Integer a_b_training) {
		this.a_b_training = a_b_training;
	}
	/**
	 * @return the a_b_employ
	 */
	public Integer getA_b_employ() {
		return a_b_employ;
	}
	/**
	 * @param a_b_employ the a_b_employ to set
	 */
	public void setA_b_employ(Integer a_b_employ) {
		this.a_b_employ = a_b_employ;
	}
	/**
	 * @return the a_b_project
	 */
	public Integer getA_b_project() {
		return a_b_project;
	}
	/**
	 * @param a_b_project the a_b_project to set
	 */
	public void setA_b_project(Integer a_b_project) {
		this.a_b_project = a_b_project;
	}
	/**
	 * @return the a_term_begin
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd")
	public Date getA_term_begin() {
		return a_term_begin;
	}
	/**
	 * @param a_term_begin the a_term_begin to set
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd")
	public void setA_term_begin(Date a_term_begin) {
		this.a_term_begin = a_term_begin;
	}
	/**
	 * @return the a_term_end
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd")
	public Date getA_term_end() {
		return a_term_end;
	}
	/**
	 * @param a_term_end the a_term_end to set
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd")
	public void setA_term_end(Date a_term_end) {
		this.a_term_end = a_term_end;
	}
	/**
	 * @return the a_a_represent
	 */
	public String getA_a_represent() {
		return a_a_represent;
	}
	/**
	 * @param a_a_represent the a_a_represent to set
	 */
	public void setA_a_represent(String a_a_represent) {
		this.a_a_represent = a_a_represent;
	}
	/**
	 * @return the a_b_represent
	 */
	public String getA_b_represent() {
		return a_b_represent;
	}
	/**
	 * @param a_b_represent the a_b_represent to set
	 */
	public void setA_b_represent(String a_b_represent) {
		this.a_b_represent = a_b_represent;
	}
	/**
	 * @author 徐培珊
	 * @date 2018年12月1日下午8:12:55
	 * @return
	 */
	@Override
	public String toString() {
		return "Protocol [a_id=" + a_id + ", a_companyid=" + a_companyid + ", a_b_companyname=" + a_b_companyname
				+ ", a_b_address=" + a_b_address + ", a_b_contact=" + a_b_contact + ", a_b_tel=" + a_b_tel
				+ ", a_a_companyname=" + a_a_companyname + ", a_a_address=" + a_a_address + ", a_a_contact="
				+ a_a_contact + ", a_a_tel=" + a_a_tel + ", a_workstation=" + a_workstation + ", a_b_training="
				+ a_b_training + ", a_b_employ=" + a_b_employ + ", a_b_project=" + a_b_project + ", a_term_begin="
				+ a_term_begin + ", a_term_end=" + a_term_end + ", a_a_represent=" + a_a_represent + ", a_b_represent="
				+ a_b_represent + "]";
	}
}
