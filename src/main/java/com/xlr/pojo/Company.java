package com.xlr.pojo;

import java.io.Serializable;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**   
 * @ClassName:  Company   
 * @Description:员工实体类 
 * @author: 王宏志
 * @date:   2018年11月6日 上午9:11:45   
 *     
 */ 
@JsonIgnoreProperties(value = { "handler" })
public class Company implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer company_id;
	private String company_number;
	private String company_name;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date company_joindate;
	private Integer company_area;
	private String company_field;
	private String company_product;
	private Integer company_istechnology;
	private Integer company_islicense;
	private String company_leader;
	private String company_tel;
	private College college;
	private Integer company_departmentid;
	private String company_tel2;
	private String company_image;
	
	// 徐培珊添加
	private String company_address;
	private Double company_income;
	private String company_obtaininvest;
	//公司类型
	//2代表团队
	private Integer company_hatchtype;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date company_enterdate;
	private String beginTime;
	private String endTime;
	
	private Date beginDate;
	private Date endDate;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd")
	public Date getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd")
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	/**
	 * @return the company_enterdate
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd")
	public Date getCompany_enterdate() {
		return company_enterdate;
	}
	/**
	 * @param company_enterdate the company_enterdate to set
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd")
	public void setCompany_enterdate(Date company_enterdate) {
		this.company_enterdate = company_enterdate;
	}
	/**
	 * @return the company_income
	 */
	public Double getCompany_income() {
		return company_income;
	}
	/**
	 * @param company_income the company_income to set
	 */
	public void setCompany_income(Double company_income) {
		this.company_income = company_income;
	}
	/**
	 * @return the company_obtaininvest
	 */
	public String getCompany_obtaininvest() {
		return company_obtaininvest;
	}
	/**
	 * @param company_obtaininvest the company_obtaininvest to set
	 */
	public void setCompany_obtaininvest(String company_obtaininvest) {
		this.company_obtaininvest = company_obtaininvest;
	}
	/**
	 * @return the company_hatchtype
	 */
	public Integer getCompany_hatchtype() {
		return company_hatchtype;
	}
	/**
	 * @param company_hatchtype the company_hatchtype to set
	 */
	public void setCompany_hatchtype(Integer company_hatchtype) {
		this.company_hatchtype = company_hatchtype;
	}
	/**
	 * @return the company_address
	 */
	public String getCompany_address() {
		return company_address;
	}
	/**
	 * @param company_address the company_address to set
	 */
	public void setCompany_address(String company_address) {
		this.company_address = company_address;
	}
	/**
	 * @return the company_area
	 */
	public Integer getCompany_area() {
		return company_area;
	}
	/**
	 * @param company_area the company_area to set
	 */
	public void setCompany_area(Integer company_area) {
		this.company_area = company_area;
	}
	/**
	 * @return the company_istechnology
	 */
	public Integer getCompany_istechnology() {
		return company_istechnology;
	}
	/**
	 * @param company_istechnology the company_istechnology to set
	 */
	public void setCompany_istechnology(Integer company_istechnology) {
		this.company_istechnology = company_istechnology;
	}
	/**
	 * @return the company_islicense
	 */
	public Integer getCompany_islicense() {
		return company_islicense;
	}
	/**
	 * @param company_islicense the company_islicense to set
	 */
	public void setCompany_islicense(Integer company_islicense) {
		this.company_islicense = company_islicense;
	}
	/**
	 * @return the company_departmentid
	 */
	public Integer getCompany_departmentid() {
		return company_departmentid;
	}
	/**
	 * @param company_departmentid the company_departmentid to set
	 */
	public void setCompany_departmentid(Integer company_departmentid) {
		this.company_departmentid = company_departmentid;
	}
	/**
	 * @return the company_image
	 */
	public String getCompany_image() {
		return company_image;
	}
	/**
	 * @param company_image the company_image to set
	 */
	public void setCompany_image(String company_image) {
		this.company_image = company_image;
	}
	
 	public Integer getCompany_id() {
		return company_id;
	}
	public void setCompany_id(Integer company_id) {
		this.company_id = company_id;
	}
	public String getCompany_number() {
		return company_number;
	}
	public void setCompany_number(String company_number) {
		this.company_number = company_number;
	}
	public String getCompany_name() {
		return company_name;
	}
	public void setCompany_name(String company_name) {
		this.company_name = company_name;
	}
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd")
	public Date getCompany_joindate() {
		return company_joindate;
	}
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd")
	public void setCompany_joindate(Date company_joindate) {
		this.company_joindate = company_joindate;
	}
	public String getCompany_field() {
		return company_field;
	}
	public void setCompany_field(String company_field) {
		this.company_field = company_field;
	}
	public String getCompany_product() {
		return company_product;
	}
	public void setCompany_product(String company_product) {
		this.company_product = company_product;
	}
	public String getCompany_leader() {
		return company_leader;
	}
	public void setCompany_leader(String company_leader) {
		this.company_leader = company_leader;
	}
	public String getCompany_tel() {
		return company_tel;
	}
	public void setCompany_tel(String company_tel) {
		this.company_tel = company_tel;
	}
	public College getCollege() {
		return college;
	}
	public void setCollege(College college) {
		this.college = college;
	}
	public String getCompany_tel2() {
		return company_tel2;
	}
	public void setCompany_tel2(String company_tel2) {
		this.company_tel2 = company_tel2;
	}
	/**
	 * @author 徐培珊
	 * @date 2018年12月6日下午2:24:01
	 * @return
	 */
	@Override
	public String toString() {
		return "Company [company_id=" + company_id + ", company_number=" + company_number + ", company_name="
				+ company_name + ", company_joindate=" + company_joindate + ", company_area=" + company_area
				+ ", company_field=" + company_field + ", company_product=" + company_product
				+ ", company_istechnology=" + company_istechnology + ", company_islicense=" + company_islicense
				+ ", company_leader=" + company_leader + ", company_tel=" + company_tel + ", college=" + college
				+ ", company_departmentid=" + company_departmentid + ", company_tel2=" + company_tel2
				+ ", company_image=" + company_image + ", company_address=" + company_address + ", company_income="
				+ company_income + ", company_obtaininvest=" + company_obtaininvest + ", company_hatchtype="
				+ company_hatchtype + ", company_enterdate=" + company_enterdate + "]";
	}
	
}