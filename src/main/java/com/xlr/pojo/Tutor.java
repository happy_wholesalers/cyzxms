/**
 * @author 石腾飞
 * @date 2018年11月4日上午11:44:32
 * @version 1.0
 * @company 河南喜乐融商贸有限公司
 * @Copyright: 2018 www.xlr.com Inc. All rights reserved. 
 */
package com.xlr.pojo;

import java.io.Serializable;

/**
 * @author 石腾飞
 * @date 2018年11月4日上午11:44:32
 */
public class Tutor implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer t_id;
	private String t_name;
	private String t_sex;
	private String t_department;
	private String t_professional;
	private String t_tel;
	private Integer t_num;
	private String t_require;
	private String t_worknum;
	private Integer t_companyid;
	public Integer getT_id() {
		return t_id;
	}
	public void setT_id(Integer t_id) {
		this.t_id = t_id;
	}
	public String getT_name() {
		return t_name;
	}
	public void setT_name(String t_name) {
		this.t_name = t_name;
	}
	public String getT_sex() {
		return t_sex;
	}
	public void setT_sex(String t_sex) {
		this.t_sex = t_sex;
	}
	public String getT_department() {
		return t_department;
	}
	public void setT_department(String t_department) {
		this.t_department = t_department;
	}
	public String getT_professional() {
		return t_professional;
	}
	public void setT_professional(String t_professional) {
		this.t_professional = t_professional;
	}
	public String getT_tel() {
		return t_tel;
	}
	public void setT_tel(String t_tel) {
		this.t_tel = t_tel;
	}
	public Integer getT_num() {
		return t_num;
	}
	public void setT_num(Integer t_num) {
		this.t_num = t_num;
	}
	public String getT_require() {
		return t_require;
	}
	public void setT_require(String t_require) {
		this.t_require = t_require;
	}
	public String getT_worknum() {
		return t_worknum;
	}
	public void setT_worknum(String t_worknum) {
		this.t_worknum = t_worknum;
	}
	public Integer getT_companyid() {
		return t_companyid;
	}
	public void setT_companyid(Integer t_companyid) {
		this.t_companyid = t_companyid;
	}
	@Override
	public String toString() {
		return "Tutor [t_id=" + t_id + ", t_name=" + t_name + ", t_sex="
				+ t_sex + ", t_department=" + t_department
				+ ", t_professional=" + t_professional + ", t_tel=" + t_tel
				+ ", t_num=" + t_num + ", t_require=" + t_require
				+ ", t_worknum=" + t_worknum + ", t_companyid=" + t_companyid
				+ "]";
	}
}
