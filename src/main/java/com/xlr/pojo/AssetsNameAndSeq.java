package com.xlr.pojo;

import java.io.Serializable;

/**
 * @author 王宏志
 * 	资产名称与编号
 * @date 2018年12月6日下午2:46:39
 * @TOOD 
 */
public class AssetsNameAndSeq implements Serializable{

	private String assetsSequence;
	private String assetsName;
	public String getAssetsSequence() {
		return assetsSequence;
	}
	public void setAssetsSequence(String assetsSequence) {
		this.assetsSequence = assetsSequence;
	}
	public String getAssetsName() {
		return assetsName;
	}
	public void setAssetsName(String assetsName) {
		this.assetsName = assetsName;
	}
	@Override
	public String toString() {
		return "AssetsNameAndSeq [assetsSequence=" + assetsSequence + ", assetsName=" + assetsName + "]";
	}
	
}
