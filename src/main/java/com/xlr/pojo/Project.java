/**
 * @author 石腾飞
 * @date 2018年11月4日上午11:41:05
 * @version 1.0
 * @company 河南喜乐融商贸有限公司
 * @Copyright: 2018 www.xlr.com Inc. All rights reserved. 
 */
package com.xlr.pojo;

import java.io.Serializable;

/**
 * @author 石腾飞
 * @date 2018年11月4日上午11:41:05
 */
public class Project implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int p_id;
	private String p_brief;
	private String p_status;
	private String p_mpc;
	private String p_type;
	private int p_uid;
	private String p_gszc;
	private String p_advantage;

	/**
	 * @return the p_id
	 */
	public int getP_id() {
		return p_id;
	}

	/**
	 * @param p_id the p_id to set
	 */
	public void setP_id(int p_id) {
		this.p_id = p_id;
	}

	/**
	 * @return the p_brief
	 */
	public String getP_brief() {
		return p_brief;
	}

	/**
	 * @param p_brief the p_brief to set
	 */
	public void setP_brief(String p_brief) {
		this.p_brief = p_brief;
	}

	/**
	 * @return the p_status
	 */
	public String getP_status() {
		return p_status;
	}

	/**
	 * @param p_status the p_status to set
	 */
	public void setP_status(String p_status) {
		this.p_status = p_status;
	}

	/**
	 * @return the p_mpc
	 */
	public String getP_mpc() {
		return p_mpc;
	}

	/**
	 * @param p_mpc the p_mpc to set
	 */
	public void setP_mpc(String p_mpc) {
		this.p_mpc = p_mpc;
	}

	/**
	 * @return the p_type
	 */
	public String getP_type() {
		return p_type;
	}

	/**
	 * @param p_type the p_type to set
	 */
	public void setP_type(String p_type) {
		this.p_type = p_type;
	}

	/**
	 * @return the p_uid
	 */
	public int getP_uid() {
		return p_uid;
	}

	/**
	 * @param p_uid the p_uid to set
	 */
	public void setP_uid(int p_uid) {
		this.p_uid = p_uid;
	}

	/**
	 * @return the p_gszc
	 */
	public String getP_gszc() {
		return p_gszc;
	}

	/**
	 * @param p_gszc the p_gszc to set
	 */
	public void setP_gszc(String p_gszc) {
		this.p_gszc = p_gszc;
	}

	/**
	 * @return the p_advantage
	 */
	public String getP_advantage() {
		return p_advantage;
	}

	/**
	 * @param p_advantage the p_advantage to set
	 */
	public void setP_advantage(String p_advantage) {
		this.p_advantage = p_advantage;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Project [p_id=" + p_id + ", p_brief=" + p_brief + ", p_status=" + p_status + ", p_mpc=" + p_mpc
				+ ", p_type=" + p_type + ", p_uid=" + p_uid + ", p_gszc=" + p_gszc + ", p_advantage=" + p_advantage
				+ "]";
	}

}
