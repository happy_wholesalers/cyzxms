package com.xlr.pojo;

/**
 * @author 王宏志
 * 	入驻项目负责人简历
 * @date 2018年12月4日下午6:56:10
 * @TOOD 
 */
public class ChargerIntro {

	//入驻申请表单ID
	private Integer s_id;
	//申请入驻的公司ID
	private Integer s_companyid;
	//负责人姓名
	private String s_name;
	//入驻项目负责人性别
	private String s_sex;
	//入驻项目负责人单位或系别
	private String s_dep;
	//入驻项目负责人职称
	private String s_profession;
	//入驻项目负责人联系电话
	private String s_contact;
	//目前办公人数
	private Integer s_currentNumber;
	//办公场地需求
	private String s_field;
	//工位数量
	private Integer s_workstation;
	public Integer getS_id() {
		return s_id;
	}
	public void setS_id(Integer s_id) {
		this.s_id = s_id;
	}
	public Integer getS_companyid() {
		return s_companyid;
	}
	public void setS_companyid(Integer s_companyid) {
		this.s_companyid = s_companyid;
	}
	public String getS_name() {
		return s_name;
	}
	public void setS_name(String s_name) {
		this.s_name = s_name;
	}
	public String getS_sex() {
		return s_sex;
	}
	public void setS_sex(String s_sex) {
		this.s_sex = s_sex;
	}
	public String getS_dep() {
		return s_dep;
	}
	public void setS_dep(String s_dep) {
		this.s_dep = s_dep;
	}
	public String getS_profession() {
		return s_profession;
	}
	public void setS_profession(String s_profession) {
		this.s_profession = s_profession;
	}
	public String getS_contact() {
		return s_contact;
	}
	public void setS_contact(String s_contact) {
		this.s_contact = s_contact;
	}
	public Integer getS_currentNumber() {
		return s_currentNumber;
	}
	public void setS_currentNumber(Integer s_currentNumber) {
		this.s_currentNumber = s_currentNumber;
	}
	public String getS_field() {
		return s_field;
	}
	public void setS_field(String s_field) {
		this.s_field = s_field;
	}
	public Integer getS_workstation() {
		return s_workstation;
	}
	public void setS_workstation(Integer s_workstation) {
		this.s_workstation = s_workstation;
	}
	
}
