package com.xlr.realm;

import java.util.List;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

import com.xlr.pojo.Menus;
import com.xlr.pojo.Userinfo;
import com.xlr.service.permission.MenusService;
import com.xlr.service.system.UserService;

public class Realm extends AuthorizingRealm {
	
	@Autowired
	private UserService userService;
	@Autowired
	private MenusService menusService;
	/**
	 * 授权方法
	 */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		System.out.println("授权中。。。");
		//获取当前登录的用户
		Userinfo userinfo = (Userinfo)principals.getPrimaryPrincipal();
		//获取用户的所有菜单
		List<Menus> menus = menusService.findMenusListByUserid(userinfo.getUser_id());
		SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
		for (Menus menu : menus) {
			info.addStringPermission(menu.getMenuname());
		}
		return info;
	}

	/**
	 * 认证方法
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token)
			throws AuthenticationException {
		System.out.println("认证中。。。");
		UsernamePasswordToken upt = (UsernamePasswordToken)token;
		String pwd = new String(upt.getPassword());
//		// 根据用户名和密码查找用户
		Userinfo userInfo = userService.findUserInfo(upt.getUsername(), pwd);
		if(userInfo != null) {
			//返回认证信息
			//参数1：主角，就是登陆的用户
			//参数2：证书，就是凭证，对应密码
			//参数3：当前realm的名称
			return new SimpleAuthenticationInfo(userInfo, pwd, getName());
		}
		return null;
	}

}
